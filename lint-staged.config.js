module.exports = {
    '{src,test}/**/*.{js,jsx,ts,tsx}': ['eslint --cache --fix', 'prettier --write'],
    '{src,test}/**/*.{ts,tsx}': [() => 'tsc -p tsconfig.json --skipLibCheck --noEmit'],
};
