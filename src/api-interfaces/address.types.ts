import { IAddressQueryParameters } from './parameters.types';

/**
 * @request GET /address-validate
 * @tags address
 */
export type IAddressValidateQueryParams = Pick<IAddressQueryParameters, 'type' | 'countryCode' | 'postalCode' | 'cityName' | 'countyName' | 'strictValidation'>;
