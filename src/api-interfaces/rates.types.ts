import { IQueryParameters } from './parameters.types';

/**
 * @request GET /rates
 */
export type IGetRatesOnePieceQueryParams = Pick<
    IQueryParameters,
    | 'accountNumber'
    | 'originCountryCode'
    | 'originPostalCode'
    | 'originCityName'
    | 'destinationCountryCode'
    | 'destinationPostalCode'
    | 'destinationCityName'
    | 'weight'
    | 'length'
    | 'width'
    | 'height'
    | 'plannedShippingDate'
    | 'isCustomsDeclarable'
    | 'unitOfMeasurement'
    | 'nextBusinessDay'
    | 'strictValidation'
    | 'getAllValueAddedServices'
    | 'requestEstimatedDeliveryDate'
    | 'estimatedDeliveryDateType'
>;

/**
 * @request POST /rates
 */
export type IGetRatesManyQueryParameters = Pick<IQueryParameters, 'strictValidation'>;
