import { WithRequired, OneOf } from './utility-types';

/**
 * supermodelIoLogisticsExpressAccount
 * Account definition
 */
export type supermodelIoLogisticsExpressAccount = {
    /**
     * @description Please enter DHL Express acount type
     * @example shipper
     * @enum {string}
     */
    typeCode: 'shipper' | 'payer' | 'duties-taxes';
    /**
     * @description Please enter DHL Express account number
     * @example 123456789
     */
    number: string;
};

/**
 * supermodelIoLogisticsExpressAddress
 * Address definition
 */
export type supermodelIoLogisticsExpressAddress = {
    /**
     * @description Please enter your postcode or leave empty if the address doesn't have a postcode
     * @example 14800
     */
    postalCode: string;
    /**
     * @description Please enter the city
     * @example Prague
     */
    cityName: string;
    /**
     * @description Please enter ISO country code
     * @example CZ
     */
    countryCode: string;
    /**
     * @description Please enter your province or state code
     * @example CZ
     */
    provinceCode?: string;
    /**
     * @description Please enter address line 1
     * @example V Parku 2308/10
     */
    addressLine1: string;
    /**
     * @description Please enter address line 2
     * @example addres2
     */
    addressLine2?: string;
    /**
     * @description Please enter address line 3
     * @example addres3
     */
    addressLine3?: string;
    /**
     * @description Please enter your suburb or county name
     * @example Central Bohemia
     */
    countyName?: string;
};

/**
 * supermodelIoLogisticsExpressAddressCreateShipmentRequest
 * Address definition for /shipments request
 */
export type supermodelIoLogisticsExpressAddressCreateShipmentRequest = {
    /**
     * @description Please enter your postcode or leave empty if the address doesn't have a postcode
     * @example 14800
     */
    postalCode: string;
    /**
     * @description Please enter the city
     * @example Prague
     */
    cityName: string;
    /**
     * @description Please enter ISO country code
     * @example CZ
     */
    countryCode: string;
    /**
     * @description Please enter your province or state code
     * @example CZ
     */
    provinceCode?: string;
    /**
     * @description Please enter address line 1
     * @example V Parku 2308/10
     */
    addressLine1: string;
    /**
     * @description Please enter address line 2
     * @example addres2
     */
    addressLine2?: string;
    /**
     * @description Please enter address line 3
     * @example addres3
     */
    addressLine3?: string;
    /**
     * @description Please enter your suburb or county name
     * @example Central Bohemia
     */
    countyName?: string;
    /**
     * @description Please enter your state or province name
     * @example Central Bohemia
     */
    provinceName?: string;
    /**
     * @description Please enter your country name
     * @example Czech Republic
     */
    countryName?: string;
};

/**
 * supermodelIoLogisticsExpressAddressCreateShipmentResponse
 * Address definition for /shipments response
 */
export type supermodelIoLogisticsExpressAddressCreateShipmentResponse = {
    /**
     * @description Postal code
     * @example 27801
     */
    postalCode: string;
    /**
     * @description City name
     * @example Kralupy nad Vltavou
     */
    cityName: string;
    /**
     * @description Country code
     * @example CZ
     */
    countryCode: string;
    /**
     * @description Province or state code
     * @example CZ
     */
    provinceCode?: string;
    /**
     * @description Address line 1
     * @example Na Cukrovaru 1063
     */
    addressLine1: string;
    /**
     * @description Address line 2
     * @example addres 2
     */
    addressLine2?: string;
    /**
     * @description Address line 3
     * @example address 3
     */
    addressLine3?: string;
    /**
     * @description Suburb or county name
     * @example Kralupy
     */
    cityDistrictName?: string;
    /**
     * @description Please enter your state or province name
     * @example Central Bohemia
     */
    provinceName?: string;
    /**
     * @description Please enter your country name
     * @example Czech Republic
     */
    countryName?: string;
};

/**
 * supermodelIoLogisticsExpressAddressValidateResponse
 * Definition of /address-validate response message
 * @description Comment describing your JSON Schema
 */
export type supermodelIoLogisticsExpressAddressValidateResponse = {
    warnings?: string[];
    address?: {
        /** @example CZ */
        countryCode: string;
        /** @example 14800 */
        postalCode: string;
        /** @example PRAGUE */
        cityName?: string;
        /** @description Please enter your suburb or county name */
        countyName?: string;
        serviceArea?: {
            /** @example PRG */
            code?: string;
            /** @example PRAGUE-CZECH REPUBLIC, THE */
            description?: string;
            /** @example +01:00 */
            GMTOffset?: string;
        };
    }[];
};

/**
 * supermodelIoLogisticsExpressErrorResponse
 * @description error message
 * @request GET /rates
 */
export type supermodelIoLogisticsExpressErrorResponse = {
    /** @example /expressapi/shipments */
    instance?: string;
    /** @example #/customerDetails/shipperDetails: required key [countryCode] not found */
    detail?: string;
    /** @example Validation error */
    title?: string;
    /** @example Unprocessable Entity */
    message?: string;
    additionalDetails?: Array<string>;
    /** @example 998 */
    status?: string;
};

/**
 * supermodelIoLogisticsExpressRates
 * Definition of /rates, /landed-cost response message
 *
 * @request GET /rates
 * @request GET /landed-cost
 */
export type ILogisticsExpressRates = {
    /** products */
    products: Array<{
        /**
         * @description DHL Express product - Global Product Name
         * @example EXPRESS DOMESTIC
         */
        productName?: string;
        /**
         * @description This is the global DHL Express product code for which the delivery is feasible respecting the input data from the request.
         * @example N
         */
        productCode?: string;
        /**
         * @description This is the local DHL Express product code for which the delivery is feasible respecting the input data from the request.
         * @example N
         */
        localProductCode?: string;
        /**
         * @description The country code for the local service used
         * @example CZ
         */
        localProductCountryCode?: string;
        /**
         * @description The NetworkTypeCode element indicates the product belongs to the Day Definite (DD) or Time Definite (TD) network.<BR>            Possible Values;<BR>             DD: Day Definite product<BR>             TD: Time Definite product
         * @example TD
         */
        networkTypeCode?: string;
        /**
         * @description Indicator that the product only can be offered to customers with prior agreement.
         * @example false
         */
        isCustomerAgreement?: boolean;
        /** weight */
        weight: {
            /**
             * @description The dimensional weight of the shipment
             * @example 0
             */
            volumetric?: number;
            /**
             * @description The quoted weight of the shipment
             * @example 1.5
             */
            provided?: number;
            /**
             * @description The unit of measurement for the dimensions of the package.
             * @example metric
             */
            unitOfMeasurement?: string;
        };
        totalPrice: Array<{
            /**
             * @description Possible Values :<BR>                  'BILLC', billing currency<BR>                  'PULCL', country public rates currency<BR>                  'BASEC', base currency
             * @example BILLC
             */
            currencyType?: string;
            /**
             * @description This the currency of the rated shipment for the prices listed.
             * @example GBP
             */
            priceCurrency?: string;
            /**
             * @description This is the total price of the rated shipment for the product listed.
             * @example 141.51
             */
            price: number;
        }>;
        totalPriceBreakdown?: Array<{
            /**
             * @description Possible Values :<BR>                  'BILLC', billing currency<BR>                  'PULCL', country public rates currency<BR>                  'BASEC', base currency
             * @example BILLC
             */
            currencyType?: string;
            /**
             * @description This the currency of the rated shipment for the prices listed.
             * @example GBP
             */
            priceCurrency?: string;
            priceBreakdown?: Array<{
                /**
                 * @description Expected values in Breakdown/Type are below:<BR>                        STTXA:  Total tax for the shipment<BR>                        STDIS: Total discount for the shipment<BR>                        SPRQT: Net shipment / weight charge
                 * @example SPRQT
                 */
                typeCode: string;
                /**
                 * @description The amount price of DHL product and services
                 * @example 114.92
                 */
                price: number;
            }>;
        }>;
        detailedPriceBreakdown?: Array<{
            /**
             * @description Possible Values :<BR>                  'BILLC', billing currency<BR>                  'PULCL', country public rates currency<BR>                  'BASEC', base currency
             * @example BILLC
             */
            currencyType?: string;
            /**
             * @description This the currency of the rated shipment for the prices listed.
             * @example GBP
             */
            priceCurrency: string;
            breakdown?: Array<{
                /**
                 * @description For /rates:<BR>  name within the first occurrence of breakdown will be the Global Product Name.<BR><BR>
                 * For /landed-cost:<BR> When landed-cost is requested then following items name (Charge Types) might be returned: <BR>                        Charge Type : Description <BR>                        STDIS : Quoted shipment total discount <BR>                        SCUSV : Shipment Customs value <BR>                        SINSV : Insured value <BR>                        SPRQD : Shipment product quote discount<BR>                        SPRQN : The price quoted to the Customer by DHL at the time of the booking. This quote covers the weight price including discounts and without taxes. <BR>                        STSCH : The total of service charges quoted to customer for DHL Express value added services, the amount is after discounts and doesn't include tax amounts. <BR>                        MACHG : The total of service charges as provided by Merchant for the purpose of landed cost calculation. <BR>                        MFCHG : The freight charge as provided by Merchant for the purpose of landed cost calculation.
                 * @example 12:00 PREMIUM
                 */
                name?: string;
                /**
                 * @description Special service or extra charge code. This is the code you would have to use in the /shipment service if you wish to add an optional Service such as Saturday delivery
                 * @example YK
                 */
                serviceCode?: string;
                /**
                 * @description Local service code
                 * @example YK
                 */
                localServiceCode?: string;
                /** @description Price breakdown type code. <BR>typeCode within the first occurrence of breakdown will be the Local Product Name. */
                typeCode?: string;
                /**
                 * @description Special service charge code type for service.
                 * @example SCH
                 */
                serviceTypeCode?: string;
                /**
                 * @description Price breakdown value
                 * @example 5
                 */
                price?: number;
                /**
                 * @description This the currency of the rated shipment for the prices listed.
                 * @example GBP
                 */
                priceCurrency?: string;
                /**
                 * @description Customer agreement indicator for product and services, if service is offered with prior customer agreement
                 * @example false
                 */
                isCustomerAgreement?: boolean;
                /**
                 * @description Indicator if the special service is marketed service
                 * @example false
                 */
                isMarketedService?: boolean;
                /**
                 * @description Indicator if there is any discount allowed
                 * @example false
                 */
                isBillingServiceIndicator?: boolean;
                /** priceDetail */
                priceBreakdown?: Array<{
                    /**
                     * @description If a breakdown is provided, details can either be; 'TAX',<BR>                              'DISCOUNT'
                     * @example TAX
                     */
                    priceType?: string;
                    /**
                     * @description Discount or tax type codes as provided by DHL Express. Example values:<BR>                              For discount;<BR>                              P: promotional<BR>                              S: special
                     * @example All Bu
                     */
                    typeCode?: string;
                    /**
                     * @description The actual amount of the discount/tax
                     * @example 0
                     */
                    price?: number;
                    /**
                     * @description Percentage of the discount/tax
                     * @example 0
                     */
                    rate?: number;
                    /**
                     * @description The base amount of the service charge
                     * @example 5
                     */
                    basePrice?: number;
                }>;
                /**
                 * @description Tariff Rate Formula on Shipment Level
                 * @example ((0.3464 % COST) MAX (528.33))
                 */
                tariffRateFormula?: string;
            }>;
        }>;
        /** @description Group of serviceCodes that are mutually exclusive.  Only one serviceCode among the list must be applied for a shipment */
        serviceCodeMutuallyExclusiveGroups?: Array<{
            /**
             * @description Mutually exclusive serviceCode group name
             * @example Exclusive Billing Services
             */
            serviceCodeRuleName?: string;
            /**
             * @description Mutually exclusive serviceCode group description
             * @example Mutually exclusive Billing Services - shipment can contain just one of following
             */
            description?: string;
            serviceCodes?: Array<{
                /**
                 * @description The special service charge code
                 * @example PZ
                 */
                serviceCode?: string;
            }>;
        }>;
        /** @description Dependency rule groups for a particular serviceCode. */
        serviceCodeDependencyRuleGroups?: Array<{
            /**
             * @description Dependent special service charge code where the rule groups are applied
             * @example PZ
             */
            dependentServiceCode?: string;
            dependencyRuleGroup?: Array<{
                /**
                 * @description Dependency rule group name
                 * @example Labelfree and PLT rule
                 */
                dependencyRuleName?: string;
                /**
                 * @description Dependency rule group description
                 * @example Labelfree requires Paperless Trade (PLT) only if PLT is allowed for product globaly
                 */
                dependencyDescription?: string;
                /**
                 * @description Dependency rule group condition statement
                 * @example Must provide the requiredServiceCode if it is allowed for the productCode
                 */
                dependencyCondition?: string;
                requiredServiceCodes?: Array<{
                    /**
                     * @description required special service code
                     * @example WY
                     */
                    serviceCode?: string;
                }>;
            }>;
        }>;
        pickupCapabilities?: {
            /**
             * @description This indicator has values of Y or N, and tells the consumer if the service in the response has a pickup date on the same day as the requested shipment date (per the request).
             * @example false
             */
            nextBusinessDay?: boolean;
            /**
             * @description This is the cutoff time for the service<BR>                offered in the response. This represents the latest time (local to origin) which the shipment can be tendered to the courier for that service on that day.
             * @example 2019-09-18T15:00:00
             */
            localCutoffDateAndTime?: string;
            /**
             * @description Pickup cut off time in origin's time zone
             * @example 16:00:00
             */
            GMTCutoffTime?: string;
            /**
             * @description The DHL earliest time possible for pickup
             * @example 09:30:00
             */
            pickupEarliest?: string;
            /**
             * @description The DHL latest time possible for pickup
             * @example 16:00:00
             */
            pickupLatest?: string;
            /**
             * @description The DHL Service Area Code for the origin of the Shipment
             * @example ELA
             */
            originServiceAreaCode?: string;
            /**
             * @description The DHL Facility Code for the Origin
             * @example HHR
             */
            originFacilityAreaCode?: string;
            /**
             * @description This is additional transit delays (in days) for shipment picked up from the mentioned city or postal area to arrival at the service area.
             * @example 0
             */
            pickupAdditionalDays?: number;
            /**
             * @description Pickup day of the week number
             * @example 3
             */
            pickupDayOfWeek?: number;
        };
        deliveryCapabilities?: {
            /**
             * @description Delivery Date capabilities considering customs clearance days.Estimated Delivery Date Type. QDDF: is the fastest transit time as quoted to the customer at booking or shipment creation. When clearance or any other non-transport operational component is expected to impact transit time, QDDF does not constitute DHL's service commitment. QDDC: cconstitutes DHL's service commitment as quoted at booking or shipment creation. QDDC builds in clearance time, and potentially other special operational non-transport component(s), when relevant.
             * @example QDDC
             */
            deliveryTypeCode?: string;
            /**
             * @description This is the estimated date/time the shipment will be delivered by for the rated shipment and product listed
             * @example 2019-09-20T12:00:00
             */
            estimatedDeliveryDateAndTime?: string;
            /**
             * @description The DHL Service Area Code for the destination of the Shipment
             * @example PRG
             */
            destinationServiceAreaCode?: string;
            /**
             * @description The DHL Facility Code for the Destination
             * @example PR3
             */
            destinationFacilityAreaCode?: string;
            /**
             * @description This is additional transit delays (in days) for shipment delivered to the<BR>                mentioned city or postal area following arrival at the service area.
             * @example 0
             */
            deliveryAdditionalDays?: number;
            /**
             * @description Destination day of the week number
             * @example 5
             */
            deliveryDayOfWeek?: number;
            /**
             * @description The number of transit days
             * @example 2
             */
            totalTransitDays?: number;
        };
        items?: Array<{
            /**
             * @description Item line number
             * @example 1
             */
            number: number;
            breakdown?: Array<{
                /**
                 * @description Name of the charge
                 * @example DUTY
                 */
                name?: string;
                /**
                 * @description Special service or extra charge code. This is the code you would have to use in the /shipment service if you wish to add an optional Service such as Saturday delivery
                 * @example II
                 */
                serviceCode?: string;
                /**
                 * @description Local service code
                 * @example II
                 */
                localServiceCode?: string;
                /**
                 * @description Charge type or category.<BR>                        Possible values;<BR>                        - DUTY<BR>                        - TAX<BR>                        - FEE
                 * @example DUTY
                 */
                typeCode: string;
                /**
                 * @description Special service charge code type for service. XCH type charge codes are Optional Services and should be displayed to users for selection.<BR>                        The possible values are;<BR>                        - XCH = Extra charge<BR>                        - FEE = Fee<BR>                        - SCH = Surcharge<BR>                        - NRI = Non Revenue Item<BR>                        Other charges may be automatically returned when applicable.
                 * @example FEE
                 */
                serviceTypeCode?: string;
                /**
                 * @description The charge amount of the line item charge.
                 * @example 20
                 */
                price: number;
                /**
                 * @description This the currency of the rated shipment for the prices listed.
                 * @example CZK
                 */
                priceCurrency?: string;
                /**
                 * @description Customer agreement indicator for product and services, if service is offered with prior customer agreement
                 * @example false
                 */
                isCustomerAgreement?: boolean;
                /**
                 * @description Indicator if the special service is marketed service
                 * @example false
                 */
                isMarketedService?: boolean;
                /**
                 * @description Indicator if there is any discount allowed
                 * @example false
                 */
                isBillingServiceIndicator?: boolean;
                /** priceDetail */
                priceBreakdown?: Array<{
                    /**
                     * @description Discount or tax type codes as provided by DHL.<BR>                              Example values;<BR>                              For discount;<BR>                              P: promotional<BR>                              S: special
                     * @example P
                     */
                    priceType?: string;
                    /**
                     * @description If a breakdown is provided, details can either be; - "TAX",<BR>                              - "DISCOUNT"
                     * @example TAX
                     */
                    typeCode?: string;
                    /**
                     * @description The actual amount of the discount/tax
                     * @example 110
                     */
                    price?: number;
                    /**
                     * @description Percentage of the discount/tax
                     * @example 10
                     */
                    rate?: number;
                    /**
                     * @description The base amount of the service charge
                     * @example 100
                     */
                    basePrice?: number;
                }>;
                /**
                 * @description Tariff Rate Formula on Line Item Level
                 * @example ((0.3464 % COST) MAX (528.33))
                 */
                tariffRateFormula?: string;
            }>;
        }>;
        /**
         * @description The date when the rates for DHL products and services is provided
         * @example 2020-02-25
         */
        pricingDate?: string;
    }>;
    /** exchangeRates */
    exchangeRates?: Array<{
        /**
         * @description Rate of the currency exchange
         * @example 1.188411
         */
        currentExchangeRate: number;
        /**
         * @description The currency code
         * @example GBP
         */
        currency: string;
        /**
         * @description The currency code of the base currency is either USD or EUR
         * @example EUR
         */
        baseCurrency: string;
    }>;
    warnings?: Array<string>;
};

/**
 * supermodelIoLogisticsExpressBankDetails
 * Bank Details definition
 */
export type supermodelIoLogisticsExpressBankDetails = Array<{
    /**
     * @description To be mapped in commercial Invoice - Russia Bank Name field
     * @example Russian Bank Name
     */
    name?: string;
    /**
     * @description To be mapped in commercial Invoice - Russia Bank Settlement Account Number in RUR field
     * @example RUB
     */
    settlementLocalCurrency?: string;
    /**
     * @description To be mapped in commercial Invoice - Russia Bank Settlement Account Number in RUR field
     * @example USD
     */
    settlementForeignCurrency?: string;
}>;

/**
 * supermodelIoLogisticsExpressContact
 * Contact definition
 */
export type supermodelIoLogisticsExpressContact = {
    /**
     * @description Please enter email address
     * @example that@before.de
     */
    email?: string;
    /**
     * @description Please enter phone number
     * @example +1123456789
     */
    phone: string;
    /**
     * @description Please enter mobile phone number
     * @example +60112345678
     */
    mobilePhone?: string;
    /**
     * @description Please enter company name
     * @example Company Name
     */
    companyName: string;
    /**
     * @description Please enter full name
     * @example John Brew
     */
    fullName: string;
};

/** Contact definition (Buyer) */
export type supermodelIoLogisticsExpressContactBuyer = {
    /**
     * @description Please enter email address
     * @example buyer@domain.com
     */
    email?: string;
    /**
     * @description Please enter phone number
     * @example +44123456789
     */
    phone: string;
    /**
     * @description Please enter mobile phone number
     * @example +42123456789
     */
    mobilePhone?: string;
    /**
     * @description Please enter company name
     * @example Customer Company Name
     */
    companyName: string;
    /**
     * @description Please enter full name
     * @example Mark Companer
     */
    fullName: string;
};

/** Contact definition of /shipments response */
export type supermodelIoLogisticsExpressContactCreateShipmentResponse = {
    /**
     * @description Company name
     * @example Better One s.r.o
     */
    companyName: string;
    /**
     * @description Full name
     * @example Huahom Peral
     */
    fullName: string;
};

/** Definition of /shipments request message */
export type supermodelIoLogisticsExpressCreateShipmentRequest = {
    /**
     * @description Identifies the date and time the package is tendered. Both the date and time portions of the string are expected to be used. The date should not be a past date or a date more than 10 days in the future. The time is the local time of the shipment based on the shipper's time zone. The date component must be in the format: YYYY-MM-DD; the time component must be in the format: HH:MM:SS using a 24 hour clock. The date and time parts are separated by the letter T (e.g. 2006-06-26T17:00:00 GMT+01:00).
     * @example 2019-08-04T14:00:31GMT+01:00
     */
    plannedShippingDateAndTime: string;
    /** pickup */
    pickup: {
        /**
         * @description Please advise if a pickup is needed for this shipment
         * @default false
         * @example false
         */
        isRequested: boolean;
        /**
         * @description The latest time the location premises is available to dispatch the DHL Express shipment. (HH:MM)
         * @example 18:00
         */
        closeTime?: string;
        /**
         * @description Provides information on where the package should be picked up by DHL courier.
         * @example reception
         */
        location?: string;
        /** @description Details special pickup instructions you may wish to send to the DHL Courier. */
        specialInstructions?: {
            /**
             * @description Any special instructions user wish to send to the courier for the order pick-up.
             * @example please ring door bell
             */
            value: string;
            /**
             * @description for future use
             * @example TBD
             */
            typeCode?: string;
        }[];
        /** @description Please enter address and contact details related to your pickup */
        pickupDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressBankDetails;
            /**
             * @description Please enter the business party type related to the pickup.
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details of the individual requesting the pickup */
        pickupRequestorDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressBankDetails;
            /**
             * @description Please enter the business party type of the pickup requestor.
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
    };
    /**
     * @description Please enter DHL Express Global Product code
     * @example D
     */
    productCode: string;
    /**
     * @description Please enter DHL Express Local Product code. Important when shipping domestic products.
     * @example D
     */
    localProductCode?: string;
    /**
     * @description Please advise if you want to get rate estimates for given shipment
     * @default false
     */
    getRateEstimates?: boolean;
    /**
     * accounts
     * @description Please enter all the DHL Express accounts and types to be used for this shipment
     */
    accounts: supermodelIoLogisticsExpressAccount[];
    /**
     * valueAddedServices
     * @description This section communicates additional shipping services, such as Insurance (or Shipment Value Protection).
     */
    valueAddedServices?: supermodelIoLogisticsExpressValueAddedServices[];
    /** @description Here you can modify label, waybillDoc, invoice and shipment receipt properties */
    outputImageProperties?: {
        /**
         * @description Printer DPI Resolution for X-axis and Y-axis (in DPI) for transport label and waybill document output
         * @example 300
         * @enum {number}
         */
        printerDPI?: 200 | 300;
        /** @description Customer barcodes to be printed on supported transport label templates */
        customerBarcodes?: {
            /**
             * @description Please enter barcode content
             * @example barcode content
             */
            content: string;
            /**
             * @description Please tner text below customer barcode
             * @example text below barcode
             */
            textBelowBarcode?: string;
            /**
             * @description Please enter valid Symbology code
             * @example 93
             * @enum {string}
             */
            symbologyCode: '93' | '39' | '128';
        }[];
        /** @description Customer Logo Image to be printed on transport label */
        customerLogos?: {
            /**
             * @description Please specify image file format
             * @enum {string}
             */
            fileFormat: 'PNG' | 'GIF' | 'JPEG' | 'JPG';
            /**
             * @description Please provide base64 encoded logo image
             * @example base64 encoded image
             */
            content: string;
        }[];
        /**
         * @description Please provide the format of the output documents. Note that invoice and shipment receipt will always come back as PDF
         * @default pdf
         * @example pdf
         * @enum {string}
         */
        encodingFormat?: 'pdf' | 'zpl' | 'lp2' | 'epl';
        /** @description Here the image options are defined for label, waybillDoc, invoice, QRcode and shipment receipt */
        imageOptions?: WithRequired<
            {
                /**
                 * @description Please enter the document type you want to wish set properties for
                 * @example label
                 * @enum {string}
                 */
                typeCode: 'label' | 'waybillDoc' | 'invoice' | 'qr-code' | 'shipmentReceipt';
                /**
                 * @description Please enter DHL Express document template name. <BR>                Sample Transport label templates:<BR>                ECOM26_84_A4_001 <BR>                ECOM26_84_001 - default<BR>                ECOM_TC_A4<BR>                ECOM26_A6_002<BR>                ECOM26_84CI_001<BR>                ECOM26_84CI_002 - supported single customer barcode<BR>                ECOM26_84CI_003 - to be used if customer barcodes are used<BR>                ECOM_A4_RU_002<BR> ECOM26_84_LBBX_001 - supported for loose BBX shipment<BR> ECOM26_64_LBBX_001 - supported for loose BBX shipment<BR>Sample WaybillDoc templates<BR>                ARCH_8X4_A4_002<BR>                ARCH_8X4 - default<BR>                ARCH_6X4<BR>                ARCH_A4_RU_002<BR>                <BR>                Sample Commercial invoice templates:<BR>                COMMERCIAL_INVOICE_04 - This template can print the Shipper, Recipient, and Buyer and Importer address details and is on portrait orientation, exclusive use for preparing Loose BBX shipment.<BR>                COMMERCIAL_INVOICE_P_10 - (default) This template can print the Shipper, Recipient and upto two more additional address details in portrait orientation. Note: If customer provided more than four address roles in the request message and this template is selected, the rendered invoice will only contain four address roles based on order of priority: Shipper, Recipient, Seller, Importer, Exporter, Buyer. <BR>                COMMERCIAL_INVOICE_L_10 - This template can print the Shipper,Recipient, Buyer, and Importer and Exporter address details and is on landscape orientation..<BR>                RET_COM_INVOICE_A4_01 - This template can print the Shipper, Recipient and Importer of record address details and is on landscape orientation. This template is for exclusive use for certain shipment where the goods are actual 'returns'. The Shipper is the party that earlier has received the goods, but now wishes to return the goods to its originating party. The Recipient in this shipment scenario will receive the 'returned goods'. Therefore such request of shipment with an invoice rendering may utilize the specific invoice template for 'Returns Invoice'.<BR>                <BR>                Sample Shipment Receipt template<BR>                SHIP_RECPT_A4_RU_002<BR> SHIPRCPT_EN_001 - default <BR> <BR> Sample QR Code template template<BR>  QR_1_00_LL_PNG_001 - default
                 * @example ECOM26_84_001
                 */
                templateName?: string;
                /**
                 * @description To be used for waybillDoc, invoice, shipment receipt and QRcode. If set to true then the document is provided otherwise not
                 * @example true
                 */
                isRequested?: boolean;
                /**
                 * @description To be used for waybillDoc. If set to true then account information will not be printed on the waybillDoc
                 * @example false
                 */
                hideAccountNumber?: boolean;
                /**
                 * @description You can ask up to 2 waybillDoc copies to be provided
                 * @example 1
                 */
                numberOfCopies?: number;
                /**
                 * @description Please advise what type of customs documentation is required
                 * @enum {string}
                 */
                invoiceType?: 'commercial' | 'proforma' | 'returns';
                /**
                 * @description Please enter ISO 3 letters language code for invoice or shipment receipt
                 * @example eng
                 */
                languageCode?: string;
                /**
                 * @description Please enter ISO 2 letters language country code for invoice or shipment receipt
                 * @example br
                 */
                languageCountryCode?: string;
                /**
                 * @description Please enter ISO 4 letters language script code for shipment receipt
                 * @example Latn
                 */
                languageScriptCode?: string;
                /**
                 * @description Please provide the format of the QR Code output format.
                 * @example png
                 * @enum {string}
                 */
                encodingFormat?: 'png';
                /**
                 * @description DHL Logo to be printed in Transport Label or Waybill Document
                 * @example false
                 */
                renderDHLLogo?: boolean;
                /**
                 * @description To print respective Transport Label and Waybill document into A4 margin PDF.<BR>                Note: ECOM26_A6_002,ECOM26_84CI_001,ECOM26_84CI_002,ARCH_6X4,ARCH_8X4 template. <BR>                This option is applicable only for PDF encodingFormat selection.<BR>                false: Transport Label and Waybill document will use default margin settings (default behavior) <BR>                true: Transport Label and Waybill document will print into A4 margin PDF
                 * @example false
                 */
                fitLabelsToA4?: boolean;
                /** @description Additional customer label free text that can be printed in certain label.Note: Applicable only to ECOM26_A6_002, ECOM_TC_A4 and ECOM26_84CI_001. */
                labelFreeText?: string;
                /** @description Additional customer label text that can be printed in certain label.Note: Applicable only to ECOM26_84_A4_001, ECOM_TC_A4 and ECOM26_84CI_001 */
                labelCustomerDataText?: string;
                /** @description Declaration text that can be printed in certain shipment receipt template */
                shipmentReceiptCustomerDataText?: string;
            },
            'typeCode'
        >[];
        /** @description When set to true it will generate a single PDF or thermal output file for the Transport Label, a single PDF or thermal output file for the Waybill document and a single PDF file consisting of Commercial Invoice and Shipment Receipt. The default value is false, a single PDF or thermal output image file consists of Transport Label and single PDF or thermal output image file for Waybill Document will be returned in create shipment response. */
        splitTransportAndWaybillDocLabels?: boolean;
        /** @description When set to true it will generate a single PDF or thermal output image file consists of Transport Label, Waybill Document, Shipment Receipt and Commercial Invoice.<BR>          The default value is false, where a single PDF or thermal output image file consists of Transport Label + Waybill Document and single PDF or thermal output image file for Shipment Receipt and Customs Invoice will be returned. */
        allDocumentsInOneImage?: boolean;
        /** @description When set to true it will generate a single PDF or thermal output image file for each page for the Transport Label and single PDF or thermal output image file for Waybill Document will be returned in the create shipment response. The default value is false, a single PDF or thermal output image file for each page for Transport Label and single PDF or thermal output image file for Waybill Document will be returned in create shipment response. */
        splitDocumentsByPages?: boolean;
        /** @description When set to true it will generate a single PDF or thermal output image file consisting of Transport Label + Waybill Document, a single file consist of Commercial Invoice and a single file consist of Shipment Receipt. The default value is false, a single PDF or thermal output image file consists of Transport Label + Waybill Document and single PDF or thermal output image file for Shipment Receipt and Customs Invoice will be returned in create shipment response. */
        splitInvoiceAndReceipt?: boolean;
        /** @description When set to true it will generate a single PDF file consisting of Transport Label, Waybill Document and Shipment Receipt. The default value is false, a single PDF or thermal output image file consists of Transport Label + Waybill Document and single PDF file for Shipment Receipt will be returned in create shipment response.  Applicable only when #/outputImageProperties/imageOptions/0/typeCode is 'receipt' and #/outputImageProperties/encodingFormat is PDF. */
        receiptAndLabelsInOneImage?: boolean;
    };
    /** @description Here you can declare your customer references */
    customerReferences?: supermodelIoLogisticsExpressReference[];
    /** @description Identifiers section is on the shipment level where you can optionaly provide a DHL Express waybill number. This has to be enabled by your DHL Express IT contact. */
    identifiers?: supermodelIoLogisticsExpressIdentifier[];
    /** @description Here you need to define all the parties needed to ship the package */
    customerDetails: {
        /** @description Please enter address and contact details related to shipper */
        shipperDetails: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressBankDetails;
            /**
             * @description Please enter the business party role type of the shipper
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to receiver */
        receiverDetails: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressBankDetails;
            /**
             * @description Please enter the business party type of the receiver
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to buyer */
        buyerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContactBuyer;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressBankDetails;
            /**
             * @description Please enter the business party type of the buyer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to importer */
        importerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressBankDetails;
            /**
             * @description Please enter the business party type of the importer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to exporter */
        exporterDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressBankDetails;
            /**
             * @description Please enter the business party type of the exporter
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to seller */
        sellerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressBankDetails;
            /**
             * @description Please enter the business party role type of the seller
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to payer */
        payerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressBankDetails;
            /**
             * @description Please enter the business party role type of the payer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to manufacturer */
        manufacturerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressRegistrationNumbers;
            /** @description Please enter the business party role type of the manufacturer */
            typeCode?: string;
        };
        /** @description Please enter address and contact details related to ultimate consignee */
        ultimateConsigneeDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressRegistrationNumbers;
            /** @description Please enter the business party role type of the ultimate consignee */
            typeCode?: string;
        };
        /** @description Please enter address and contact details related to broker */
        brokerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddressCreateShipmentRequest;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            bankDetails?: supermodelIoLogisticsExpressRegistrationNumbers;
            /** @description Please enter the business party role type of the broker */
            typeCode?: string;
        };
    };
    /** @description Here you can define all the properties related to the content of the prospected shipment */
    content: {
        /** @description Here you can define properties per package */
        packages: supermodelIoLogisticsExpressPackage[];
        /** @description For customs purposes please advise if your shipment is dutiable (true) or non dutiable (false).Note:If the shipment is dutiable, exportDeclaration element must be provided. */
        isCustomsDeclarable: boolean;
        /**
         * @description For customs purposes please advise on declared value of the shipment
         * @example 150
         */
        declaredValue?: number;
        /**
         * @description For customs purposes please advise on declared value currency code of the shipment
         * @example CZK
         */
        declaredValueCurrency?: string;
        /** @description Here you can find all details related to export declaration */
        exportDeclaration?: {
            /** @description Please enter details for each export line item */
            lineItems: {
                /**
                 * @description Please provide line item number
                 * @example 1
                 */
                number: number;
                /**
                 * @description Please provide description of the line item
                 * @example line item description
                 */
                description: string;
                /**
                 * @description Please provide unit or article price line item value
                 * @example 150
                 */
                price: number;
                /** @description Please enter information about quantity for this line item */
                quantity: {
                    /**
                     * @description Please enter number of pieces in the line item
                     * @example 1
                     */
                    value: number;
                    /**
                     * @description Please provide correct unit of measurement<BR>                        <BR>                        Possible values;<BR>                        BOX Boxes<BR>                        2GM                               Centimeters<BR>                        2M3 Cubic Centimeters<BR>                        3M3 Cubic Feet<BR>                        M3 Cubic Meters<BR>                        DPR Dozen Pairs<BR>                        DOZ Dozen<BR>                        2NO Each<BR>                        PCS Pieces<BR>                        GM Grams<BR>                        GRS Gross<BR>                        KG Kilograms<BR>                        L Liters<BR>                        M Meters<BR>                        3GM Milligrams<BR>                        3L Milliliters<BR>                        X No Unit Required<BR>                        NO Number<BR>                        2KG Ounces<BR>                        PRS Pairs<BR>                        2L Gallons<BR>                        3KG Pounds<BR>                        CM2 Square Centimeters<BR>                        2M2 Square Feet<BR>                        3M2 Square Inches<BR>                        M2 Square Meters<BR>                        4M2 Square Yards<BR>                        3M Yards<BR>                        CM Centimeters<BR>                        CONE Cone<BR>                        CT Carat<BR>                        EA Each<BR>                        LBS Pounds<BR>                        RILL Rill<BR>                        ROLL Roll<BR>                        SET Set<BR>                        TU Time Unit<BR>                        YDS Yard
                     * @example BOX
                     * @enum {string}
                     */
                    unitOfMeasurement:
                        | 'BOX'
                        | '2GM'
                        | '2M3'
                        | '3M3'
                        | 'M3'
                        | 'DPR'
                        | 'DOZ'
                        | '2NO'
                        | 'PCS'
                        | 'GM'
                        | 'GRS'
                        | 'KG'
                        | 'L'
                        | 'M'
                        | '3GM'
                        | '3L'
                        | 'X'
                        | 'NO'
                        | '2KG'
                        | 'PRS'
                        | '2L'
                        | '3KG'
                        | 'CM2'
                        | '2M2'
                        | '3M2'
                        | 'M2'
                        | '4M2'
                        | '3M'
                        | 'CM'
                        | 'CONE'
                        | 'CT'
                        | 'EA'
                        | 'LBS'
                        | 'RILL'
                        | 'ROLL'
                        | 'SET'
                        | 'TU'
                        | 'YDS';
                };
                /** @description Please provide Commodity codes for the shipment at item line level */
                commodityCodes?: {
                    /**
                     * @description Please provide type of the commodity code
                     * @example outbound
                     * @enum {string}
                     */
                    typeCode: 'outbound' | 'inbound';
                    /**
                     * @description Please provide the commodity code
                     * @example 851713
                     */
                    value: string;
                }[];
                /**
                 * @description Please provide the reason for export
                 * @example permanent
                 * @enum {string}
                 */
                exportReasonType?:
                    | 'permanent'
                    | 'temporary'
                    | 'return'
                    | 'used_exhibition_goods_to_origin'
                    | 'intercompany_use'
                    | 'commercial_purpose_or_sale'
                    | 'personal_belongings_or_personal_use'
                    | 'sample'
                    | 'gift'
                    | 'return_to_origin'
                    | 'warranty_replacement'
                    | 'diplomatic_goods'
                    | 'defence_material';
                /**
                 * @description Please enter two letter ISO manufacturer country code
                 * @example CZ
                 */
                manufacturerCountry: string;
                /** @description Please enter the weight information for line item */
                weight: {
                    /**
                     * @description Please enter the net weight value
                     * @example 10
                     */
                    netValue: number;
                    /**
                     * @description Please enter the gross weight value
                     * @example 10
                     */
                    grossValue: number;
                };
                /** @description Please provide if the Taxes is paid for the line item */
                isTaxesPaid?: boolean;
                /** @description Please provide the additional information */
                additionalInformation?: string[];
                /** @description Please provide the Customer References for the line item */
                customerReferences?: {
                    /**
                     * @description Please provide the line item reference type code. Please refer to the YAML Reference Data Guide PDF file for valid enumeration values.
                     * @example AFE
                     * @enum {string}
                     */
                    typeCode:
                        | 'AFE'
                        | 'BRD'
                        | 'DGC'
                        | 'AAJ'
                        | 'INB'
                        | 'MAK'
                        | 'ALX'
                        | 'PAN'
                        | 'PON'
                        | 'ABW'
                        | 'SE'
                        | 'SON'
                        | 'OID'
                        | 'DTC'
                        | 'DTM'
                        | 'DTQ'
                        | 'DTR'
                        | 'ITR'
                        | 'MID'
                        | 'OED'
                        | 'OET'
                        | 'OOR'
                        | 'SME'
                        | 'USM'
                        | 'AAM'
                        | 'CFR'
                        | 'DOM'
                        | 'FOR'
                        | 'USG'
                        | 'MAT'
                        | 'NLR';
                    /**
                     * @description Please provide the line item reference
                     * @example custref123
                     */
                    value: string;
                }[];
                /** @description Please provide the customs documents details */
                customsDocuments?: {
                    /**
                     * @description Please provide the line item customs document type code
                     * @example 972
                     * @enum {string}
                     */
                    typeCode:
                        | '972'
                        | 'AHC'
                        | 'ALC'
                        | 'ATA'
                        | 'ATR'
                        | 'BEX'
                        | 'CHA'
                        | 'CHD'
                        | 'CHP'
                        | 'CIT'
                        | 'CIV'
                        | 'CI2'
                        | 'COO'
                        | 'CPA'
                        | 'CRL'
                        | 'CSD'
                        | 'DEX'
                        | 'DGD'
                        | 'DLI'
                        | 'DOV'
                        | 'ELP'
                        | 'EU1'
                        | 'EU2'
                        | 'EUS'
                        | 'EXL'
                        | 'FMA'
                        | 'HLC'
                        | 'HWB'
                        | 'INV'
                        | 'IPA'
                        | 'JLC'
                        | 'LIC'
                        | 'LNP'
                        | 'NID'
                        | 'PAS'
                        | 'PFI'
                        | 'PHY'
                        | 'PLI'
                        | 'POA'
                        | 'PCH'
                        | 'ROD'
                        | 'T2M'
                        | 'TAD'
                        | 'TCS'
                        | 'VET'
                        | 'VEX';
                    /**
                     * @description Please provide the line item customs document ID
                     * @example custdoc456
                     */
                    value: string;
                }[];
                /**
                 * @description Please provide monetary value of the line item x quantity
                 * @example 150
                 */
                preCalculatedLineItemTotalValue?: number;
            }[];
            /** @description Please provide invoice related information */
            invoice?: {
                /**
                 * @description Please enter commercial invoice number
                 * @example 12345-ABC
                 */
                number: string;
                /**
                 * Format: date
                 * @description Please enter commercial invoice date
                 * @example 2020-03-18
                 */
                date: string;
                /**
                 * @description Please enter who has signed the invoce
                 * @example Brewer
                 */
                signatureName?: string;
                /**
                 * @description Please provide title of person who has signed the invoice
                 * @example Mr.
                 */
                signatureTitle?: string;
                /**
                 * @description Please provide the signature image
                 * @example Base64 encoded image
                 */
                signatureImage?: string;
                /** @description Shipment instructions for customs invoice printing purposes. Printed only when using Customs Invoice template COMMERCIAL_INVOICE_04. If using Customs Invoice template 			COMMERCIAL_INVOICE_04, recommended max length is 120 characters. */
                instructions?: string[];
                /** @description Customer data text to be printed in<BR>                  customs invoice.<BR>                  Printed only when using Customs<BR>                  Invoice template<BR>                  COMMERCIAL_INVOICE_04. */
                customerDataTextEntries?: string[];
                /** @description Please provide the total net weight */
                totalNetWeight?: number;
                /** @description Please provide the total gross weight */
                totalGrossWeight?: number;
                /** @description Please provide the customer references at invoice level. It is recommended to provide less than 20 customer references of 'MRN' type code. */
                customerReferences?: {
                    /**
                     * @description Please provide the invoice reference type code
                     * @example CU
                     * @enum {string}
                     */
                    typeCode:
                        | 'ACL'
                        | 'CID'
                        | 'CN'
                        | 'CU'
                        | 'ITN'
                        | 'UCN'
                        | 'MRN'
                        | 'OID'
                        | 'PON'
                        | 'RMA'
                        | 'AAM'
                        | 'ABT'
                        | 'ADA'
                        | 'AES'
                        | 'AFD'
                        | 'ANT'
                        | 'BKN'
                        | 'BOL'
                        | 'CDN'
                        | 'COD'
                        | 'DSC'
                        | 'FF'
                        | 'FN'
                        | 'FTR'
                        | 'HWB'
                        | 'IBC'
                        | 'IPP'
                        | 'LLR'
                        | 'MAB'
                        | 'MWB'
                        | 'OBC'
                        | 'PD'
                        | 'PRN'
                        | 'RTL'
                        | 'SID'
                        | 'SS'
                        | 'SWN';
                    /**
                     * @description Please provide the invoice reference
                     * @example custref112
                     */
                    value: string;
                }[];
                /**
                 * @description Please provide the terms of payment
                 * @example 100 days
                 */
                termsOfPayment?: string;
                /** @description indicativeCustomsValues contains child nodes importCustomsDutyValue and importTaxesValue.<BR>                  <BR>                  These 2 child elements are only applicable for Commercial Invoice printing purpose in Customs Invoice template*: COMMERCIAL_INVOICE_P_10 and COMMERCIAL_INVOICE_L_10.<BR>                  If any of this child nodes are present, it will only be able to display up to three OtherCharges. <BR>                  <BR>                  Nonetheless, the ShipmentRequest can still contain up to five additionalCharges.<BR>                  If there are more than three additionalCharges, the third additionalCharges onwards will be combined and displayed under one single caption of 'Other Charges'.<BR>                  <BR>                  Note: If either first or second additionalCharges has typeCode of 'other', and there are more than three additionalCharges provided in the request, the additionalCharges with typeCode of 'other' will be consolidated under the combined 'Other Charges' caption as well. */
                indicativeCustomsValues?: {
                    /**
                     * @description Please provide the pre-calculated import customs duties value for the shipment
                     * @example 150.57
                     */
                    importCustomsDutyValue?: number;
                    /**
                     * @description Please provide the pre-calculated import taxes (VAT/GST) value for the shipment
                     * @example 49.43
                     */
                    importTaxesValue?: number;
                    /**
                     * @description Please provide pre-calculated total of all line items plus additional charges plus indicativeCustomsValues
                     * @example [
                     *   350.57
                     * ]
                     */
                    totalWithImportDutiesAndTaxes?: number;
                };
                /** @description Please provide pre-calculated total values */
                preCalculatedTotalValues?: {
                    /**
                     * @description Please provide the pre-calculated total value of all line items
                     * @example 49.43
                     */
                    preCalculatedTotalGoodsValue: number;
                    /**
                     * @description Please provide the total value of all line items plus additional charges if any
                     * @example 150.57
                     */
                    preCalculatedTotalInvoiceValue: number;
                };
            };
            /** @description Please enter up to three remarks. <BR>              If using Customs Invoice template COMMERCIAL_INVOICE_04, the invoice can only print the first remarks field. The recommended max length is 20 characters. <BR>              If using Customs Invoice template COMMERCIAL_INVOICE_L_10 or COMMERCIAL_INVOICE_P_10, the invoice can print all three remraks fields.  The recommended max length is 45 characters. */
            remarks?: {
                /**
                 * @description Please enter remark text
                 * @example declaration remark
                 */
                value: string;
            }[];
            /** @description Please enter additional charge to appear on the invoice<BR>              admin, Administration Charge<BR>              delivery, Delivery Charge<BR>              documentation, Documentation Charge<BR>              expedite, Expedite Charge<BR>              export, Export Charge<BR>              freight, Freight Charge<BR>              fuel_surcharge, Fuel Surcharge<BR>              logistic, Logistic Charge<BR>              other, Other Charge<BR>              packaging, Packaging Charge<BR>              pickup, Pickup Charge<BR>              handling, Handling Charge<BR>              vat, VAT Charge<BR>              insurance, Insurance Cost<BR>              reverse_charge, Reverse Charge */
            additionalCharges?: {
                /**
                 * @description Please provide the charge value
                 * @example 10
                 */
                value: number;
                /**
                 * @description Please enter charge caption
                 * @example fee
                 */
                caption?: string;
                /**
                 * @description Please enter charge type
                 * @example freight
                 * @enum {string}
                 */
                typeCode:
                    | 'admin'
                    | 'delivery'
                    | 'documentation'
                    | 'expedite'
                    | 'export'
                    | 'freight'
                    | 'fuel_surcharge'
                    | 'logistic'
                    | 'other'
                    | 'packaging'
                    | 'pickup'
                    | 'handling'
                    | 'vat'
                    | 'insurance'
                    | 'reverse_charge';
            }[];
            /**
             * @description Please provide destination port details
             * @example port details
             */
            destinationPortName?: string;
            /**
             * @description Name of port of departure, shipment or destination as required under the applicable delivery term.
             * @example port of departure or destination details
             */
            placeOfIncoterm?: string;
            /**
             * @description Please provide Payer VAT number
             * @example 12345ED
             */
            payerVATNumber?: string;
            /**
             * @description Please enter recipient reference
             * @example recipient reference
             */
            recipientReference?: string;
            /** @description Exporter related details */
            exporter?: {
                /**
                 * @description Please provide exporter Id
                 * @example 123
                 */
                id?: string;
                /**
                 * @description Please provide exporter code
                 * @example EXPCZ
                 */
                code?: string;
            };
            /**
             * @description Please enter package marks
             * @example marks
             */
            packageMarks?: string;
            /** @description Please provide up to three dcelaration notes */
            declarationNotes?: {
                /** @example up to three declaration notes */
                value: string;
            }[];
            /**
             * @description Please enter export reference
             * @example export reference
             */
            exportReference?: string;
            /**
             * @description Please enter export reason
             * @example export reason
             */
            exportReason?: string;
            /**
             * @description Please provide the reason for export
             * @example permanent
             * @enum {string}
             */
            exportReasonType?:
                | 'permanent'
                | 'temporary'
                | 'return'
                | 'used_exhibition_goods_to_origin'
                | 'intercompany_use'
                | 'commercial_purpose_or_sale'
                | 'personal_belongings_or_personal_use'
                | 'sample'
                | 'gift'
                | 'return_to_origin'
                | 'warranty_replacement'
                | 'diplomatic_goods'
                | 'defence_material';
            /** @description Please provide details about export and import licenses */
            licenses?: {
                /**
                 * @description Please provide type of the license
                 * @enum {string}
                 */
                typeCode: 'export' | 'import';
                /**
                 * @description Please provide the license
                 * @example license
                 */
                value: string;
            }[];
            /**
             * @description Please provide the shipment was sent for Personal (Gift) or Commercial (Sale) reasons
             * @example personal
             * @enum {string}
             */
            shipmentType?: 'personal' | 'commercial';
            /** @description Please provide the Customs Documents at invoice level */
            customsDocuments?: {
                /**
                 * @description Please provide the Customs Document type code at invoice level
                 * @example 972
                 * @enum {string}
                 */
                typeCode:
                    | '972'
                    | 'AHC'
                    | 'ALC'
                    | 'ATA'
                    | 'ATR'
                    | 'BEX'
                    | 'CHA'
                    | 'CHD'
                    | 'CHP'
                    | 'CIT'
                    | 'CIV'
                    | 'CI2'
                    | 'COO'
                    | 'CPA'
                    | 'CRL'
                    | 'CSD'
                    | 'DEX'
                    | 'DGD'
                    | 'DLI'
                    | 'DOV'
                    | 'ELP'
                    | 'EU1'
                    | 'EU2'
                    | 'EUS'
                    | 'EXL'
                    | 'FMA'
                    | 'HLC'
                    | 'HWB'
                    | 'INV'
                    | 'IPA'
                    | 'JLC'
                    | 'LIC'
                    | 'LNP'
                    | 'NID'
                    | 'PAS'
                    | 'PFI'
                    | 'PHY'
                    | 'PLI'
                    | 'POA'
                    | 'PCH'
                    | 'ROD'
                    | 'T2M'
                    | 'TAD'
                    | 'TCS'
                    | 'VET'
                    | 'VEX';
                /**
                 * @description Please provide the Customs Document ID at invoice level
                 * @example custdoc445
                 */
                value: string;
            }[];
        };
        /**
         * @description Please enter description of your shipment
         * @example shipment description
         */
        description: string;
        /**
         * @description This is used for the US AES4, FTR and ITN numbers to be printed on the Transport Label
         * @example 12345
         */
        USFilingTypeValue?: string;
        /**
         * @description The Incoterms rules are a globally-recognized set of standards, used worldwide in international and domestic contracts for the delivery of goods, illustrating responsibilities between buyer and seller for costs and risk, as well as cargo insurance.<BR>          EXW ExWorks<BR>          FCA Free Carrier<BR>          CPT Carriage Paid To<BR>          CIP Carriage and Insurance Paid To<BR>          DPU Delivered at Place Unloaded<BR>          DAP Delivered at Place<BR>          DDP Delivered Duty Paid<BR>          FAS Free Alongside Ship<BR>          FOB Free on Board<BR>          CFR Cost and Freight<BR>          CIF Cost, Insurance and Freight<BR>          DAF Delivered at Frontier<BR>          DAT Delivered at Terminal<BR>          DDU Delivered Duty Unpaid<BR>          DEQ Delivery ex Quay<BR>          DES Delivered ex Ship
         * @example DAP
         * @enum {string}
         */
        incoterm: 'EXW' | 'FCA' | 'CPT' | 'CIP' | 'DPU' | 'DAP' | 'DDP' | 'FAS' | 'FOB' | 'CFR' | 'CIF' | 'DAF' | 'DAT' | 'DDU' | 'DEQ' | 'DES';
        /**
         * @description Please enter Unit of measurement - metric,imperial
         * @example metric
         * @enum {string}
         */
        unitOfMeasurement: 'metric' | 'imperial';
    };
    documentImages?: supermodelIoLogisticsExpressDocumentImages;
    /** @description Here you can provide data in case you wish to use DHL Express On demand delivery service */
    onDemandDelivery?: WithRequired<
        {
            /**
             * @description Please choose from one of the delivery options
             * @enum {string}
             */
            deliveryOption: 'servicepoint' | 'neighbour' | 'signatureRelease';
            /**
             * @description If delivery option is signatureDelivery please specify location where to leave the shipment
             * @example front door
             */
            location?: string;
            /**
             * @description Please enter additional information that might be useful for the DHL Express courier. This field is conditionally mandatory if selected location is 'Other'.
             * @example ringe twice
             */
            specialInstructions?: string;
            /**
             * @description Please provide entry code to gain access to an apartment complex or gate
             * @example 1234
             */
            gateCode?: string;
            /**
             * @description In ase your deliveryOption is 'neighbour' please specify where to leave the package
             * @enum {string}
             */
            whereToLeave?: 'concierge' | 'neighbour';
            /**
             * @description In case you wish to leave the package with neighbour please provide the neighbour's name
             * @example Mr.Dan
             */
            neighbourName?: string;
            /**
             * @description In case you wish to leave the package with neighbour please provide the neighbour's house number
             * @example 777
             */
            neighbourHouseNumber?: string;
            /**
             * @description In case your delivery option is 'signatureRelease' please provide name of the person who is authorized to sign and receive the package
             * @example Newman
             */
            authorizerName?: string;
            /**
             * @description In case your delivery option is 'servicepoint' please provide unique DHL Express Service point location ID of where the parcel should be delieverd (please contact your local DHL Express Account Manager to obtain the list of the servicepoint IDs)
             * @example SPL123
             */
            servicePointId?: string;
            /**
             * @description for future use
             * @example 2020-04-20
             */
            requestedDeliveryDate?: string;
        },
        'deliveryOption'
    >;
    /**
     * @description Determines whether to request the On Demand Delivery (ODD) link. When set to true it will provide an URL link for the specified Waybill Number, Shipper Account Number. The default value is false, no ODD link URL is provided in the response message.
     * @example false
     */
    requestOndemandDeliveryURL?: boolean;
    /** @description This is to support sending email notification once the shipment is created. The email will contain the basic information on the shipper, recipient, waybill number, and shipment information */
    shipmentNotification?: {
        /**
         * @description Please enter channel type to send the notification by. At this moment only email is supported
         * @enum {string}
         */
        typeCode: 'email';
        /**
         * @description Please enter notification receiver email address
         * @example receiver@email.com
         */
        receiverId: string;
        /**
         * @description Please enter three letter lanuage code in which you wish to receive the notification in
         * @default eng
         * @example eng
         */
        languageCode?: string;
        /**
         * @description Please enter two letter language country code
         * @default UK
         * @example UK
         */
        languageCountryCode?: string;
        /**
         * @description Please enter your message which will be added to the DHL Express notification email
         * @example message to be included in the notification
         */
        bespokeMessage?: string;
    }[];
    /** @description Please provide any charges you have already paid for this shipment, like freight paid upfront. To allow using this section please contact your DHL Express representative */
    prepaidCharges?: {
        /**
         * @description Please enter type of prepaid charge. At this moment only freight is supported
         * @enum {string}
         */
        typeCode: 'freight';
        /**
         * @description Please enter currency for the value you have entered into value field
         * @example CZK
         */
        currency: string;
        /**
         * @description Please enter nominal value related to the charge
         * @example 200
         */
        value: number;
        /**
         * @description Please enter method you have used to pay the charges. At this moment only cash is supported
         * @enum {string}
         */
        method: 'cash';
    }[];
    /**
     * @description If set to true, response will return transliterated text of shipper and receiver details.
     * @example false
     */
    getTransliteratedResponse?: boolean;
    /**
     * estimated delivery date
     * @description Estimated delivery date option for QDDF or QDDC.
     */
    estimatedDeliveryDate?: {
        /**
         * @description Please indicate if requesting to get EDD for this shipment.
         *  Estimated Delivery Date Type. QDDF: is the fastest transit time as quoted to the customer at booking or shipment creation. When clearance or any other non-transport operational component is expected to impact transit time, QDDF does not constitute DHL's service commitment. QDDC: cconstitutes DHL's service commitment as quoted at booking or shipment creation. QDDC builds in clearance time, and potentially other special operational non-transport component(s), when relevant.
         * @default false
         * @example false
         */
        isRequested: boolean;
        /**
         * @description Please indicate the EDD type being requested
         * @example QDDC
         * @enum {string}
         */
        typeCode?: 'QDDC' | 'QDDF';
    };
    /** @description Provides additional information in the response like service area details, routing code and pickup-related information */
    getAdditionalInformation?: {
        /**
         * @description Provide type code of data that can be returned in response.  Values can be pickupDetails, optionalShipmentData, transliterateResponse and linkLabelsByPieces (link label image generated for each package level)
         * @enum {string}
         */
        typeCode?: 'pickupDetails' | 'optionalShipmentData' | 'barcodeInformation' | 'linkLabelsByPieces';
        isRequested: boolean;
    }[];
    /** @description Please provide the parent (mother) shipment details */
    parentShipment?: {
        /** @description Please provide the parent (mother) Product Code */
        productCode?: string;
        /** @description Please provide the parent (mother) shipment's Number of Packages */
        packagesCount?: number;
    };
};

/**
 * Definition of /shipments response message
 * @description MyDHL API REST /shipments response schema
 */
export type supermodelIoLogisticsExpressCreateShipmentResponse = {
    /**
     * @description URL where the request has been sent to
     * @example https://express.api.dhl.com/mydhlapi/shipments
     */
    url?: string;
    /**
     * @description Here you will receive Shipment Identification Number of your package
     * @example 123456790
     */
    shipmentTrackingNumber?: string;
    /**
     * @description If you requested pickup for your shipment you can use this URL to cancel the pickup
     * @example https://express.api.dhl.com/mydhlapi/shipment-pickups/PRG200227000256
     */
    cancelPickupUrl?: string;
    /**
     * @description You can use ths URL to track your shipment
     * @example https://express.api.dhl.com/mydhlapi/shipments/1234567890/tracking
     */
    trackingUrl?: string;
    /**
     * @description If you asked for pickup service here you will find Dispach Confirmation Number which identifies your pickup booking
     * @example PRG200227000256
     */
    dispatchConfirmationNumber?: string;
    /** @description Here you can find information for all pieces your shipment is having like Piece Identification Number */
    packages?: {
        /**
         * @description Piece serial number
         * @example 1
         */
        referenceNumber?: number;
        /**
         * @description Here is provided each piece its Identification number
         * @example JD914600003889482921
         */
        trackingNumber: string;
        /**
         * @description You can use ths URL to track your shipment by Piece Identification Number
         * @example https://express.api.dhl.com/mydhlapi/shipments/1234567890/tracking?PieceID=JD014600003889482921
         */
        trackingUrl?: string;
        /**
         * @description Here is provided each piece volumetric/ dimensional weight
         * @example 2.5
         */
        volumetricWeight?: number;
        /** @description Here you can find all documents created for the piece's QRcode */
        documents?: {
            /**
             * @description Identifies image format the document is created in, like PNG etc.
             * @example PNG
             */
            imageFormat: string;
            /**
             * @description Contains base64 encoded document image
             * @example base64 encoded document
             */
            content: string;
            /**
             * @description Identifie type of the QR code
             * @example qr-code
             */
            typeCode: string;
        }[];
    }[];
    /** @description Here you can find all documents created for the shipment like Transport and WaybillDoc labels, Invoice, Receipt */
    documents?: {
        /**
         * @description Identifie image format the document is created in, like PDF, JPG etc.
         * @example PDF
         */
        imageFormat: string;
        /**
         * @description Contains base64 encoded document image
         * @example base64 encoded document
         */
        content: string;
        /**
         * @description Identifie type of the document, like invoice, label or receipt
         * @example label
         */
        typeCode: string;
        /**
         * @description Package reference number
         * @example 1
         */
        packageReferenceNumber?: number;
    }[];
    /**
     * @description In this field you will find the On Demand Delivery (ODD) URL link if requested
     * @example https://odd-test.dhl.com/odd-online/US/wH24aaaaa1
     */
    onDemandDeliveryURL?: string;
    /** @description Here you can find additional information related to your shipment when you ask for it in the request */
    shipmentDetails?: {
        /** @description This array contains all the DHL Express special handling feature codes */
        serviceHandlingFeatureCodes?: string[];
        /**
         * @description Here you can find calculated volumetric weight based on dimensions provided in the request
         * @example 245.56
         */
        volumetricWeight?: number;
        /**
         * @description Here you can find billing code which was applied on your shipment
         * @example IMP
         */
        billingCode?: string;
        /**
         * @description Here you can find the DHL Express shipment content code of your shipment
         * @example WPX
         */
        serviceContentCode?: string;
        /** @description Here you need to define all the parties needed to ship the package */
        customerDetails?: {
            shipperDetails?: {
                postalAddress?: supermodelIoLogisticsExpressAddressCreateShipmentResponse;
                contactInformation?: supermodelIoLogisticsExpressContactCreateShipmentResponse;
            };
            receiverDetails?: {
                postalAddress?: supermodelIoLogisticsExpressAddressCreateShipmentResponse;
                contactInformation?: supermodelIoLogisticsExpressContactCreateShipmentResponse;
            };
        };
        originServiceArea?: {
            /** @example TSS */
            facilityCode?: string;
            /** @example ZYP */
            serviceAreaCode?: string;
            outboundSortCode?: string;
        };
        destinationServiceArea?: {
            /** @example GUM */
            facilityCode?: string;
            /** @example GUM */
            serviceAreaCode?: string;
            inboundSortCode?: string;
        };
        /**
         * @description Here you can find DHL Routing Code which was applied on your shipment
         * @example GBSE186PJ+48500001
         */
        dhlRoutingCode?: string;
        /**
         * @description Here you can find DHL Routing Data ID which was applied on your shipment
         * @example 2L
         */
        dhlRoutingDataId?: string;
        /**
         * @description Here you can find Delivery Date Code which was applied on your shipment
         * @example S
         */
        deliveryDateCode?: string;
        /**
         * @description Here you can find Delivery Time Code which was applied on your shipment
         * @example X09
         */
        deliveryTimeCode?: string;
        /**
         * @description Here you can find the product short name of your shipment
         * @example EXPRESS WORLDWIDE
         */
        productShortName?: string;
        valueAddedServices?: {
            /** @example II */
            serviceCode: string;
            /** @example INSURANCE */
            description?: string;
        }[];
        /** @description Here you can find pickup details */
        pickupDetails?: {
            /**
             * @description Pickup booking cutoff time
             * @example 2021-10-04T16:30:00
             */
            localCutoffDateAndTime?: string;
            /**
             * @description Pickup cutoff time in origin's time zone
             * @example 17:00:00
             */
            gmtCutoffTime?: string;
            /**
             * @description Pickup booking cutoff time in GMT offset
             * @example PT30M
             */
            cutoffTimeOffset?: string;
            /**
             * @description The DHL earliest time possible for pickup
             * @example 09:00:00
             */
            pickupEarliest?: string;
            /**
             * @description The DHL latest time possible for pickup
             * @example 17:00:00
             */
            pickupLatest?: string;
            /**
             * @description The number of transit days
             * @example 8
             */
            totalTransitDays?: string;
            /**
             * @description This is additional transit delays (in days) for shipment picked up from the mentioned city or postal area to arrival at the service area
             * @example 0
             */
            pickupAdditionalDays?: string;
            /**
             * @description This is additional transit delays (in days) for shipment delivered to the mentioned city or postal area following arrival at the service area
             * @example 0
             */
            deliveryAdditionalDays?: string;
            /**
             * @description Pickup day of the week number
             * @example 1
             */
            pickupDayOfWeek?: string;
            /**
             * @description Destination day of the week number
             * @example 2
             */
            deliveryDayOfWeek?: string;
        };
    }[];
    /** @description Here you can find rates related to your shipment */
    shipmentCharges?: {
        /**
         * @description Possible Values :
         * - 'BILLC', billing currency
         * - 'PULCL', country public rates currency
         * - 'BASEC', base currency
         * @example BILLC
         */
        currencyType: string;
        /**
         * @description This the currency of the rated shipment for the prices listed.
         * @example USD
         */
        priceCurrency: string;
        /**
         * @description The amount price of DHL product and services
         * @example 147
         */
        price: number;
        serviceBreakdown?: {
            /** @example EXPRESS WORLDWIDE */
            name: string;
            /**
             * @description The amount price of DHL product and services
             * @example 147
             */
            price: number;
            /**
             * @description Special service charge code type for service.
             * @example FF
             */
            typeCode?: string;
        }[];
    }[];
    /** @description Here you can find barcode details in base64 */
    barcodeInfo?: {
        /**
         * @description Barcode base64 encoded airwaybill number
         * @example base64 encoded airwaybill number
         */
        shipmentIdentificationNumberBarcodeContent?: string;
        /**
         * @description Barcode base64 image of origin service area code, destination service area code and global product code
         * @example base64 encoded string
         */
        originDestinationServiceTypeBarcodeContent?: string;
        /**
         * @description Barcode base64 image of DHL routing code
         * @example base64 encoded string
         */
        routingBarcodeContent?: string;
        /** @description Here you can find barcode details for each piece */
        trackingNumberBarcodes?: {
            /**
             * @description Piece serial number
             * @example 1
             */
            referenceNumber?: number;
            /**
             * @description Barcode base4 image of each piece of the shipment
             * @example base64 encoded string
             */
            trackingNumberBarcodeContent?: string;
        }[];
    };
    /** @description Here you can find details of estimated delivery date */
    estimatedDeliveryDate?: {
        /** @example 2021-09-27 */
        estimatedDeliveryDate?: string;
        /**
         * @description EDD type
         * @example QDDC
         */
        estimatedDeliveryType?: string;
    };
    warnings?: string[];
};

/**
 * Get Image Response definition
 * @description MyDHL API REST document image response schema
 */
export type supermodelIoLogisticsExpressDocumentImageResponse = {
    /** @description Here you can find all document images from search query */
    documents?: {
        /**
         * @description Shipment Tracking Number
         * @example 1234567890
         */
        shipmentTrackingNumber: string;
        /**
         * @description Identifies type of the document like commercial invoice or waybill, or archived zip documents
         * @example waybill
         */
        typeCode: string;
        /**
         * @description Clearance code or document function whether for import, export or both.  Returned only for customs-entry
         * @example import
         * @enum {string}
         */
        function?: 'import' | 'export' | 'both';
        /**
         * @description Identifies image format the document is created in, like PDF, TIFF, or ZIP
         * @example PDF
         */
        encodingFormat: string;
        /**
         * @description Contains base64 encoded document image or archived zip
         * @example base64 encoded document
         */
        content: string;
    }[];
};

/**
 * Get Image request definition
 * @description This section is to support multiple base64 encoded string with the image of export documentation for Paperless Trade images. When an invalid base64 encoded string is provided, an error message will be returned
 */
export type supermodelIoLogisticsExpressDocumentImages = Array<{
    /**
     * @description Please provide correct document type you wish to upload<BR>        <BR>        Possible values;<BR>        INV, Invoice<BR>        PNV, Proforma<BR>        COO, Certificate of Origin<BR>        NAF, Nafta Certificate of Origin<BR>        CIN, Commercial Invoice<BR>        DCL, Custom Declaration<BR>        AWB, Air Waybill and Waybill Document
     * @default INV
     * @example INV
     * @enum {string}
     */
    typeCode?: 'INV' | 'PNV' | 'COO' | 'CIN' | 'DCL' | 'AWB' | 'NAF';
    /**
     * @description Please provide the image file format for the document you want to upload
     * @default PDF
     * @example PDF
     * @enum {string}
     */
    imageFormat?: 'PDF' | 'PNG' | 'GIF' | 'TIFF' | 'JPEG';
    /**
     * @description Please provide the base64 encoded document
     * @example base64 encoded image
     */
    content: string;
}>;

/**
 * Export Declaration definition details
 * @description Here you can find all details related to export declaration
 */
export type supermodelIoLogisticsExpressExportDeclaration = {
    /** @description Please enter details for each export line item */
    lineItems: {
        /**
         * @description Please provide line item number
         * @example 1
         */
        number: number;
        /**
         * @description Please provide description of the line item
         * @example line item description
         */
        description: string;
        /**
         * @description Please provide unit or article price line item value
         * @example 150
         */
        price: number;
        /** @description Please enter information about quantity for this line item */
        quantity: {
            /**
             * @description Please enter number of pieces in the line item
             * @example 1
             */
            value: number;
            /**
             * @description Please provide correct unit of measurement<BR>                <BR>                Possible values;<BR>                BOX Boxes<BR>                2GM Centigram<BR>              2M3 Cubic Centimeters<BR>                3M3 Cubic Feet<BR>                M3 Cubic Meters<BR>                DPR Dozen Pairs<BR>                DOZ Dozen<BR>                2NO Each<BR>                PCS Pieces<BR>                GM Grams<BR>                GRS Gross<BR>                KG Kilograms<BR>                L Liters<BR>                M Meters<BR>                3GM Milligrams<BR>                3L Milliliters<BR>                X No Unit Required<BR>                NO Number<BR>                2KG Ounces<BR>                PRS Pairs<BR>                2L Gallons<BR>                3KG Pounds<BR>                CM2 Square Centimeters<BR>                2M2 Square Feet<BR>                3M2 Square Inches<BR>                M2 Square Meters<BR>                4M2 Square Yards<BR>                3M Yards<BR>                CM Centimeters<BR>                CONE Cone<BR>                CT Carat<BR>                EA Each<BR>                LBS Pounds<BR>                RILL Rill<BR>                ROLL Roll<BR>                SET Set<BR>                TU Time Unit<BR>                YDS Yard
             * @example BOX
             * @enum {string}
             */
            unitOfMeasurement:
                | 'BOX'
                | '2GM'
                | '2M3'
                | '3M3'
                | 'M3'
                | 'DPR'
                | 'DOZ'
                | '2NO'
                | 'PCS'
                | 'GM'
                | 'GRS'
                | 'KG'
                | 'L'
                | 'M'
                | '3GM'
                | '3L'
                | 'X'
                | 'NO'
                | '2KG'
                | 'PRS'
                | '2L'
                | '3KG'
                | 'CM2'
                | '2M2'
                | '3M2'
                | 'M2'
                | '4M2'
                | '3M'
                | 'CM'
                | 'CONE'
                | 'CT'
                | 'EA'
                | 'LBS'
                | 'RILL'
                | 'ROLL'
                | 'SET'
                | 'TU'
                | 'YDS';
        };
        /** @description Please provide Commodity codes for the shipment at item line level */
        commodityCodes?: {
            /**
             * @description Please provide type of the commodity code
             * @example outbound
             * @enum {string}
             */
            typeCode: 'outbound' | 'inbound';
            /**
             * @description Please provide the commodity code
             * @example 851713
             */
            value: string;
        }[];
        /**
         * @description Please provide the reason for export
         * @enum {string}
         */
        exportReasonType?:
            | 'permanent'
            | 'temporary'
            | 'return'
            | 'used_exhibition_goods_to_origin'
            | 'intercompany_use'
            | 'commercial_purpose_or_sale'
            | 'personal_belongings_or_personal_use'
            | 'sample'
            | 'gift'
            | 'return_to_origin'
            | 'warranty_replacement'
            | 'diplomatic_goods'
            | 'defence_material';
        /**
         * @description Please enter two letter ISO manufacturer country code
         * @example CZ
         */
        manufacturerCountry: string;
        /** @description Please enter the weight information for line item.  Either a netValue or grossValue must be provided for the line item. */
        weight: {
            /**
             * @description Please enter the net weight value
             * @example 10
             */
            netValue?: number;
            /**
             * @description Please enter the gross weight value
             * @example 10
             */
            grossValue?: number;
        } & (
            | {
                  /**
                   * @description Please enter the net weight value
                   * @example 10
                   */
                  netValue: number;
              }
            | {
                  /**
                   * @description Please enter the gross weight value
                   * @example 10
                   */
                  grossValue: number;
              }
        );
        /** @description Please provide if the Taxes is paid for the line item */
        isTaxesPaid?: boolean;
        /** @description Please provide the Customer References for the line item */
        customerReferences?: {
            /**
             * @description Please provide the line item reference type code. Please refer to the YAML Reference Data Guide PDF file for valid enumeration values.
             * @example AFE
             * @enum {string}
             */
            typeCode:
                | 'AFE'
                | 'BRD'
                | 'DGC'
                | 'AAJ'
                | 'INB'
                | 'MAK'
                | 'ALX'
                | 'PAN'
                | 'PON'
                | 'ABW'
                | 'SE'
                | 'SON'
                | 'OID'
                | 'DTC'
                | 'DTM'
                | 'DTQ'
                | 'DTR'
                | 'ITR'
                | 'MID'
                | 'OED'
                | 'OET'
                | 'OOR'
                | 'SME'
                | 'USM'
                | 'AAM'
                | 'CFR'
                | 'DOM'
                | 'FOR'
                | 'USG'
                | 'MAT'
                | 'NLR';
            /**
             * @description Please provide the line item reference
             * @example customerref1
             */
            value: string;
        }[];
        /** @description Please provide the customs documents details */
        customsDocuments?: {
            /**
             * @description Please provide the line item customs document type code. Please refer to the YAML Reference Data Guide PDF file for valid enumeration values.
             * @example 972
             * @enum {string}
             */
            typeCode:
                | '972'
                | 'AHC'
                | 'ALC'
                | 'ATA'
                | 'ATR'
                | 'BEX'
                | 'CHA'
                | 'CHD'
                | 'CHP'
                | 'CIT'
                | 'CIV'
                | 'CI2'
                | 'COO'
                | 'CPA'
                | 'CRL'
                | 'CSD'
                | 'DEX'
                | 'DGD'
                | 'DLI'
                | 'DOV'
                | 'ELP'
                | 'EU1'
                | 'EU2'
                | 'EUS'
                | 'EXL'
                | 'FMA'
                | 'HLC'
                | 'HWB'
                | 'INV'
                | 'IPA'
                | 'JLC'
                | 'LIC'
                | 'LNP'
                | 'NID'
                | 'PAS'
                | 'PFI'
                | 'PHY'
                | 'PLI'
                | 'POA'
                | 'PCH'
                | 'ROD'
                | 'T2M'
                | 'TAD'
                | 'TCS'
                | 'VET'
                | 'VEX';
            /**
             * @description Please provide the line item customs document ID
             * @example custdoc456
             */
            value: string;
        }[];
        /**
         * @description Please provide monetary value of the line item x quantity
         * @example 150
         */
        preCalculatedLineItemTotalValue?: number;
    }[];
    /** @description Please provide invoice related information */
    invoice: {
        /**
         * @description Please enter commercial invoice number
         * @example 12345-ABC
         */
        number: string;
        /**
         * @description Please enter commercial invoice date
         * @example 2021-03-18
         */
        date: string;
        /**
         * @description Please provide the purpose was the document details captured and are planned to be used. Note: export and import is only applicable for approve Sale In Transit customers
         * @example import
         * @enum {string}
         */
        function: 'import' | 'export' | 'both';
        /** @description Please provide the customer references at invoice level.  
  Note: customerReference/0/value with typeCode 'CU' is mandatory if using POST method and no shipmentTrackingNumber is provided in request. It is recommended to provide less than 20 customer references of 'MRN' type code */
        customerReferences?: {
            /**
             * @description Please provide the invoice reference type code
             * @example CU
             * @enum {string}
             */
            typeCode:
                | 'ACL'
                | 'CID'
                | 'CN'
                | 'CU'
                | 'ITN'
                | 'MRN'
                | 'OID'
                | 'PON'
                | 'RMA'
                | 'UCN'
                | 'AAM'
                | 'ABT'
                | 'ADA'
                | 'AES'
                | 'AFD'
                | 'ANT'
                | 'BKN'
                | 'BOL'
                | 'CDN'
                | 'COD'
                | 'DSC'
                | 'FF'
                | 'FN'
                | 'FTR'
                | 'HWB'
                | 'IBC'
                | 'IPP'
                | 'LLR'
                | 'MAB'
                | 'MWB'
                | 'OBC'
                | 'PD'
                | 'PRN'
                | 'RTL'
                | 'SID'
                | 'SS'
                | 'SWN';
            /**
             * @description Please provide the invoice reference
             * @example custref112
             */
            value: string;
        }[];
        /** @description Please provide Perfect Invoice related information */
        indicativeCustomsValues?: {
            /**
             * @description Please provide the pre-calculated import customs duties value for the shipment
             * @example 150.57
             */
            importCustomsDutyValue?: number;
            /**
             * @description Please provide the pre-calculated import taxes (VAT/GST) value for the shipment
             * @example 49.43
             */
            importTaxesValue?: number;
            /**
             * @description Please provide pre-calculated total of all line items plus additional charges plus indicativeCustomsValues
             * @example 350.57
             */
            totalWithImportDutiesAndTaxes?: number;
        };
        /** @description Please provide pre-calculated total values */
        preCalculatedTotalValues?: {
            /**
             * @description Please provide the pre-calculated total value of all line items
             * @example 49.43
             */
            preCalculatedTotalGoodsValue: number;
            /**
             * @description Please provide the total value of all line items plus additional charges if any
             * @example 150.57
             */
            preCalculatedTotalInvoiceValue: number;
        };
    };
    /** @description Please enter up to three remarks */
    remarks?: {
        /**
         * @description Please enter remark text
         * @example declaration remark
         */
        value: string;
    }[];
    /** @description Please enter additional charge to appear on the invoice<BR>      admin, Administration Charge<BR>      delivery, Delivery Charge<BR>      documentation, Documentation Charge<BR>      expedite, Expedite Charge<BR>      freight, Freight Charge<BR>      fuel surcharge, Fuel Surcharge<BR>      logistic, Logistic Charge<BR>      other, Other Charge<BR>      packaging, Packaging Charge<BR>      pickup, Pickup Charge<BR>      handling, Handling Charge<BR>      vat, VAT Charge<BR>      insurance, Insurance Cost */
    additionalCharges?: {
        /**
         * @description Please provide the charge value
         * @example 10
         */
        value: number;
        /**
         * @description Please enter charge type
         * @enum {string}
         */
        typeCode:
            | 'admin'
            | 'delivery'
            | 'documentation'
            | 'expedite'
            | 'export'
            | 'freight'
            | 'fuel_surcharge'
            | 'logistic'
            | 'other'
            | 'packaging'
            | 'pickup'
            | 'handling'
            | 'vat'
            | 'insurance'
            | 'reverse_charge';
    }[];
    /**
     * @description Name of port of departure, shipment or destination as required under the applicable delivery term.
     * @example port of departure or destination details
     */
    placeOfIncoterm?: string;
    /**
     * @description Please enter recipient reference
     * @example recipient reference
     */
    recipientReference?: string;
    /** @description Exporter related details */
    exporter?: {
        /**
         * @description Please provide exporter Id
         * @example 123
         */
        id?: string;
        /**
         * @description Please provide exporter code
         * @example EXPCZ
         */
        code?: string;
    };
    /**
     * @description Please provide the reason for export
     * @example permanent
     * @enum {string}
     */
    exportReasonType?:
        | 'permanent'
        | 'temporary'
        | 'return'
        | 'used_exhibition_goods_to_origin'
        | 'intercompany_use'
        | 'commercial_purpose_or_sale'
        | 'personal_belongings_or_personal_use'
        | 'sample'
        | 'gift'
        | 'return_to_origin'
        | 'warranty_replacement'
        | 'diplomatic_goods'
        | 'defence_material';
    /**
     * @description Please provide the shipment was sent for Personal (Gift) or Commercial (Sale) reasons
     * @example personal
     * @enum {string}
     */
    shipmentType?: 'personal' | 'commercial';
    /** @description Please provide the Customs Documents at invoice level */
    customsDocuments?: {
        /**
         * @description Please provide the Customs Document type code at invoice level. Please refer to the YAML Reference Data Guide PDF file for valid enumeration values.
         * @example 972
         * @enum {string}
         */
        typeCode:
            | '972'
            | 'AHC'
            | 'ALC'
            | 'ATA'
            | 'ATR'
            | 'BEX'
            | 'CHA'
            | 'CHD'
            | 'CHP'
            | 'CIT'
            | 'CIV'
            | 'CI2'
            | 'COO'
            | 'CPA'
            | 'CRL'
            | 'CSD'
            | 'DEX'
            | 'DGD'
            | 'DLI'
            | 'DOV'
            | 'ELP'
            | 'EU1'
            | 'EU2'
            | 'EUS'
            | 'EXL'
            | 'FMA'
            | 'HLC'
            | 'HWB'
            | 'INV'
            | 'IPA'
            | 'JLC'
            | 'LIC'
            | 'LNP'
            | 'NID'
            | 'PAS'
            | 'PFI'
            | 'PHY'
            | 'PLI'
            | 'POA'
            | 'PCH'
            | 'ROD'
            | 'T2M'
            | 'TAD'
            | 'TCS'
            | 'VET'
            | 'VEX';
        /**
         * @description Please provide the Customs Document ID at invoice level
         * @example custdoc445
         */
        value: string;
    }[];
    /**
     * @description The Incoterms rules are a globally-recognized set of standards, used worldwide in international and domestic contracts for the delivery of goods, illustrating responsibilities between buyer and seller for costs and risk, as well as cargo insurance.<BR>      EXW ExWorks<BR>      FCA Free Carrier<BR>      CPT Carriage Paid To<BR>      CIP Carriage and Insurance Paid To<BR>      DPU Delivered at Place Unloaded<BR>      DAP Delivered at Place<BR>      DDP Delivered Duty Paid<BR>      FAS Free Alongside Ship<BR>      FOB Free on Board<BR>      CFR Cost and Freight<BR>      CIF Cost, Insurance and Freight<BR>      DAF Delivered at Frontier<BR>      DAT Delivered at Terminal<BR>      DDU Delivered Duty Unpaid<BR>      DEQ Delivery ex Quay<BR>      DES Delivered ex Ship
     * @example DAP
     * @enum {string}
     */
    incoterm: 'EXW' | 'FCA' | 'CPT' | 'CIP' | 'DPU' | 'DAP' | 'DDP' | 'FAS' | 'FOB' | 'CFR' | 'CIF' | 'DAF' | 'DAT' | 'DDU' | 'DEQ' | 'DES';
};

/** Identifier definition */
export type supermodelIoLogisticsExpressIdentifier = {
    /**
     * @description Please provide type of the identifier you want to set value for
     * @example shipmentId
     * @enum {string}
     */
    typeCode: 'parentId' | 'shipmentId' | 'pieceId';
    /**
     * @description Please enter value of your identifier (WB number, PieceID)
     * @example 1234567890
     */
    value: string;
    /**
     * @description Please enter value of Piece Data Identifier. Note: Piece identification data should be same for all the pieces provided in single shipment.
     * @example 00
     */
    dataIdentifier?: string;
};

/** Definition of /identifiers response message */
export type supermodelIoLogisticsExpressIdentifierResponse = {
    warnings?: string[];
    identifiers?: {
        /**
         * @description Type of identifier
         * @example SID
         */
        typeCode: string;
        /** @description List of identifers */
        list: string[];
    }[];
};

/** Definition of image upload request response message */
export type supermodelIoLogisticsExpressImageUploadRequest = {
    /** @example 2020-04-20 */
    originalPlannedShippingDate: string;
    /**
     * accounts
     * @description Please enter all the DHL Express accounts and types to be used for this shipment
     */
    accounts: supermodelIoLogisticsExpressAccount[];
    /**
     * @description Please enter DHL Express Global Product code
     * @example D
     */
    productCode: string;
    documentImages: supermodelIoLogisticsExpressDocumentImages;
};

/**
 * Definition of /landed-cost request message
 * @description Landed cost request model description
 */
export type supermodelIoLogisticsExpressLandedCostRequest = {
    /** @description Here you need to define all the parties needed to ship the package */
    customerDetails: {
        shipperDetails: supermodelIoLogisticsExpressAddressRatesRequest;
        receiverDetails: supermodelIoLogisticsExpressAddressRatesRequest;
    };
    /** @description Please enter all the DHL Express accounts and types to be used for this shipment */
    accounts: supermodelIoLogisticsExpressAccount[];
    /**
     * @description Please enter DHL Express Global Product code
     * @example P
     */
    productCode?: string;
    /**
     * @description Please enter DHL Express Local Product code
     * @example P
     */
    localProductCode?: string;
    /**
     * @description Please enter Unit of measurement - metric,imperial
     * @example metric
     * @enum {string}
     */
    unitOfMeasurement: 'metric' | 'imperial';
    /**
     * @description Currency code for the item price (the product being sold) and freight charge. The Landed Cost calculation result will be returned in this defined currency
     * @example CZK
     */
    currencyCode: string;
    /**
     * @description Set this to true is shipment contains declarable content
     * @example true
     */
    isCustomsDeclarable: boolean;
    /**
     * @description Set this to true if you want DHL EXpress product Duties and Taxes Paid outside shipment destination
     * @example true
     */
    isDTPRequested?: boolean;
    /**
     * @description Set this true if you ask for DHL Express insurance service
     * @example false
     */
    isInsuranceRequested?: boolean;
    /**
     * @description Allowed values 'true' - item cost breakdown will be returned, 'false' - item cost breakdown will not be returned
     * @example true
     */
    getCostBreakdown: boolean;
    /** @description Please provide any additional charges you would like to include in total cost calculation */
    charges?: {
        /**
         * @example insurance
         * @enum {string}
         */
        typeCode: 'freight' | 'additional' | 'insurance';
        /** @example 1250 */
        amount: number;
        /** @example CZK */
        currencyCode: string;
    }[];
    /**
     * @description Possible values:<BR>      commercial: B2B<BR>      personal: B2C<BR>      commercia': B2B<BR>      personal: B2C
     * @example personal
     * @enum {string}
     */
    shipmentPurpose?: 'commercial' | 'personal';
    /**
     * @example air
     * @enum {string}
     */
    transportationMode?: 'air' | 'ocean' | 'ground';
    /**
     * @description Carrier being used to ship with. Allowed values are:<BR>      'DHL','UPS','FEDEX','TNT','POST',<BR>      'OTHERS'
     * @example DHL
     * @enum {string}
     */
    merchantSelectedCarrierName?: 'DHL' | 'UPS' | 'FEDEX' | 'TNT' | 'POST' | 'OTHERS';
    /**
     * packages
     * @description Here you can define properties per package
     */
    packages: supermodelIoLogisticsExpressPackageRR[];
    items: {
        /**
         * @description Line item number
         * @example 1
         */
        number: number;
        /**
         * @description Name of the item
         * @example KNITWEAR COTTON
         */
        name?: string;
        /**
         * @description Item full description
         * @example KNITWEAR 100% COTTON REDUCTION PRICE FALL COLLECTION
         */
        description?: string;
        /**
         * @description ISO Country code of the goods manufacturer
         * @example CN
         */
        manufacturerCountry?: string;
        /**
         * @description SKU number
         * @example 12345555
         */
        partNumber?: string;
        /**
         * @description Total quantity of the item(s) to be shipped.
         * @example 2
         */
        quantity: number;
        /**
         * @description Please provide quantitiy type. prt - part, box - box
         * @example prt
         * @enum {string}
         */
        quantityType?: 'prt' | 'box';
        /**
         * @description Product Unit price
         * @example 120
         */
        unitPrice: number;
        /**
         * @description Currency code of the Unit Price
         * @example EUR
         */
        unitPriceCurrencyCode: string;
        /**
         * @description not used
         * @example 120
         */
        customsValue?: number;
        /**
         * @description not used
         * @example EUR
         */
        customsValueCurrencyCode?: string;
        /**
         * @description commodityCode is mandatory if estimatedTariffRateType ('derived_rate' or 'highest_rate' or 'lowest_rate' or 'center_rate') not provided in the request otherwise it is considered as Optional.<BR>                              'highest_rate' or 'lowest_rate' or 'center_rate') not provided in the request otherwise it is considered as Optional.<BR>            Can be provided with or without dots
         * @example 851713
         */
        commodityCode?: string;
        /**
         * @description Weight of the item
         * @example 5
         */
        weight?: number;
        /**
         * @description Unit of measurement
         * @example metric
         * @enum {string}
         */
        weightUnitOfMeasurement?: 'metric' | 'imperial';
        /**
         * @description commodityCode category can be retrieved via referenceData API/ commodityCategory dataset.<BR>
         * Category code of the Item.<BR>            101 - Coats & Jacket<BR>            102 - Blazers<BR>            103 - Suits<BR>            104 - Ensembles<BR>            105 - Trousers<BR>            106 - Shirts & Blouses<BR>            107 - Dresses<BR>            108 - Skirts<BR>            109 - Jerseys, Sweatshirts & Pullovers<BR>            110 - Sports & Swimwear<BR>            111 - Night & Underwear<BR>            112 - T-Shirts<BR>            113 - Tights & Leggings<BR>            114 - Socks <BR>            115 - Baby Clothes<BR>            116 - Clothing Accessories<BR>            201 - Sneakers<BR>            202 - Athletic Footwear<BR>            203 - Leather Footwear<BR>            204 - Textile & Other Footwear<BR>            301 - Spectacle Lenses<BR>            302 - Sunglasses<BR>            303 - Eyewear Frames<BR>            304 - Contact Lenses<BR>            401 - Watches<BR>            402 - Jewelry<BR>            403 - Suitcases & Briefcases<BR>            404 - Handbags<BR>            405 - Wallets & Little Cases<BR>            406 - Bags & Containers<BR>            501 - Beer<BR>            502 - Spirits<BR>            503 - Wine<BR>            504 - Cider, Perry & Rice Wine<BR>            601 - Bottled Water<BR>            602 - Soft Drinks<BR>            603 - Juices<BR>            604 - Coffee<BR>            605 - Tea<BR>            606 - Cocoa<BR>            701 - Dairy Products & Eggs<BR>            702 - Meat<BR>            703 - Fish & Seafood<BR>            704 - Fruits & Nuts<BR>            705 - Vegetables<BR>            706 - Bread & Cereal Products<BR>            707 - Oils & Fats<BR>            708 - Sauces & Spices<BR>            709 - Convenience Food<BR>            710 - Spreads & Sweeteners<BR>            711 - Baby Food<BR>            712 - Pet Food<BR>            801 - Cigarettes<BR>            802 - Smoking Tobacco<BR>            803 - Cigars<BR>            804 - E-Cigarettes<BR>            901 - Household Cleaners<BR>            902 - Dishwashing Detergents<BR>            903 - Polishes<BR>            904 - Room Scents<BR>            905 - Insecticides<BR>            1001 - Cosmetics<BR>            1002 - Skin Care<BR>            1003 - Personal Care<BR>            1004 - Fragrances<BR>            1101 - Toilet Paper<BR>            1102 - Paper Tissues<BR>            1103 - Household Paper<BR>            1104 - Feminine Hygiene<BR>            1105 - Baby Diapers<BR>            1106 - Incontinence<BR>            1202 - TV, Radio & Multimedia<BR>            1203 - TV Peripheral Devices<BR>            1204 - Telephony<BR>            1205 - Computing<BR>            1206 - Drones<BR>            1301 - Refrigerators<BR>            1302 - Freezers<BR>            1303 - Dishwashing Machines<BR>            1304 - Washing Machines<BR>            1305 - Cookers & Oven<BR>            1306 - Vacuum Cleaners<BR>            1307 - Small Kitchen Appliances<BR>            1308 - Hair Clippers<BR>            1309 - Irons<BR>            1310 - Toasters<BR>            1311 - Grills & Roasters<BR>            1312 - Hair Dryers<BR>            1313 - Coffee Machines<BR>            1314 - Microwave Ovens<BR>            1315 - Electric Kettles<BR>            1401 - Seats & Sofas<BR>            1402 - Beds<BR>            1403 - Mattresses<BR>            1404 - Closets, Nightstands & Dressers<BR>            1405 - Lamps & Lighting<BR>            1406 - Floor Covering<BR>            1407 - Kitchen Furniture<BR>            1408 - Plastic & Other Furniture<BR>            1501 - Analgesics<BR>            1502 - Cold & Cough Remedies<BR>            1503 - Digestives & Intestinal Remedies<BR>            1504 - Skin Treatment<BR>            1505 - Vitamins & Minerals<BR>            1506 - Hand Sanitizer <BR>            1601 - Toys & Games<BR>            1602 - Musical Instruments<BR>            1603 - Sports Equipment
         * @example 204
         */
        category?: string;
        /**
         * @description Item's brand
         * @example SHOE 1
         */
        brand?: string;
        goodsCharacteristics?: {
            /**
             * @description Please contact Express country representative to provide all applicable codes
             * @example IMPORTER
             */
            typeCode: string;
            /**
             * @description Value related to the code
             * @example Registered
             */
            value: string;
        }[];
        additionalQuantityDefinitions?: {
            /**
             * @description Item additional quantity value UOM:<BR>                  example PFL=percent of alcohol
             * @example DPR
             */
            typeCode: string;
            /**
             * @description An Item's additional quantity value:<BR>                  example is percent of alcohol
             * @example 2
             */
            amount: number;
        }[];
        /**
         * @description Please enter Tariff Rate Type - default_rate,derived_rate,highest_rate,center_rate,lowest_rate
         * @example default_rate
         * @enum {string}
         */
        estimatedTariffRateType?: 'default_rate' | 'derived_rate' | 'highest_rate' | 'center_rate' | 'lowest_rate';
    }[];
    /**
     * @description Allowed values 'true' - tariff formula on item and shipment level will be returned, 'false' - tariff formula on item and shipment level will not be returned
     * @example true
     */
    getTariffFormula?: boolean;
    /**
     * @description Allowed values 'true' - quotation ID on shipment level will be returned, 'false' - quotation ID on shipment level will not be returned
     * @example true
     */
    getQuotationID?: boolean;
};

/**
 * Package definition for /shipments
 * @description Package definition for /shipments
 */
export type supermodelIoLogisticsExpressPackage = {
    /**
     * @description Please contact your DHL Express representative if you wish to use a DHL specific package otherwise ignore this element.
     * @example 2BP
     * @enum {string}
     */
    typeCode?: '3BX' | '2BC' | '2BP' | 'CE1' | '7BX' | '6BX' | '4BX' | '2BX' | '1CE' | 'WB1' | 'WB3' | 'XPD' | '8BX' | '5BX' | 'WB6' | 'TBL' | 'TBS' | 'WB2';
    /**
     * @description The weight of the package.
     * @example 22.501
     */
    weight: number;
    /**
     * dimensions
     * @description Dimensions of the package
     */
    dimensions: {
        /**
         * @description Length of the package
         * @example 15.001
         */
        length: number;
        /**
         * @description Width of the package
         * @example 15.001
         */
        width: number;
        /**
         * @description Height of the package
         * @example 40.001
         */
        height: number;
    };
    /** @description Here you can declare your customer references for each package */
    customerReferences?: supermodelIoLogisticsExpressPackageReference[];
    /** @description Identifiers section is on the package level where you can optionaly provide a DHL Express waybill number. This has to be enabled by your DHL Express IT contact. */
    identifiers?: supermodelIoLogisticsExpressIdentifier[];
    /**
     * @description Please enter description of content for each package
     * @example Piece content description
     */
    description?: string;
    /** @description This allows you to define up to two bespoke barcodes on the DHL Express Tranport label. To use this feature please set outputImageProperties/imageOptions/templateName as ECOM26_84CI_003 for typeCode=label */
    labelBarcodes?: {
        /**
         * @description Position of the bespoke barcode
         * @example left
         * @enum {string}
         */
        position: 'left' | 'right';
        /**
         * @description Please enter valid Symbology code
         * @example 93
         * @enum {string}
         */
        symbologyCode: '93' | '39' | '128';
        /** @description Please enter barcode content */
        content: string;
        /**
         * @description Please enter text below customer barcode
         * @example text below left barcode
         */
        textBelowBarcode: string;
    }[];
    /** @description This allows you to enter up to two bespoke texts on the DHL Express Tranport label. To use this feature please set outputImageProperties/imageOptions/templateName as ECOM26_84CI_003 for typeCode=label */
    labelText?: {
        /**
         * @description Position of the bespoke text
         * @example left
         * @enum {string}
         */
        position: 'left' | 'right';
        /**
         * @description Please enter caption to be printed in the tag value
         * @example text caption
         */
        caption: string;
        /**
         * @description Please enter value to be printed for the respective tag in caption
         * @example text value
         */
        value: string;
    }[];
    /**
     * @description Please enter additional customer description
     * @example bespoke label description
     */
    labelDescription?: string;
    /**
     * @description Please enter package reference number. If package reference number is provided for at least one package, then package reference number must be provided for all packages.
     * @example 1
     */
    referenceNumber?: number;
};

/**
 * Package definition for /rates, /products, /landed-cost
 * @description Package defintion for rating related services
 */
export type supermodelIoLogisticsExpressPackageRR = {
    /**
     * @description Please contact your DHL Express representative if you wish to use a DHL specific package otherwise ignore this element.
     * @example 3BX
     * @enum {string}
     */
    typeCode?: '3BX' | '2BC' | '2BP' | 'CE1' | '7BX' | '6BX' | '4BX' | '2BX' | '1CE' | 'WB1' | 'WB3' | 'XPD' | '8BX' | '5BX' | 'WB6' | 'TBL' | 'TBS' | 'WB2';
    /**
     * @description The weight of the package.
     * @example 10.5
     */
    weight: number;
    /**
     * dimensions
     * @description Dimensions of the package
     */
    dimensions: {
        /**
         * @description Length of the package
         * @example 25
         */
        length: number;
        /**
         * @description Width of the package
         * @example 35
         */
        width: number;
        /**
         * @description Height of the package
         * @example 15
         */
        height: number;
    };
};

/**
 * Package Reference definition
 * @description Package Reference model description
 */
export type supermodelIoLogisticsExpressPackageReference = {
    /**
     * @description Please provide reference
     * @example Customer reference
     */
    value: string;
    /**
     * @description Please provide reference type<BR>      <BR>      AAO, shipment reference number of receiver<BR>      CU, reference number of consignor - default<BR>      FF, reference number of freight forwarder<BR>      FN, freight bill number for <ex works invoice number><BR>      IBC, inbound center reference number<BR>      LLR, load list reference for <10-digit Shipment ID><BR>      OBC, outbound center reference number for <SHIPMEN IDENTIFIER (COUNTRY OF ORIGIN)><BR>      PRN, pickup request number for <BOOKINGREFERENCE NUMBER><BR>      ACP, local payer account number<BR>      ACS, local shipper account number<BR>      ACR, local receiver account number<BR>      CDN, customs declaration number<BR>      STD, eurolog 15-digit shipment id<BR>      CO, buyers order number
     *
     * @default CU
     * @example CU
     * @enum {string}
     */
    typeCode?: 'AAO' | 'CU' | 'FF' | 'FN' | 'IBC' | 'LLR' | 'OBC' | 'PRN' | 'ACP' | 'ACS' | 'ACR' | 'CDN' | 'STD' | 'CO';
};

/**
 * Definition of /pickups request message
 * @description RequestPickup schema definition
 */
export type supermodelIoLogisticsExpressPickupRequest = {
    /**
     * @description Identifies the date and time the package is ready for pickup Both the date and time portions of the string are expected to be used. The date should not be a past date or a date more than 10 days in the future. The time is the local time of the shipment based on the shipper's time zone. The date component must be in the format: YYYY-MM-DD; the time component must be in the format: HH:MM:SS using a 24 hour clock. The date and time parts are separated by the letter T (e.g. 2006-06-26T17:00:00 GMT+01:00).<BR>
     * @example 2019-08-04T14:00:31GMT+01:00
     */
    plannedPickupDateAndTime: string;
    /**
     * @description The latest time the location premises is available to dispatch the DHL Express shipment. (HH:MM)
     * @example 18:00
     */
    closeTime?: string;
    /**
     * @description Provides information on where the package should be picked up by DHL courier. <BR>
     * @example reception
     */
    location?: string;
    /**
     * @description Provides information on where the package should be picked up by DHL courier. <BR>
     * @example residence
     * @enum {string}
     */
    locationType?: 'business' | 'residence';
    /** accounts */
    accounts: supermodelIoLogisticsExpressAccount[];
    /** @description Details special pickup instructions you may wish to send to the DHL Courier. */
    specialInstructions?: {
        /**
         * @description Any special instructions user wish to send to the courier for the order pick-up.
         * @example please ring door bell
         */
        value: string;
        /**
         * @description for future use
         * @example TBD
         */
        typeCode?: string;
    }[];
    /** @description Please provide additional pickup remark */
    remark?: string;
    customerDetails: {
        shipperDetails: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
        };
        receiverDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
        };
        bookingRequestorDetails?: {
            postalAddress?: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
        };
        pickupDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
        };
    };
    /** @description Please provide details related to shipment you want to do the pickup for */
    shipmentDetails: {
        /** @description Please provide DHL Express Global product code of the shipment */
        productCode: string;
        /** @description Please provide DHL Express Local product code of the shipment */
        localProductCode?: string;
        /**
         * accounts
         * @description Please enter all the DHL Express accounts related to this shipment
         */
        accounts?: supermodelIoLogisticsExpressAccount[];
        /**
         * valueAddedServices
         * @description This section communicates additional shipping services, such as Insurance (or Shipment Value Protection).
         */
        valueAddedServices?: supermodelIoLogisticsExpressValueAddedServicesRates[];
        /** @description For customs purposes please advise if your shipment is dutiable (true) or non dutiable (false) */
        isCustomsDeclarable: boolean;
        /**
         * @description For customs purposes please advise on declared value of the shipment
         * @example 150
         */
        declaredValue?: number;
        /**
         * @description For customs purposes please advise on declared value currency code of the shipment
         * @example CZK
         */
        declaredValueCurrency?: string;
        /**
         * @description Please enter Unit of measurement - metric,imperial
         * @example metric
         * @enum {string}
         */
        unitOfMeasurement: 'metric' | 'imperial';
        /**
         * @description Please provide Shipment Identification number (AWB number)
         * @example 123456790
         */
        shipmentTrackingNumber?: string;
        /** @description Here you can define properties per package */
        packages: supermodelIoLogisticsExpressPackageRR[];
    }[];
};

/**
 * Definition of /pickups response message
 * @description Comment describing your JSON Schema
 */
export type supermodelIoLogisticsExpressPickupResponse = {
    /** @description List of Dispatch Confirmation Numbers which identifies the scheduled pickup */
    dispatchConfirmationNumbers?: string[];
    /** @example 12:00 */
    readyByTime?: string;
    /** @example 2020-06-01 */
    nextPickupDate?: string;
    warnings?: string[];
};

/**
 * Definition of /products response message
 * @description List of products
 */
export type supermodelIoLogisticsExpressProducts = {
    /** products */
    products: {
        /**
         * @description DHL Express product - Global Product Name
         * @example EXPRESS 12:00 DOC
         */
        productName?: string;
        /**
         * @description This is the global DHL Express product code for which the delivery is feasible respecting the input data from the request.
         * @example T
         */
        productCode?: string;
        /**
         * @description This is the local DHL Express product code for which the delivery is feasible respecting the input data from the request.
         * @example T
         */
        localProductCode?: string;
        /**
         * @description The country code for the local service used
         * @example US
         */
        localProductCountryCode?: string;
        /**
         * @description The NetworkTypeCode element indicates the product belongs to the Day Definite (DD) or Time Definite (TD) network.<BR>            Possible Values;<BR>            DD: Day Definite product<BR>            TD: Time Definite product
         * @example TD
         */
        networkTypeCode?: string;
        /**
         * @description Indicator that the product only can be offered to customers with prior agreement.
         * @example false
         */
        isCustomerAgreement?: boolean;
        /** weight */
        weight?: {
            /**
             * @description The dimensional weight of the shipment
             * @example 100
             */
            volumetric?: number;
            /**
             * @description The quoted weight of the shipment
             * @example 50
             */
            provided?: number;
            /**
             * @description The unit of measurement for the dimensions of the package.
             * @example metric
             */
            unitOfMeasurement?: string;
        };
        breakdown?: {
            /**
             * @description Breakdown Name. <BR> name within the first occurrence of breakdown will be the Global Product Name.
             * @example EXPRESS 12:00
             */
            name?: string;
            /**
             * @description Special service or extra charge code.  This is the code you would have to use in the /shipment service if you wish to add an optional Service such as Saturday delivery
             * @example AB
             */
            serviceCode?: string;
            /**
             * @description Local service code
             * @example AB
             */
            localServiceCode?: string;
            /**
             * @description Breakdown type code. <BR> typeCode within the first occurrence of breakdown will be the Local Product Name.
             * @example EXPRESS 12:00 DOC
             */
            typeCode?: string;
            /**
             * @description Special service charge code type for service.
             * @example XCH
             */
            serviceTypeCode?: string;
            /**
             * @description Customer agreement indicator for product and services, if service is offered with prior customer agreement
             * @example false
             */
            isCustomerAgreement?: boolean;
            /**
             * @description Indicator if the special service is marketed service
             * @example false
             */
            isMarketedService?: boolean;
            /**
             * @description Indicator if there is any discount allowed
             * @example false
             */
            isBillingServiceIndicator?: boolean;
        }[];
        /** @description Group of serviceCodes that are mutually exclusive.  Only one serviceCode among the list must be applied for a shipment */
        serviceCodeMutuallyExclusiveGroups?: {
            /**
             * @description Mutually exclusive serviceCode group name
             * @example Exclusive Billing Services
             */
            serviceCodeRuleName?: string;
            /**
             * @description Mutually exclusive serviceCode group description
             * @example Mutually exclusive Billing Services - shipment can contain just one of following
             */
            description?: string;
            serviceCodes?: {
                /**
                 * @description The special service charge code
                 * @example PZ
                 */
                serviceCode?: string;
            }[];
        }[];
        /** @description Dependency rule groups for a particular serviceCode. */
        serviceCodeDependencyRuleGroups?: {
            /**
             * @description Dependent special service charge code where the rule groups are applied
             * @example PZ
             */
            dependentServiceCode?: string;
            dependencyRuleGroup?: {
                /**
                 * @description Dependency rule group name
                 * @example Labelfree and PLT rule
                 */
                dependencyRuleName?: string;
                /**
                 * @description Dependency rule group description
                 * @example Labelfree requires Paperless Trade (PLT) only if PLT is allowed for product globaly
                 */
                dependencyDescription?: string;
                /**
                 * @description Dependency rule group condition statement
                 * @example Must provide the requiredServiceCode if it is allowed for the productCode
                 */
                dependencyCondition?: string;
                requiredServiceCodes?: {
                    /**
                     * @description required special service code
                     * @example WY
                     */
                    serviceCode?: string;
                }[];
            }[];
        }[];
        pickupCapabilities?: {
            /**
             * @description This indicator has values of Y or N, and tells the consumer if the service in the response has a pickup date on the same day as the requested shipment date (per the request).
             * @example false
             */
            nextBusinessDay?: boolean;
            /**
             * @description This is the cutoff time for the service<BR>                offered in the response. This represents the latest time (local to origin) which the shipment can be tendered to the courier for that service on that day.
             * @example 2019-09-18T15:00:00
             */
            localCutoffDateAndTime?: string;
            /**
             * @description Pickup cut off time in origin's time zone
             * @example 16:00:00
             */
            GMTCutoffTime?: string;
            /**
             * @description The DHL earliest time possible for pickup
             * @example 09:30:00
             */
            pickupEarliest?: string;
            /**
             * @description The DHL latest time possible for pickup
             * @example 16:00:00
             */
            pickupLatest?: string;
            /**
             * @description The DHL Service Area Code for the origin of the Shipment
             * @example ELA
             */
            originServiceAreaCode?: string;
            /**
             * @description The DHL Facility Code for the Origin
             * @example ELA
             */
            originFacilityAreaCode?: string;
            /**
             * @description This is additional transit delays (in days) for shipment picked up from the mentioned city or postal area to arrival at the service area.
             * @example 0
             */
            pickupAdditionalDays?: number;
            /**
             * @description Pickup day of the week number
             * @example 3
             */
            pickupDayOfWeek?: number;
        };
        deliveryCapabilities?: {
            /**
             * @description Delivery Date capabilities considering customs clearance days. Estimated Delivery Date Type. QDDF: is the fastest transit time as quoted to the customer at booking or shipment creation. When clearance or any other non-transport operational component is expected to impact transit time, QDDF does not constitute DHL's service commitment. QDDC: cconstitutes DHL's service commitment as quoted at booking or shipment creation. QDDC builds in clearance time, and potentially other special operational non-transport component(s), when relevant.
             * @example QDDC
             */
            deliveryTypeCode?: string;
            /**
             * @description This is the estimated date/time the shipment will be delivered by for the rated shipment and product listed
             * @example 2019-09-20T12:00:00
             */
            estimatedDeliveryDateAndTime?: string;
            /**
             * @description The DHL Service Area Code for the destination of the Shipment
             * @example PRG
             */
            destinationServiceAreaCode?: string;
            /**
             * @description The DHL Facility Code for the Destination
             * @example PR3
             */
            destinationFacilityAreaCode?: string;
            /**
             * @description This is additional transit delays (in days) for shipment delivered to the<BR>                mentioned city or postal area following arrival at the service area.
             * @example 0
             */
            deliveryAdditionalDays?: number;
            /**
             * @description Destination day of the week number
             * @example 5
             */
            deliveryDayOfWeek?: number;
            /**
             * @description The number of transit days
             * @example 2
             */
            totalTransitDays?: number;
        };
    }[];
    warnings?: string[];
};

/**
 * Reference definition
 * @description Reference model description
 */
export type supermodelIoLogisticsExpressReference = {
    /**
     * @description Please provide reference
     * @example Customer reference
     */
    value: string;
    /**
     * @description Please provide reference type<BR>      <BR>      AAO, shipment reference number of receiver<BR>      CU, reference number of consignor - default<BR>      FF, reference number of freight forwarder<BR>      FN, freight bill number for <ex works invoice number><BR>      IBC, inbound center reference number<BR>      LLR, load list reference for <10-digit Shipment ID><BR>      OBC, outbound center reference number for <SHIPMEN IDENTIFIER (COUNTRY OF ORIGIN)><BR>      PRN, pickup request number for <BOOKINGREFERENCE NUMBER><BR>      ACP, local payer account number<BR>      ACS, local shipper account number<BR>      ACR, local receiver account number<BR>      CDN, customs declaration number<BR>      STD, eurolog 15-digit shipment id<BR>      CO, buyers order number
     * @default CU
     * @example CU
     * @enum {string}
     */
    typeCode?: 'AAO' | 'CU' | 'FF' | 'FN' | 'IBC' | 'LLR' | 'OBC' | 'PRN' | 'ACP' | 'ACS' | 'ACR' | 'CDN' | 'STD' | 'CO';
};

/**
 * RegistrationNumbers definition
 * @description Should your country require registration numbers, such as VAT, EOR etc., please declare it here
 */
export type supermodelIoLogisticsExpressRegistrationNumbers = {
    /**
     * @description VAT, Value-Added tax<BR>      EIN, Employer Identification Number<BR>      GST, Goods and Service Tax<BR>      SSN, Social Security Number<BR>      EOR, European Union Registration and Identification<BR>      DUN, Data Universal Numberin System<BR>      FED, Federal Tax ID<BR>      STA, State Tax ID<BR>      CNP, Brazil CNPJ/CPF Federal Tax<BR>      IE, Brazil type IE/RG Federal Tax<BR>      INN, Russia bank details section INN<BR>      KPP, Russia bank details section KPP<BR>      OGR, Russia bank details section OGRN<BR>      OKP, Russia bank details sectionOKPO<BR>      SDT, Overseas Registered Supplier or Import One-Stop-Shop or GB VAT (foreign) registration or AUSid GST Registration or VAT on E-Commerce<BR>      FTZ, Free Trade Zone ID<BR>      DAN, Deferment Account Duties Only<BR>      TAN, Deferment Account Tax Only<BR>      DTF, Deferment Account Duties, Taxes and Fees Only<BR>      RGP, EU Registered Exporters registration ID<BR>       DLI,Driver's License <BR>      NID,National Identity Card<BR>      ,PAS:Passport<BR>      ,MID:Manufacturer ID
     * @default VAT
     * @example VAT
     * @enum {string}
     */
    typeCode:
        | 'VAT'
        | 'EIN'
        | 'GST'
        | 'SSN'
        | 'EOR'
        | 'DUN'
        | 'FED'
        | 'STA'
        | 'CNP'
        | 'IE'
        | 'INN'
        | 'KPP'
        | 'OGR'
        | 'OKP'
        | 'MRN'
        | 'SDT'
        | 'FTZ'
        | 'DAN'
        | 'TAN'
        | 'DTF'
        | 'RGP'
        | 'DLI'
        | 'NID'
        | 'PAS'
        | 'MID';
    /**
     * @description Please enter registration number
     * @example CZ123456789
     */
    number: string;
    /**
     * @description Please enter 2 character code of the country where the Registration Number has been issued by
     * @example CZ
     */
    issuerCountryCode: string;
};

/**
 * Definition of tracking response message
 * @description tracking response schema
 */
export type supermodelIoLogisticsExpressTrackingResponse = {
    shipments?: {
        /** @example 1234567890 */
        shipmentTrackingNumber?: string;
        /** @example Success */
        status?: string;
        /** @example 2020-05-14T18:00:31 */
        shipmentTimestamp?: string;
        /**
         * @description DHL product code
         * @example N
         */
        productCode?: string;
        /** @example Shipment Description */
        description?: string;
        shipperDetails?: {
            /**
             * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
             * @example
             */
            name?: string;
            postalAddress?: {
                /**
                 * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
                 * @example
                 */
                cityName?: string;
                /**
                 * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
                 * @example
                 */
                countyName?: string;
                /**
                 * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
                 * @example
                 */
                postalCode?: string;
                /**
                 * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
                 * @example
                 */
                provinceCode?: string;
                /** @example CZ */
                countryCode?: string;
            };
            serviceArea?: {
                /** @example ABC */
                code?: string;
                /** @example Alpha Beta Area */
                description?: string;
                outboundSortCode?: string;
            }[];
        };
        receiverDetails?: {
            /**
             * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
             * @example
             */
            name?: string;
            postalAddress?: {
                /**
                 * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
                 * @example
                 */
                cityName?: string;
                /**
                 * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
                 * @example
                 */
                countyName?: string;
                /**
                 * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
                 * @example
                 */
                postalCode?: string;
                /**
                 * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
                 * @example
                 */
                provinceCode?: string;
                /** @example SK */
                countryCode?: string;
            };
            serviceArea?: {
                /** @example BSA */
                code?: string;
                /** @example BSA Area */
                description?: string;
                /** @example facil area */
                facilityCode?: string;
                inboundSortCode?: string;
            }[];
        };
        /** @example 10 */
        totalWeight?: number;
        /** @example metric */
        unitOfMeasurements?: string;
        shipperReferences?: supermodelIoLogisticsExpressReference[];
        events: {
            /** @example 2020-06-10 */
            date?: string;
            /** @example 13:06:00 */
            time?: string;
            /** @example +09:00 */
            GMTOffset?: string;
            /** @example PU */
            typeCode?: string;
            /** @example Shipment picked up */
            description?: string;
            serviceArea?: {
                /** @example BNE */
                code?: string;
                /** @example Brisbane-AU */
                description?: string;
            }[];
            /**
             * @description Note: This field may be intentionally left empty in accordance with the General Data Protection Regulation (GDPR) requirements.
             * @example
             */
            signedBy?: string;
        }[];
        /** @example 1 */
        numberOfPieces?: number;
        pieces?: {
            /** @example 1 */
            number?: number;
            typeCode?: string;
            shipmentTrackingNumber?: string;
            trackingNumber?: string;
            description?: string;
            /**
             * @description The weight of the package.
             * @example 22.5
             */
            weight?: number;
            /**
             * @description The weight of the package.
             * @example 22.5
             */
            dimensionalWeight?: number;
            /**
             * @description The weight of the package.
             * @example 22.5
             */
            actualWeight?: number;
            /**
             * dimensions
             * @description Dimensions of the package
             */
            dimensions?: {
                /**
                 * @description Length of the package
                 * @example 15
                 */
                length?: number;
                /**
                 * @description Width of the package
                 * @example 15
                 */
                width?: number;
                /**
                 * @description Height of the package
                 * @example 40
                 */
                height?: number;
            };
            /**
             * dimensions
             * @description Dimensions of the package
             */
            actualDimensions?: {
                /**
                 * @description Length of the package
                 * @example 15
                 */
                length: number;
                /**
                 * @description Width of the package
                 * @example 15
                 */
                width: number;
                /**
                 * @description Height of the package
                 * @example 40
                 */
                height: number;
            };
            unitOfMeasurements?: string;
            shipperReferences?: supermodelIoLogisticsExpressReference[];
            events: {
                date?: string;
                /** @example +09:00 */
                GMTOffset?: string;
                time?: string;
                typeCode?: string;
                description?: string;
                serviceArea?: {
                    code?: string;
                    description?: string;
                }[];
                /**
                 * @description Note: This field may be intentionally left empty in accordance with
                 * the General Data Protection Regulation (GDPR) requirements.
                 * @example
                 */
                signedBy?: string;
            }[];
        }[];
        /** @example 2020-06-12 */
        estimatedDeliveryDate?: string;
        childrenShipmentIdentificationNumbers?: string[];
        /** @description controlled access data codes such as 'SHPR_CTY' for shipper's city,
         * 'CNSGN_CTY' for consignee's city, 'SVP_URL' for service point URL,
         * 'SVP_FAC' for service point facility code and 'SIGN_NM' for signatory name. */
        controlledAccessDataCodes?: string[];
    }[];
};

/**
 * Definition of /pickups update request message
 * @description UpdatePickup schema definition
 */
export type supermodelIoLogisticsExpressUpdatePickupRequest = {
    /**
     * @description Please enter Dispatch confirmation number which identifies the already scheduled pickup
     * @example CBJ201220123456
     */
    dispatchConfirmationNumber: string;
    /**
     * @description Please enter the shipper account number which was used during the scheduled pickup creation
     * @example 123456789
     */
    originalShipperAccountNumber: string;
    /**
     * @description Identifies the date and time the package is ready for pickup Both the date and time portions of the string are expected to be used. The date should not be a past date or a date more than 10 days in the future. The time is the local time of the shipment based on the shipper's time zone. The date component must be in the format: YYYY-MM-DD; the time component must be in the format: HH:MM:SS using a 24 hour clock. The date and time parts are separated by the letter T (e.g. 2006-06-26T17:00:00 GMT+01:00).
     * @example 2019-08-04T14:00:31GMT+01:00
     */
    plannedPickupDateAndTime: string;
    /**
     * @description The latest time the location premises is available to dispatch the DHL Express shipment. (HH:MM)
     * @example 18:00
     */
    closeTime?: string;
    /**
     * @description Provides information on where the package should be picked up by DHL courier.
     * @example reception
     */
    location?: string;
    /**
     * @description Provides information on where the package should be picked up by DHL courier.
     * @example residence
     * @enum {string}
     */
    locationType?: 'business' | 'residence';
    /** accounts */
    accounts: supermodelIoLogisticsExpressAccount[];
    /** @description Details special pickup instructions you may wish to send to the DHL Courier. */
    specialInstructions?: {
        /**
         * @description Any special instructions user wish to send to the courier for the order pick-up.
         * @example please ring door bell
         */
        value: string;
        /**
         * @description for future use
         * @example TBD
         */
        typeCode?: string;
    }[];
    /** @description Please provide additional pickup remark */
    remark?: string;
    customerDetails: {
        shipperDetails: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
        };
        receiverDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
        };
        bookingRequestorDetails?: {
            postalAddress?: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
        };
        pickupDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
        };
    };
    /** @description Please provide updated details related to shipment you want update the pickup for */
    shipmentDetails?: {
        /**
         * @description Please provide DHL Express Global product code of the shipment
         * @example D
         */
        productCode: string;
        /**
         * @description Please provide DHL Express Local product code of the shipment
         * @example D
         */
        localProductCode?: string;
        /** accounts */
        accounts?: supermodelIoLogisticsExpressAccount[];
        /** valueAddedServices */
        valueAddedServices?: supermodelIoLogisticsExpressValueAddedServicesRates[];
        /** @description For customs purposes please advise if your shipment is dutiable (true) or non dutiable (false) */
        isCustomsDeclarable: boolean;
        /**
         * @description For customs purposes please advise on declared value of the shipment
         * @example 150
         */
        declaredValue?: number;
        /**
         * @description For customs purposes please advise on declared value currency code of the shipment
         * @example CZK
         */
        declaredValueCurrency?: string;
        /**
         * @description Please enter Unit of measurement - metric,imperial
         * @example metric
         * @enum {string}
         */
        unitOfMeasurement: 'metric' | 'imperial';
        /**
         * @description Please provide Shipment Identification number (AWB number)
         * @example 123456790
         */
        shipmentTrackingNumber?: string;
        packages: supermodelIoLogisticsExpressPackageRR[];
    }[];
};

/**
 * Definition of /pickups update response message
 * @description Comment describing your JSON Schema
 */
export type supermodelIoLogisticsExpressUpdatePickupResponse = {
    /**
     * @description Identifies the pickup you made the changes for
     * @example PRG201220123456
     */
    dispatchConfirmationNumber?: string;
    /** @example 10:00 */
    readyByTime?: string;
    /** @example 2020-06-01 */
    nextPickupDate?: string;
    warnings?: string[];
};

/**
 * supermodelIoLogisticsExpressAddressRatesRequest
 * Address definition for /rates, /products, /landed-cost
 * @description Address defintion for rating related services
 */
export type supermodelIoLogisticsExpressAddressRatesRequest = {
    /**
     * @description Please enter your postcode or leave empty if the address doesn't have a postcode
     * @example 14800
     */
    postalCode: string;
    /**
     * @description Please enter the city
     * @example Prague
     */
    cityName: string;
    /**
     * @description Please enter ISO country code
     * @example CZ
     */
    countryCode: string;
    /**
     * @description Please enter your province or state code
     * @example CZ
     */
    provinceCode?: string;
    /**
     * @description Please enter address line 1
     * @example addres1
     */
    addressLine1?: string;
    /**
     * @description Please enter address line 3
     * @example addres2
     */
    addressLine2?: string;
    /**
     * @description Please enter address line 3
     * @example addres3
     */
    addressLine3?: string;
    /**
     * @description Please enter your suburb or county name
     * @example Central Bohemia
     */
    countyName?: string;
};

/**
 * Definition of Upload Invoice Data request message
 * @description MyDHL API REST UploadInvoiceData request JSON Schema
 */
export type supermodelIoLogisticsExpressUploadInvoiceDataRequest = {
    /**
     * @description The planned shipment date for the provided shipmentTrackingNumber.  The date must be in the format: YYYY-MM-DD
     * @example 2020-04-20
     */
    plannedShipDate?: string;
    /**
     * accounts
     * @description Please enter all the DHL Express accounts and types to be used for this shipment.
     * Note: accounts/0/number with typeCode 'shipper' is mandatory if using POST method and no shipmentTrackingNumber is provided in request.
     */
    accounts?: supermodelIoLogisticsExpressAccount[];
    /** @description Here you can define all the properties related to the content of the prospected shipment */
    content: {
        /** @description Here you can find all details related to export declaration */
        exportDeclaration: supermodelIoLogisticsExpressExportDeclaration[];
        /**
         * @description For customs purposes please advise on currency code of the indicated amount in invoice.
         * @example EUR
         */
        currency: string;
        /**
         * @description Please enter Unit of measurement - metric,imperial
         * @example metric
         * @enum {string}
         */
        unitOfMeasurement: 'metric' | 'imperial';
    };
    /** @description Here you can set invoice properties */
    outputImageProperties?: {
        /** @description Here the image options are defined for label, waybillDoc, invoice, receipt and QRcode */
        imageOptions?: {
            /**
             * @description Please enter the document type you want to wish set properties for
             * @example invoice
             * @enum {string}
             */
            typeCode: 'invoice';
            /**
             * @description Please enter DHL Express document template name.
             * @example COMMERCIAL_INVOICE_P_10
             */
            templateName?: string;
            /**
             * @description If set to true then the document is rendered otherwise not
             * @example true
             */
            isRequested?: boolean;
        }[];
    };
    /** @description Here you need to define all the parties needed to ship the package */
    customerDetails?: {
        /** @description Please enter address and contact details related to seller */
        sellerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            /**
             * @description Please enter the business party type of the buyer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
        };
        /** @description Please enter address and contact details related to buyer */
        buyerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            /**
             * @description Please enter the business party type of the buyer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to importer */
        importerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            /**
             * @description Please enter the business party type of the importer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to exporter */
        exporterDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            /**
             * @description Please enter the business party type of the exporter
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to manufacturer */
        manufacturerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            /**
             * @description Please enter the business party type of the manufacturer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to ultimate consignee */
        ultimateConsigneeDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            /**
             * @description Please enter the business party type of the ultimate consignee
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
        };
        /** @description Please enter address and contact details related to broker */
        brokerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            /**
             * @description Please enter the business party type of the broker
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
        };
    };
};

/**
 * Definition of Upload Invoice Data request message with SID
 * @description MyDHL API REST UploadInvoiceData request JSON Schema
 */
export type supermodelIoLogisticsExpressUploadInvoiceDataRequestSID = {
    /**
     * @description Please provide Shipment Identification number (AWB number)
     * @example 123456790
     */
    shipmentTrackingNumber?: string;
    /**
     * @description The planned shipment date for the provided shipmentTrackingNumber.  The date must be in the format: YYYY-MM-DD
     * @example 2020-04-20
     */
    plannedShipDate?: string;
    /**
         * accounts
         * @description Please enter all the DHL Express accounts and types to be used for this shipment.  
  Note: accounts/0/number with typeCode 'shipper' is mandatory if using POST method and no shipmentTrackingNumber is provided in request.
         */
    accounts?: supermodelIoLogisticsExpressAccount[];
    /** @description Here you can define all the properties related to the content of the prospected shipment */
    content: {
        /** @description Here you can find all details related to export declaration */
        exportDeclaration: supermodelIoLogisticsExpressExportDeclaration[];
        /**
         * @description For customs purposes please advise on currency code of the indicated amount in invoice.
         * @example EUR
         */
        currency: string;
        /**
         * @description Please enter Unit of measurement - metric,imperial
         * @example metric
         * @enum {string}
         */
        unitOfMeasurement: 'metric' | 'imperial';
    };
    /** @description Here you can set invoice properties */
    outputImageProperties?: {
        /** @description Here the image options are defined for label, waybillDoc, invoice, receipt and QRcode */
        imageOptions?: {
            /**
             * @description Please enter the document type you want to wish set properties for
             * @example invoice
             * @enum {string}
             */
            typeCode: 'invoice';
            /**
             * @description Please enter DHL Express document template name.
             * @example COMMERCIAL_INVOICE_P_10
             */
            templateName?: string;
            /**
             * @description If set to true then the document is rendered otherwise not
             * @example true
             */
            isRequested?: boolean;
        }[];
    };
    /** @description Here you need to define all the parties needed to ship the package */
    customerDetails?: {
        /** @description Please enter address and contact details related to seller */
        sellerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            /**
             * @description Please enter the business party type of the buyer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
        };
        /** @description Please enter address and contact details related to buyer */
        buyerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            /**
             * @description Please enter the business party type of the buyer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to importer */
        importerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            /**
             * @description Please enter the business party type of the importer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to exporter */
        exporterDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            /**
             * @description Please enter the business party type of the exporter
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to manufacturer */
        manufacturerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
            /**
             * @description Please enter the business party type of the manufacturer
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
        };
        /** @description Please enter address and contact details related to ultimate consignee */
        ultimateConsigneeDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            /**
             * @description Please enter the business party type of the ultimate consignee
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
        };
        /** @description Please enter address and contact details related to broker */
        brokerDetails?: {
            postalAddress: supermodelIoLogisticsExpressAddress;
            contactInformation: supermodelIoLogisticsExpressContact;
            /**
             * @description Please enter the business party type of the broker
             * @enum {string}
             */
            typeCode?: 'business' | 'direct_consumer' | 'government' | 'other' | 'private' | 'reseller';
            registrationNumbers?: supermodelIoLogisticsExpressRegistrationNumbers[];
        };
    };
};

/**
 * Upload Invoice Data Response details
 * @description MyDHL API REST UploadInvoiceData response JSON schema
 */
export type supermodelIoLogisticsExpressUploadInvoiceDataResponse = {
    warnings?: string[];
    /**
     * @description Status description
     * @example OK
     */
    status?: string;
};

/** Value Added Services definition for /shipments */
export type supermodelIoLogisticsExpressValueAddedServices = {
    /**
     * @description Please enter DHL Express value added service code. For detailed list of all available service codes for your prospect shipment please invoke GET /products or GET /rates
     * @example II
     */
    serviceCode: string;
    /**
     * @description Please enter monetary value of service (e.g. Insured Value)
     * @example 100
     */
    value?: number;
    /**
     * @description Please enter currency code (e.g. Insured Value currency code)
     * @example GBP
     */
    currency?: string;
    /**
     * @description Payment method code (e.g. Cash)
     * @example cash
     */
    method?: string;
    /**
     * @description The DangerousGoods section indicates if there is dangerous good content within the shipment.
     *  The ContentID node contains the Content ID for Dangerous Good classification.
     *  It is required to provide the dangerous good Content ID for every dangerous good special service provided, and vice versa.
     *
     * For the complete list of dangerous goods Content IDs and dangerous goods special services combinations, refer to Reference Data Guide section 5. valueAddedServicesDefinition\\dangerousGoods.
     *  Note: Please contact your DHL Express IT representative if additional assistance is required.
     *
     *
     * For dangerous goods shipment with Dry Ice example: UN1845 (Content ID: 901), additional node must be populated 'dryIceTotalNetWeight.'
     *
     * For dangerous goods shipment with 'Excepted Quantities', additional node must be populated 'unCodes'. Few scenarios guideline to prepare a Dangerous Goods shipment for:
     *
     *
     * A) Dry Ice:
     *  1.'serviceCode' element must contain value of 'HC'
     *  2.'contentID' element consists of '901'
     *  3.'dryIceTotalNetWeight' element consists of the total net weight of the dry ice in 'unitofMeasurement'
     *
     *
     * B) Lithium Battery:
     *  1.'serviceType' element must contain value of 'HD', 'HM', 'HV' or 'HW'
     *  2.'contentID' element consists of '966', '969', '967', '970' respectively
     *
     *
     * C) Excepted Quantities:
     *  1.'serviceCode' element must contain value of 'HH'
     *  2.'contentID' element consists of 'E01
     *  3.'unCodes' element consists of the UN code associate with it.
     */
    dangerousGoods?: ({
        /**
         * @description Please enter valid DHL Express Dangerous good content id (please contact your DHL Express IT representative for the relevant content ID code if you are shipping Dan
         * @example 908
         */
        contentId: string;
        /**
         * @description Please enter dry ice total net weight when shipping dry ice
         * @example 12
         */
        dryIceTotalNetWeight?: number;
        /**
         * @description The customDescription node contains the customized Dangerous Goods statement to declare contents accurately.
         * The customDescription node value will be displayed in the Transport Label and Waybill Document,
         * replacing the default IATA Dangerous Goods statement constructed based on contentId node.
         * Multiple customDescription nodes from multiple dangerousGoods nodes will be concatenated using comma separator
         * with combined maximum limit of 200 characters.
         * It is recommended to use customDescription for entire shipment for each dangerousGoods
         * to fully utilize customDescription printout in Transport Label and Waybill Document.
         * Note: For 'customDescription' usage, ensure all 'dangerousGoods' segments are including the 'customDescription' field value.
         * Any of the dangerousGoods does not provide with customDescription field value will be ignored from printing
         * in Transport Label and Waybill Document.
         *
         * @example 1 package Lithium ion batteries in compliance with Section II of P.I. 9661
         */
        customDescription?: string;
        /** @description Please enter UN code(s) */
        unCodes?: string[];
    } & OneOf<
        [
            {
                /**
                 * @description Please enter dry ice total net weight when shipping dry ice
                 * @example 12
                 */
                dryIceTotalNetWeight: number;
            },
            {
                /** @description Please enter UN code(s) */
                unCodes: string[];
            },
        ]
    >)[];
};

/** Value Added Services definition for /rates, /products, /landed-cost */
export type supermodelIoLogisticsExpressValueAddedServicesRates = {
    /**
     * @description Please enter DHL Express value added global service code. For detailed list of all available service codes for your prospect shipment please invoke /products or /rates
     * @example II
     */
    serviceCode: string;
    /**
     * @description Please enter DHL Express value added local service code. For detailed list of all available service codes for your prospect shipment please invoke /products or /rates
     * @example II
     */
    localServiceCode?: string;
    /**
     * @description Please enter monetary value of service (e.g. Insured Value)
     * @example 100
     */
    value?: number;
    /**
     * @description Please enter currency code (e.g. Insured Value currency code)
     * @example GBP
     */
    currency?: string;
    /**
     * @description For future use
     * @example cash
     * @enum {string}
     */
    method?: 'cash';
};

/** Definition of electronic proof of delivery response message */
export type supermodelIoLogisticsExpressEPODResponse = {
    documents?: {
        /** @example PDF */
        encodingFormat?: string;
        /** @example base64 encoded content */
        content?: string;
        /** @example POD */
        typeCode?: string;
    }[];
};

/**
 * Definition of Reference Data response message
 * @description MyDHL API REST ReferenceData response JSON Schema
 */
export type supermodelIoLogisticsExpressReferenceDataResponse = {
    /** @description The result of search from provided reference criteria */
    referenceData?: supermodelIoLogisticsExpressReferenceData[];
    warnings?: string[];
};

/** Definition of Reference Data response message object definition */
export type supermodelIoLogisticsExpressReferenceData = {
    /**
     * @description The reference data dataset name
     * @example country
     */
    datasetName: string;
    dataSetCaptions?: string;
    data?: {
        /** @example attributeName */
        attribute: string;
        /** @example attributeValue */
        value: string;
    }[][];
};

/**
 * Definition of DeliveryOption response JSON Schema
 * @description MyDHL API REST DeliveryOption response JSON Schema
 */
export type supermodelIoLogisticsExpressDeliveryOptionsResponse = {
    /** @description Contains available deliveryOptions for the shipment */
    deliveryOptions: {
        /**
         * @description The name of delivery option"
         * @example signatureRelease
         * @enum {string}
         */
        type: 'signatureRelease' | 'servicePoint' | 'scheduleDelivery' | 'vacationHold';
        parameters: {
            /**
             * @description delivery option parameter name"
             * @example authorizerName
             */
            name: string;
            /** @example true */
            isRequired: boolean;
            /** @description Returned only for type=signatureRelease */
            acceptedValues?: string[];
        }[];
        /**
         * @description Returned only for type=servicePoint
         * @example https://expressapi.dhl.com/mydhlapi/servicepoints?servicePointResults=10&address=BEWDLEY%20DY12%201PY&countryCode=GB&capability=81,73,74,88&language=en
         */
        servicePointURL?: string;
        /** @description Returned only for type=scheduleDelivery and vacationHold. Important: the start and end datetime field values must be among the options provided in GET delivery-option response. */
        deliveryWindows?: {
            /**
             * @description Format e.g 2016-12-31T00:00:00 GMT+08:00
             * @example 2023-08-23T17:00:00 GMT+01:00
             */
            startDateTime: string;
            /**
             * @description Format e.g 2016-12-31T23:59:00 GMT+08:00
             * @example 2023-08-23T21:00:00 GMT+01:00
             */
            endDateTime: string;
        }[];
    }[];
    warnings?: string[];
};

/**
 * supermodelIoLogisticsExpressRateRequest
 * Definition of /rates request message
 * @description Definition of /rates request message
 */
export type ILogisticsExpressRateRequest = {
    customerDetails: {
        shipperDetails: supermodelIoLogisticsExpressAddressRatesRequest;
        receiverDetails: supermodelIoLogisticsExpressAddressRatesRequest;
    };
    /** @description Please enter all the DHL Express accounts and types to be used for this shipment */
    accounts?: supermodelIoLogisticsExpressAccount[];
    /**
     * @description Please enter DHL Express Global Product code
     * @example P
     */
    productCode?: string;
    /**
     * @description Please enter DHL Express Local Product code
     * @example P
     */
    localProductCode?: string;
    /**
     * valueAddedServices
     * @description Please use if you wish to filter the response by value added services
     */
    valueAddedServices?: supermodelIoLogisticsExpressValueAddedServicesRates[];
    /** @description Please use if you wish to filter the response by product(s) and/or value added services */
    productsAndServices?: {
        /**
         * @description Please enter DHL Express Global Product code
         * @example P
         */
        productCode: string;
        /**
         * @description Please enter DHL Express Local Product code
         * @example P
         */
        localProductCode?: string;
        /**
         * valueAddedServices
         * @description Please use if you wish to filter the response by value added services
         */
        valueAddedServices?: supermodelIoLogisticsExpressValueAddedServicesRates[];
    }[];
    /**
     * @description payerCountryCode is to be provided if your profile has been enabled to view rates without an account number (this will provide DHL Express published rates for the payer country)
     * @example CZ
     */
    payerCountryCode?: string;
    /**
     * @description Identifies the date and time the package is tendered. Both the date and time portions of the string are expected to be used. The date should not be a past date or a date more than 10 days in the future. The time is the local time of the shipment based on the shipper's time zone. The date component must be in the format: YYYY-MM-DD; the time component must be in the format: HH:MM:SS using a 24 hour clock. The date and time parts are separated by the letter T (e.g. 2006-06-26T17:00:00 GMT+01:00).
     * @example 2020-03-24T13:00:00GMT+00:00
     */
    plannedShippingDateAndTime: string;
    /**
     * @description Please enter Unit of measurement - metric,imperial
     * @example metric
     * @enum {string}
     */
    unitOfMeasurement: 'metric' | 'imperial';
    /**
     * @description For customs purposes please advise if your shipment is dutiable (true) or non dutiable (false)
     * @example false
     */
    isCustomsDeclarable: boolean;
    /**
     * monetaryAmount
     * @description Please provide monetary amount related to your shipment, for example shipment declared value
     */
    monetaryAmount?: {
        /**
         * @description Please provide the monetary amount type
         * @example declaredValue
         * @enum {string}
         */
        typeCode: 'declaredValue' | 'insuredValue';
        /**
         * @description Please provide the monetary value
         * @example 100
         */
        value: number;
        /**
         * @description Pleaseprovide monetary amount currency code
         * @example CZK
         */
        currency: string;
    }[];
    /**
     * @description Legacy field and replaced by newer field getAdditionalInformation. Please set this to true to receive all value added services for each product available
     * @example false
     */
    requestAllValueAddedServices?: boolean;
    /**
     * estimated delivery date
     * @description Estimated delivery date option for QDDF or QDDC.
     */
    estimatedDeliveryDate?: {
        /**
         * @description Please indicate if requesting to get EDD for this shipment.
         *  Estimated Delivery Date Type. QDDF: is the fastest transit time as quoted to the customer at booking or shipment creation. When clearance or any other non-transport operational component is expected to impact transit time, QDDF does not constitute DHL's service commitment. QDDC: cconstitutes DHL's service commitment as quoted at booking or shipment creation. QDDC builds in clearance time, and potentially other special operational non-transport component(s), when relevant.
         * @default true
         * @example false
         */
        isRequested?: boolean;
        /**
         * @description Please indicate the EDD type being requested
         * @default QDDF
         * @example QDDC
         * @enum {string}
         */
        typeCode?: 'QDDC' | 'QDDF';
    };
    /** @description Provides additional information in the response like all value added services, and rule groups */
    getAdditionalInformation?: {
        /**
         * @description Provide type code of data that can be returned in response.  Values can be allValueAddedServices, allValueAddedServicesAndRuleGroups
         * @enum {string}
         */
        typeCode?: 'allValueAddedServices' | 'allValueAddedServicesAndRuleGroups';
        isRequested: boolean;
    }[];
    /**
     * @description Please set this to true to filter out all products which needs DHL Express special customer agreement
     * @example false
     */
    returnStandardProductsOnly?: boolean;
    /**
     * @description Please set this to true in case you want to receive products which are not available on planned shipping date but next available day
     * @default false
     * @example false
     */
    nextBusinessDay?: boolean;
    /**
     * @description Please select which type of priducts you are interested in
     * @example all
     * @enum {string}
     */
    productTypeCode?: 'all' | 'dayDefinite' | 'timeDefinite';
    /**
     * packages
     * @description Here you can define properties per package
     */
    packages: supermodelIoLogisticsExpressPackageRR[];
};
