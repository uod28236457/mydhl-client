import { IQueryParameters } from './parameters.types';

/**
 * @request GET /products
 * @tags product
 */
export type IGetProductsQueryParams = Pick<
    IQueryParameters,
    | 'accountNumber'
    | 'originCountryCode'
    | 'originPostalCode'
    | 'originCityName'
    | 'destinationCountryCode'
    | 'destinationPostalCode'
    | 'destinationCityName'
    | 'weight'
    | 'length'
    | 'width'
    | 'height'
    | 'plannedShippingDate'
    | 'isCustomsDeclarable'
    | 'unitOfMeasurement'
    | 'nextBusinessDay'
    | 'strictValidation'
    | 'getAllValueAddedServices'
    | 'requestEstimatedDeliveryDate'
    | 'estimatedDeliveryDateType'
>;
