/** @description Sub-entity holding the facility address */
export type Address = {
    /** @description First line of the facility address */
    addressLine1?: string;
    /** @description Second line of the facility address (only present if field is filled in GREF database) */
    addressLine2?: string;
    /** @description Third line of the facility address (only present if field is filled in GREF database) */
    addressLine3?: string;
    /** @description Street details in facility address */
    street?: string;
    /** @description House number in facility address */
    houseNumber?: string;
    /** @description Additional details in facility address */
    additionalInfo?: string;
    /** @description Facility city */
    city?: string;
    /** @description Zip code of the facility */
    zipCode?: string;
    /** @description State */
    state?: string;
    /** @description Country */
    country?: string;
    /** @description DHL country */
    dhlCountry?: string;
    /** @description Country Division Code */
    countryDivisionCode?: string;
    /** @description Enumeration (State, Province) */
    countryDivisionCodeType?: string;
    /** @description Obsolete. This is planned to be removed in future releases. */
    html?: string;
};
/** @description Capacity information – only if additional call to SCMS was triggered */
export type CapacityStatus = {
    /** @description Severity code */
    sev?: string;
    /** @description SCMS */
    msgClg?: string;
    /** @description SCMS Status code */
    msgCIgd?: string;
    /** @description SCMS Description of status code */
    dsc?: string;
    /** @description SCMS detailed description */
    dtlDsc?: string;
};
export type Chronology = {
    zone?: DateTimeZone;
};
/** @description Array of dates:{from, to} when is closed. */
export type ClosedDates = {
    /** @description Date – start when it is closed */
    from?: string;
    /** @description Date – end when it is closed */
    to?: string;
};
/** @description Information about how the Service Point can be contacted */
export type ContactDetails = {
    /** @description Phone number of the Service Point */
    phoneNumber?: string;
    /** @description E-Mail address of the Service Point */
    email?: string;
    /** @description Link to website of the Service Point */
    linkUri?: string;
    /** @description Service Point ID */
    servicePointId?: string;
    /** @description Obsolete. This is planned to be removed in future releases. */
    html?: string;
};
export type DateTimeField = {
    name?: string;
    type?: DateTimeFieldType;
    supported?: boolean;
    lenient?: boolean;
    /** Format: int32 */
    minimumValue?: number;
    /** Format: int32 */
    maximumValue?: number;
    durationField?: DurationField;
    rangeDurationField?: DurationField;
    leapDurationField?: DurationField;
};
export type DateTimeFieldType = {
    name?: string;
    durationType?: DurationFieldType;
    rangeDurationType?: DurationFieldType;
};
export type DateTimeZone = {
    id?: string;
    fixed?: boolean;
};
/** @description Array of numbers L W H */
export type Dimensions = {
    l?: number;
    w?: number;
    h?: number;
};
export type DurationField = {
    name?: string;
    type?: DurationFieldType;
    supported?: boolean;
    /** Format: int64 */
    unitMillis?: number;
    precise?: boolean;
};
export type DurationFieldType = {
    name?: string;
};
/** @description The geo coordinates of the facility’s location */
export type GeoLocation = {
    /**
     * Format: double
     * @description Latitude of the geocoded search address (used for GREF web service search)
     */
    latitude?: number;
    /**
     * Format: double
     * @description Longitude of the geocoded search address (used for GREF web service search)
     */
    longitude?: number;
    suggestion?: Suggestion;
};
/** @description Holiday details */
export type Holidays = {
    /** @description Array of objects: {date, from, to}, where date is date and from and to is time */
    open?: OpenDatesTime[];
    /** @description Array of dates:{from, to} when is closed. */
    closed?: ClosedDates[];
};
/** @description Information about language used for search */
export type Language = {
    /** @description Language Code */
    languageCode?: string;
    /** @description Language Script Code */
    languageScriptCode?: string;
    /** @description Language Country Code */
    languageCountryCode?: string;
    /**
     * @description Language Valid
     * @enum {boolean}
     */
    languageOk?: false;
};
/** @description Array of openingHours entities, each consisting of week day, opening time and closing time. */
export type LocalTime = {
    chronology?: Chronology;
    /** Format: int32 */
    hourOfDay?: number;
    /** Format: int32 */
    minuteOfHour?: number;
    /** Format: int32 */
    secondOfMinute?: number;
    /** Format: int32 */
    millisOfSecond?: number;
    /** Format: int32 */
    millisOfDay?: number;
    fields?: DateTimeField[];
    values?: number[];
    fieldTypes?: DateTimeFieldType[];
};
/** @description Array of objects: {date, from, to}, where date is date and from and to is time */
export type OpenDatesTime = {
    /** @description Date – when is open during holidays */
    date?: string;
    /** @description Time – beginning of open hours */
    from?: string;
    /** @description Time – end of open hours */
    to?: string;
};
/** @description Array of openingHours entities, each consisting of week day, opening time and closing time. */
export type OpeningHours = {
    /** @description Multiple opening hours entities can exist for the same week day. */
    openingHours: OpeningTime[];
    /** @description Holiday details with date */
    holidayDates?: string[];
    /** @description Holiday details with date */
    holidaysDates?: {
        [key: string]: string[];
    };
    /** @description Obsolete. This is planned to be removed in future releases. */
    html?: string;
    /** @description Obsolete. This is planned to be removed in future releases. */
    holidayOpeningHoursHtml?: string;
    holidays?: Holidays;
};
/** @description Multiple opening hours entities can exist for the same week day. */
export type OpeningTime = {
    /**
     * @description Weekday for which this opening hours entity is valid. Possible values are: MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY, HOLIDAY
     * @enum {string}
     */
    dayOfWeek?: 'MONDAY' | 'TUESDAY' | 'WEDNESDAY' | 'THURSDAY' | 'FRIDAY' | 'SATURDAY' | 'SUNDAY' | 'HOLIDAY';
    /** @description The opening time of this entity */
    openingTime?: string;
    /** @description The closing time of this entity */
    closingTime?: string;
};
/** @description Partner information (when SVP type is PRT) */
export type Partner = {
    /** @description ID of a partner */
    partnerId?: string;
    /** @description Name of a partner */
    partnerName?: string;
    /** @description Code of a partner */
    partnerTypeCode?: string;
    promotion?: Promotion;
};
/** @description Promotion on a SVP level */
export type Promotion = {
    /**
     * Format: int64
     * @description Unique identifier for promotion
     */
    id: number;
    /** @description Country Code */
    countryCode?: string;
    /** @description Partner Type code */
    partnerTypeCode?: string;
    /** @description Service Point ID */
    servicePointId?: string;
    /** @description Client ID */
    clientId: string;
    /** @description Capability(PPC) */
    capability: string;
    /**
     * Format: date-time
     * @description Promotion Start date
     */
    startDate: string;
    /**
     * Format: date-time
     * @description Promotion End date
     */
    endDate: string;
    /** @description Promotion on specific day of the week */
    dayOfWeek?: string;
    /** @description Promotion message */
    text: string;
    /** @description Promotion button text */
    buttonText?: string;
    /** @description Promotion Language Code */
    languageCode1?: string;
    /** @description Promotion message */
    text1?: string;
    /** @description Promotion button text */
    buttonText1?: string;
    /** @description Promotion Language Code */
    languageCode2?: string;
    /** @description Promotion message */
    text2?: string;
    /** @description Promotion button text */
    buttonText2?: string;
    /** @description Promotion Language Code */
    languageCode3?: string;
    /** @description Promotion message */
    text3?: string;
    /** @description Promotion button text */
    buttonText3?: string;
    /** @description Promotion Web link */
    hyperlink?: string;
    /**
     * Format: date-time
     * @description Promotion created date
     */
    created?: string;
};
/** @description Response status */
export type RestResponseStatus = {
    /**
     * Format: int32
     * @description Status/error code of the response
     */
    statusCode?: number;
    /** @description Status/error message text of the response */
    statusMessage?: string;
    ok?: boolean;
    warning?: boolean;
    errorStatus?: boolean;
};
/** @description Array of the found Service Points. Each Service Point entity contains details about the service point. */
export type ServicePoint = {
    /**
     * Format: int32
     * @description The facility ID from GREF database
     */
    id?: number;
    /**
     * @description Service Point ID is a unique key with 6 characters, consisting of Service Area for first 3 characters (e.g. BRU) and the last 3 characters is the Facility code (e.g. 001); Service point ID = BRU001.
     * If address is used id not possible to use.
     */
    facilityId?: string;
    /** @description The facility type code from GREF database */
    facilityTypeCode?: string;
    /** @description The service point’s Service Area Code */
    serviceAreaCode?: string;
    /** @description Name of the service point */
    servicePointName?: string;
    /** @description Formatted name of the service point */
    servicePointNameFormatted?: string;
    /** @description The local trading name of the Service Point */
    localName?: string;
    /**
     * @description The type of the Service Point. CITY, STATION, PARTNER or TWENTYFOURSEVEN.
     * @enum {string}
     */
    servicePointType?: 'CITY' | 'STATION' | 'PARTNER' | 'TWENTYFOURSEVEN';
    address?: Address;
    geoLocation?: GeoLocation;
    /** @description The distance from the search address to this Service Point (beeline). */
    distance?: string;
    /** @description Time until which a shipment can be handed in at the Service Point, and is still shipped on the same day */
    shippingCutOffTime?: string;
    openingHours?: OpeningHours;
    servicePointCapabilities?: ServicePointCapabilities;
    contactDetails?: ContactDetails;
    /** @description Obsolete. This is planned to be removed in future releases. */
    shippingCutOffTimeHtml?: string;
    /** @description Distance of SVP from searched location */
    distanceValue?: string;
    /**
     * Format: double
     * @description Metric of distance
     */
    distanceMetric?: number;
    language?: Language;
    shipmentLimitations?: ShipmentLimitations;
    shipmentLimitationsByPiece?: ShipmentLimitationsByPiece;
    /** @description Charge code, e.g. XX */
    chargeCode?: string;
    partner?: Partner;
    promotion?: Promotion;
    capacityStatus?: CapacityStatus;
    /** @description Status of the service point(Active or Inactive) */
    svpStatus?: string;
    /**
     * Format: int32
     * @description Number of day when the work week starts. It starts from 0 to 6(Sunday to Saturday)
     */
    workWeekStart?: number;
    locatedAt?: string;
    timeZone?: DateTimeZone;
};
/** @description An entity that lists all capabilities of a Service Point. */
export type ServicePointCapabilities = {
    /**
     * @description Indicates whether parking facility is available in the service point or not
     * @enum {boolean}
     */
    parkingAvailable?: false;
    /**
     * @description Indicates whether disabled access is available in the service point or not
     * @enum {boolean}
     */
    disabledAccess?: false;
    /**
     * @description Indicates whether Shipment Drop Off is available in the service point or not
     * @enum {boolean}
     */
    shipmentDropOff?: false;
    /**
     * @description Indicates whether Shipment Collection is available in the service point or not
     * @enum {boolean}
     */
    shipmentCollection?: false;
    /**
     * @description Indicates whether International Shipping is available in the service point or not
     * @enum {boolean}
     */
    internationalShipping?: false;
    /**
     * @description Indicates whether Domestic Shipping is available in the service point or not
     * @enum {boolean}
     */
    domesticShipping?: false;
    /**
     * @description Indicates whether Account Shipping is available in the service point or not
     * @enum {boolean}
     */
    accountShippers?: false;
    /**
     * @description Indicates whether Label Printing is available in the service point or not
     * @enum {boolean}
     */
    labelPrinting?: false;
    /**
     * @description Indicates whether Insurance facility is available in the service point or not
     * @enum {boolean}
     */
    insurance?: false;
    /**
     * @description Indicates whether Import Charges is applicable in the service point or not
     * @enum {boolean}
     */
    importCharges?: false;
    /**
     * @description Indicates whether Packaging facility is available in the service point or not
     * @enum {boolean}
     */
    packaging?: false;
    /**
     * @description Indicates whether Receiver Paying option is available in the service point or not
     * @enum {boolean}
     */
    receiverPaid?: false;
    /**
     * @description Indicates whether VISA program is applicable in the service point or not
     * @enum {boolean}
     */
    visaProgram?: false;
    /**
     * @description Indicates whether pay by cash option is available in the service point or not
     * @enum {boolean}
     */
    payWithCash?: false;
    /**
     * @description Indicates whether pay with credit card option is available in the service point or not
     * @enum {boolean}
     */
    payWithCreditCard?: false;
    /**
     * @description Indicates whether pay with cheque option is available in the service point or not
     * @enum {boolean}
     */
    payWithCheque?: false;
    /**
     * @description Indicates whether pay with mobile option is available in the service point or not
     * @enum {boolean}
     */
    payWithMobile?: false;
    /**
     * @description Indicates whether pay with paypal option is available in the service point or not
     * @enum {boolean}
     */
    payWithPayPal?: false;
    /** @description Title for the parking icon */
    parkingTitle?: string;
    /** @description Title for the disable wheel chair icon */
    disabledAccessTitle?: string;
    /** @description Piece Weight Limit */
    pieceWeightLimit?: number;
    /** @description Enumeration (KG or LB) */
    pieceWeightLimitUnit?: string;
    pieceDimensionsLimit?: Dimensions;
    /** @description Enumeration (CM or IN) */
    pieceDimensionsLimitUnit?: string;
    /** @description Number (integer) */
    pieceCountLimit?: number;
    /** @description Account prepaid shippers */
    accountPrepaidShippers?: boolean;
    /** @description Prepaid shippers */
    prepaidShippers?: boolean;
    /** @description Pre-printed return label */
    prePrintReturnLabel?: boolean;
    /** @description Indicates whether this particular service point can handle label free shipments or not */
    labelFree?: boolean;
    /** @description PPC list. */
    ppcList?: string[];
    /** @description Obsolete. This is planned to be removed in future releases. */
    html?: string;
    /** @description PPC codes available for this service point */
    capabilityCodes?: string;
};
export type ServicePointsRestResponseV3 = {
    status?: RestResponseStatus;
    /**
     * @description The address used for the search (value of request parameter 'address')
     * @example Chennai
     */
    searchAddress?: string;
    searchLocation?: GeoLocation;
    /** @description The culture parameter for Bing Maps API (derived from the country parameter in the request) */
    mapCulture?: string;
    /** @description Map Culture Used for Third party Maps */
    mapLanguage?: string;
    /** @description Array of the found Service Points. Each Service Point entity contains details about the service point. */
    servicePoints?: ServicePoint[];
    /** @description Array of strings. Contains information messages  - e.g. required language is not available, result was filtered due to incoming holidays. */
    messages?: string[];
    translations?: Translations;
    /**
     * @description Indicates whether lite mode is acitvated or not.
     * @enum {boolean}
     */
    liteMode?: false;
    promotion?: Promotion;
};
/** @description Information about shipment piece / size */
export type ShipmentLimitations = {
    maxNumberOfPieces?: ValueUnit;
    maxShipmentWeight?: ValueUnit;
    /** @description Obsolete. This is planned to be removed in future releases. */
    html?: string;
};
/** @description Shipment Piece Limitations in this Service Point. */
export type ShipmentLimitationsByPiece = {
    pieceSizeLimit1?: ValueUnit;
    pieceSizeLimit2?: ValueUnit;
    pieceSizeLimit3?: ValueUnit;
    maxPieceWeight?: ValueUnit;
    /** @description Obsolete. This is planned to be removed in future releases. */
    html?: string;
};
/** @description Suggestion for the search address */
export type Suggestion = {
    /** @description Always holds null value */
    label?: string;
    /** @description Always holds null value */
    value?: string;
    /**
     * Format: double
     * @description Latitude of the geocoded search address (used for GREF web service search)
     */
    latitude?: number;
    /**
     * Format: double
     * @description Longitude of the geocoded search address (used for GREF web service search)
     */
    longitude?: number;
    /** @description Country code of the search address */
    countryCode?: string;
    /** @description Place id of the search address */
    placeId?: string;
    /** @description Provider id of the search address */
    providerId?: string;
};
/** @description Translations */
export type Translations = {
    /**
     * @description key value pairs representing the translations
     * @example {'some-key': 'some-value'}
     */
    map?: {
        [key: string]: string;
    };
};
/** @description Max. weight per piece */
export type ValueUnit = {
    /** @description Value in BigDecimal */
    value?: number;
    /** @description UOM */
    uom?: string;
};
export type ExceptionResponse = {
    message?: string;
    status?: string;
    exception?: string;
};
