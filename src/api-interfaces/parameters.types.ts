export type IMessageHeaders = {
    /**
     * @description Please provide message reference
     *
     * @example 'd0e7832e-5c98-11ea-bc55-0242ac13'
     */
    'Message-Reference'?: string;
    /**
     * @description Optional reference date in the  HTTP-date format https://tools.ietf.org/html/rfc7231#section-7.1.1.2
     * @example 'Wed, 21 Oct 2015 07:28:00 GMT'
     */
    'Message-Reference-Date'?: string;
};

export type IStandartHeaders = {
    /** @description Standard User-Agent header https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent */
    'User-Agent'?: string;

    /**
     * @description Format {3-character language code}
     * @example 'eng'
     */
    'Accept-Language'?: string;
};

export type I3PVHeaders = {
    /**
     * @description Please provide name of the plugin (applicable to 3PV only)
     *
     * @example
     */
    'Plugin-Name'?: string;
    /**
     * @description Please provide version of the plugin (applicable to 3PV only)
     *
     * @example
     */
    'Plugin-Version'?: string;
    /**
     * @description Please provide name of the shipping platform(applicable to 3PV only)
     *
     * @example
     */
    'Shipping-System-Platform-Name'?: string;
    /**
     * @description Please provide version of the shipping platform (applicable to 3PV only)
     *
     * @example
     */
    'Shipping-System-Platform-Version'?: string;
    /**
     * @description Please provide name of the webstore platform (applicable to 3PV only)
     *
     * @example
     */
    'Webstore-Platform-Name'?: string;
    /**
     * @description Please provide version of the webstore platform (applicable to 3PV only)
     *
     * @example
     */
    'Webstore-Platform-Version'?: string;
};

export type IQueryParameters = {
    /**
     * @description A short text string code (see values defined in ISO 3166) specifying the shipment origin country. https://gs1.org/voc/Country, Alpha-2 Code
     * @example 'CZ'
     */
    originCountryCode: string;

    /**
     * @description Text specifying the postal code for an address. https://gs1.org/voc/postalCode
     * @example 14800
     */
    originPostalCode?: string;

    /**
     * @description Text specifying the city name
     * @example 'Prague'
     */
    originCityName: string;

    /**
     * @description A short text string code (see values defined in ISO 3166) specifying the shipment destination country. https://gs1.org/voc/Country, Alpha-2 Code
     * @example 'CZ'
     */
    destinationCountryCode: string;

    /**
     * @description Text specifying the postal code for an address. https://gs1.org/voc/postalCode
     * @example 14800
     */
    destinationPostalCode?: string;

    /**
     * @description Text specifying the city name
     * @example 'Prague'
     */
    destinationCityName: string;

    /**
     * @description DHL Express customer account number
     * @example 123456789
     */
    accountNumber: string;

    /**
     * @description DHL Express customer shipper account number
     * @example 123456789
     */
    shipperAccountNumber?: string;

    /**
     * @description Gross weight of the shipment including packaging.
     * @example 5
     */
    weight: number;

    /**
     * @description A string value indicating a Measurement Unit from UN/ECE Recommendation 20, Units of Measure used in International Trade e.g. GRM = gram - see http://www.unece.org/fileadmin/DAM/cefact/recommendations/rec20/rec20_rev3_Annex3e.pdf
     * @example 'GRM'
     */
    weightUnit: string;

    /**
     * @description Total length of the shipment including packaging.
     * @example 15
     */
    length: number;

    /**
     * @description Total width of the shipment including packaging.
     * @example 10
     */
    width: number;

    /**
     * @description Total height of the shipment including packaging.
     * @example 5
     */
    height: number;

    /**
     * @description A string value indicating a Measurement Unit from UN/ECE Recommendation 20, Units of Measure used in International Trade e.g. CM = centimeter - see http://www.unece.org/fileadmin/DAM/cefact/recommendations/rec20/rec20_rev3_Annex3e.pdf
     * @example 'CM'
     */
    dimensionsUnit: string;

    /**
     * @description Airwaybill number
     */
    awbNumber: string;

    /**
     * shipmentTrackingNumberMulti
     * @description DHL Express shipment identification number
     * @example 9356579890
     */
    shipmentTrackingNumber?: string[];

    /**
     * @description DHL Express shipment piece tracking number
     */
    pieceId?: string[];

    /**
     * @description Name of the person requesting to cancel the scheduled pickup
     *
     * @example 'Fred Brent'
     */
    requestorName: string;

    /**
     * pickupReason
     * @description Provide why scheduled pickup is being cancelled
     *
     * @example 'Unplanned leave'
     */
    reason: string;

    /**
     * @description When tracking by Shipment reference you need to restrict the search by timeframe. Please provide the start of the period.
     *
     * @example 2020-05-01
     */
    trackDateFrom?: string;

    /**
     * @description When tracking by Shipment reference you need to restrict the search by timeframe. Please provide the end of the period.
     *
     * @example 2020-06-01
     */
    trackDateTo?: string;

    /**
     * @description Timestamp represents the date you plan to ship your prospected shipment
     *
     * @example 2020-02-26
     */
    plannedShippingDate: string;

    /**
     * @description When set to true and there are no products available for given plannedShippingDate then products available for the next possible pickup date are returned
     *
     * @example false
     */
    nextBusinessDay?: boolean;

    /**
     * @example false
     */
    isCustomsDeclarable: boolean;

    /**
     * @description Shipper DHL Express Account number under which the shipment label was created
     *
     * @example 123456789
     */
    trackShipperAccountNumber?: string;

    /**
     * @description Shipment reference which was provided during the shipment label creation
     *
     * @example 'CustomerReference1'
     */
    trackShipmentReference?: string;

    /**
     * @description Shipment reference type which was provided during the shipment label creation
     *
     * @example 'CU'
     */
    trackShipmentReferenceType?: string;

    /**
     * @example 'all'
     */
    trackingLevelOfDetail?: 'shipment' | 'piece' | 'all';

    /**
     * @example 'all'
     */
    trackingView?: 'all-checkpoints' | 'all-checkpoints-with-remarks' | 'last-checkpoint' | 'shipment-details-only' | 'advance-shipment' | 'bbx-children';

    /**
     * @description Query parameter to request to return values of controlled access code fields in response.
     * @example false
     */
    requestControlledAccessDataCodes?: boolean;

    /**
     * @description Query parameter to request to return GMT Offset of each event in response,
     * for both shipment level and piece level.
     * @example false
     */
    requestGMTOffsetPerEvent?: boolean;

    /**
     * @description Applicable when `isDutiable` is set to `Y`.
     * @example 52
     */
    declaredValue?: string;

    /**
     * @description The UnitOfMeasurement node conveys the unit of measurements used in the operation.
     * This single value corresponds to the units of weight and measurement which are used throughout the message processing.
     *
     * @example 'metric'
     */
    unitOfMeasurement: 'metric' | 'imperial';

    /**
     * @description Currency code for the item price.
     * @example 52
     */
    currencyCode?: string;

    /**
     * @description A short text string code (see values defined in ISO 3166) specifying the country of the payer. https://gs1.org/voc/Country, Alpha-2 Code
     * @example 'DE'
     */
    payerCountryCode?: string;

    /**
     * @description Requested document format.
     * @example 'PDF'
     */
    imageFormat?: 'PDF';

    /**
     * @description Please provide correct document type.
     */
    typeCode: 'waybill' | 'commercial-invoice' | 'customs-entry';

    /**
     * @description Please provide the pickup's date in YYYY-MM format
     */
    pickupYearAndMonth: string;

    /**
     * @description DHL Express customer shipper account number
     * @example 123456789
     */
    'shipper-AccountNumber': string;

    /**
     * @description Please provide the document image encoding format in pdf or tiff format
     */
    encodingFormat?: 'pdf' | 'tiff';

    /**
     * @description Option to return all the document images in a single PDF file
     *
     * @example false
     */
    allInOnePDF?: boolean;

    /**
     * @description Option to return all the document images in a compressed package
     *
     * @example false
     */
    compressedPackage?: boolean;

    /**
     * @description If set to true, indicate strict DCT validation of address details,
     * and validation of product and service(s) combination provided in request.
     * @example false
     */
    strictValidation?: boolean;

    /**
     * @description Option to return list of all value added services and its rule groups if applicable
     * @example false
     */
    getAllValueAddedServices?: boolean;

    /**
     * @description Option to return Estimated Delivery Date in response
     * @example true
     */
    requestEstimatedDeliveryDate?: boolean;

    /**
     * @description Estimated Delivery Date Type.
     * - QDDF: is the fastest transit time as quoted to the customer at booking or shipment creation.
     * When clearance or any other non-transport operational component is expected to impact transit time,
     * QDDF does not constitute DHL's service commitment.
     * - QDDC: cconstitutes DHL's service commitment as quoted at booking or shipment creation.
     * QDDC builds in clearance time, and potentially other special operational non-transport component(s), when relevant.
     * @example 'QDDF'
     */
    estimatedDeliveryDateType?: 'QDDC' | 'QDDF';

    /**
     * @description Option to bypass PLT - WY service code lane capability validation
     *
     * @example false
     */
    bypassPLTError?: boolean;

    /**
     * @description If set to true, indicate to perform shipment data compliant validation on the shipment information.
     * @example false
     */
    validateDataOnly?: boolean;

    /**
     * @description Must provide at least one datasetName value. If providing just the datasetName with no filterBy fields - the response will return the entire data set from the dataset table (bulk).
     * @example 'country'
     */
    datasetName:
        | 'country'
        | 'countryPostalcodeFormat'
        | 'dangerousGoods'
        | 'incoterm'
        | 'productCode'
        | 'serviceCode'
        | 'packageTypeCode'
        | 'documentTypeCode'
        | 'customerShipmentReferenceType'
        | 'customerPackageReferenceType'
        | 'invoiceReferenceType'
        | 'invoiceItemReferenceType'
        | 'registrationNumberTypeCode'
        | 'commodityCategory'
        | 'returnStatusMessage'
        | 'trackingEventCode'
        | 'unitOfMeasurement'
        | 'all';

    /**
     * @description Use filter by value to query based on the specific string for optimized search.
     *
     *  List of supported filterByValue per dataset (eg. dataset: filterByAttribute| supported filterByValue)
     *
     *  - returnStatusMessage: serviceName|CreateShipment,DocumentImageRequest,RateRequest,RequestIdentifier,RequestPickup,AddressValidateRequest,TrackingRequest,UpdatePickup,UpdateShipment,UploadInvoiceData,DeleteShipment,DocumentRetrieve-ePOD
     *  - returnStatusMessage: operationName|get-image,identifiers,address-validate,
     *    RouteRequest, shipments, tracking, upload-image,landed-cost,rates,upload-invoice-data,pickup,proof-of-delivery
     *  - returnStatusMessage: protocol|REST,SOAP,XMLPI
     *  - productCode: docNonDocIndicator|Y,N
     * @example 'MY'
     */
    filterByValue?: string;

    /**
     * @description Use filter by attribute to define the list of supported attibuted for the specified datasetName.
     *  List of supported attributes per dataset
     *  (eg. dataset: supported filterByAttributes values)
     *
     *  - country: countryCode, countryName
     *  - countryPostalcodeFormat: countryCode
     *  - dangerousGoods: serviceCode
     *  - incoterm: incoterm
     *  - productCode: productCode, countryCode, docNonDocIndicator
     *  - serviceCode: serviceCode, countryCode, chargeCodeTypeCode, serviceGroupDescription
     *  - packageTypeCode: packageTypeCode
     *  - documentTypeCode: customsDocumentTypeCode
     *  - customerShipmentReferenceType: shipmentReferenceTypeCode
     *  - customerPackageReferenceType: packageReferenceTypeCode
     *  - invoiceReferenceType: invoiceReferenceTypeCode
     *  - invoiceItemReferenceType: itemReferenceTypeCode
     *  - registrationNumberTypeCode: registrationTypeCode
     *  - commodityCategory: commodityCategoryCode, commodityCategoryGroup, commodityCategoryDescription
     *  - returnStatusMessage: statusCode, serviceName, operationName, protocol
     *  - trackingEventCode: eventTypeCode, eventTypeDescription, visibleToCustomer
     *  - unitOfMeasurement: unitOfMeasurement
     * @example 'countryCode'
     */
    filterByAttribute?: string;

    /**
     * @description Use comparison operator to define the specific match condition for optimized search.
     * @example 'equal'
     */
    comparisonOperator?: 'equal' | 'notEqual' | 'contains';

    /**
     * @description Use queryString for additional filter criteria in format of '[attribute]:[value]:[comparisonOperator]'.
     *  All additional filters are applied together with logical connector 'AND'.
     *  Maximum of three additional attribute-value-comparisonOperator combinations.
     *  Multiple queryString parameters will be separated  by comma "," separator
     * @example 'queryString=operationName:shipment:equal'
     */
    queryString?: string;

    /**
     * @description original planned shipping date of the shipment
     * @example "2023-08-20T00:00:00.000Z"
     */
    originalPlannedShippingDate: string;

    /**
     * @description The provided DHL Express customer shipper account number of the shipment.
     * Conditionally mandatory if receiverMobilePhoneNumber is not provided.
     * The provided shipment's shipper account number must match from the previous created shipment.
     * @example 123456789
     */
    shipperAccountNumber1?: string;

    /**
     * @description The provided receiver's mobile phone number of the shipment.
     * Conditionally mandatory if shipperAccountNumber is not provided.
     * The provided receiver mobile phone number must match from previous created shipment.
     * @example 12123456789
     */
    receiverMobilePhoneNumber?: string;

    /**
     * @description retrieve the initial delivery options results and then the required parameters for each delivery option.
     * @example 'signatureRelease'
     */
    deliveryOption?: 'signatureRelease' | 'scheduleDelivery' | 'vacationHold' | 'servicePoint';
};

export type IPathParameters = {
    /**
     * @description Shipment pickup confirmation number for example `PRG999126012345`
     * @example 'PRG999126012345'
     */
    dispatchConfirmationNumber: string;

    /**
     * shipmentTrackingNumberSingle
     * @description DHL Express shipment identification number
     * @example 9356579890
     */
    shipmentTrackingNumber: string;
};

export type IIdentifierQueryParameters = {
    /**
     * identifierType
     * @description Type of DHL Express identifier to retrieve
     * @example 'SID'
     */
    type: 'SID' | 'PID' | 'ASID3' | 'ASID6' | 'ASID12' | 'ASID24' | 'HUID';

    /**
     * identifierSize
     * @description Number of identifiers to be retrieved
     * @example 1
     */
    size: string;
};

export type IAddressQueryParameters = {
    /**
     * addrValCountryCode
     * @description A short text string code (see values defined in ISO 3166) specifying the shipment origin country.
     * @see https://gs1.org/voc/Country, Alpha-2 Code
     * @example 'CZ'
     */
    countryCode: string;

    /**
     * addrValType
     * @example 'pickup'
     */
    type: 'pickup' | 'delivery';

    /**
     * addrValCityName
     * @description Text specifying the city name
     * @example 'Prague'
     */
    cityName?: string;

    /**
     * addrValCountyName
     * @description Text specifying the county name
     * @example 'praha'
     */
    countyName?: string;

    /**
     * addrValPostalCode
     * @description Text specifying the postal code for an address. https://gs1.org/voc/postalCode
     * @example 14800
     */
    postalCode?: string;

    /**
     * addrValStrictVal
     * @description If set to true service will return no records when exact valid match not found
     * @example true
     */
    strictValidation?: boolean;
};

export type IProofOfDeliveryQueqyParameters = {
    /**
     * ePODcontent
     * @example 'epod-summary'
     */
    content?: 'epod-detail' | 'epod-summary' | 'epod-detail-esig' | 'epod-summary-esig' | 'epod-table';
};

export type IHeaders = IMessageHeaders & IStandartHeaders & I3PVHeaders;
