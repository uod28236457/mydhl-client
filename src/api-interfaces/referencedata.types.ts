import { IQueryParameters } from './parameters.types';

/**
 * @request GET /reference-data
 * @tags reference-data
 */
export type IGetReferenceDataQueryParameters = Pick<IQueryParameters, 'datasetName' | 'filterByValue' | 'filterByAttribute' | 'comparisonOperator' | 'queryString'>;
