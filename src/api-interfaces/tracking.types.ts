import { IQueryParameters } from './parameters.types';

/**
 * @request GET /shipments/{shipmentTrackingNumber}/tracking
 */
export type ITrackSingleShipmentQueryParamenters = Pick<IQueryParameters, 'trackingView' | 'trackingLevelOfDetail' | 'requestControlledAccessDataCodes' | 'requestGMTOffsetPerEvent'>;

export type ITrackMultipleShipmentsQueryParameters = Pick<
    IQueryParameters,
    | 'shipmentTrackingNumber'
    | 'pieceId'
    | 'trackShipmentReference'
    | 'trackShipmentReferenceType'
    | 'trackShipperAccountNumber'
    | 'trackDateFrom'
    | 'trackDateTo'
    | 'trackingView'
    | 'trackingLevelOfDetail'
    | 'requestControlledAccessDataCodes'
>;
