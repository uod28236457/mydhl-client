import { IProofOfDeliveryQueqyParameters, IQueryParameters } from './parameters.types';

/**
 * @request GET /shipments/{shipmentTrackingNumber}/proof-of-delivery
 * @tags shipment
 */
export type IGetShipmentProofOfDeliveryQueryParamenters = Pick<IQueryParameters, 'shipperAccountNumber'> & IProofOfDeliveryQueqyParameters;

/**
 * @request POST /shipments
 * @tags shipment
 */
export type IShipmentCreateQueryParameters = Pick<IQueryParameters, 'strictValidation' | 'bypassPLTError' | 'validateDataOnly'>;

/**
 * @request GET /shipments/{shipmentTrackingNumber}/get-image
 * @tags shipment
 */
export type IShipmentGetImageParameters = Pick<IQueryParameters, 'shipperAccountNumber' | 'typeCode' | 'pickupYearAndMonth' | 'encodingFormat' | 'allInOnePDF' | 'compressedPackage'>;
