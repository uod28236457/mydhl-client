export class MyDHLClientError extends Error {}

export class MyDHLClientResponseError<E = unknown> extends MyDHLClientError {
    response?: E;

    withResponse(response: E) {
        this.response = response;

        return this;
    }
}
