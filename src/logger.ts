import { MyDHLClientError } from './errors';

export const INFO_LOG_METHOD = 'info';
export const ERROR_LOG_METHOD = 'error';

export type ILogger = {
    [INFO_LOG_METHOD](params: any, msg: string): any;
    [ERROR_LOG_METHOD](params: any, msg: string): any;
};

export type ILoggerOptions = {
    credentials: { user: string; password: string };
    logger?: ILogger;
    loggerMethodOverride?: {
        [INFO_LOG_METHOD]?: string;
        [ERROR_LOG_METHOD]?: string;
    };
};

export class Logger implements ILogger {
    protected loggerMethodOverride?: ILoggerOptions['loggerMethodOverride'];
    protected logger: ILogger;
    protected infoMethod: string;
    protected errorMethod: string;

    constructor(options: ILoggerOptions) {
        if (!options.logger) return;

        this.loggerMethodOverride = { ...options.loggerMethodOverride };

        if (!Object.entries(this.loggerMethodOverride).every(([k, v]) => ['error', INFO_LOG_METHOD].includes(k) && typeof v === 'string')) {
            throw new MyDHLClientError('loggerMethodOverride may only contain "error" or "info" keys with string values');
        }
        this.logger = options.logger;
        this.infoMethod = this.loggerMethodOverride?.[INFO_LOG_METHOD] ?? INFO_LOG_METHOD;
        this.errorMethod = this.loggerMethodOverride?.[ERROR_LOG_METHOD] ?? ERROR_LOG_METHOD;
    }

    info(obj: any, msg?: string) {
        return this.logger?.[this.infoMethod]?.(obj, msg);
    }

    error(obj: any, msg?: string) {
        return this.logger?.[this.errorMethod]?.(obj, msg);
    }
}
