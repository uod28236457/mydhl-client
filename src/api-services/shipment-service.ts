// import queryString from 'query-string';
import {
    supermodelIoLogisticsExpressCreateShipmentRequest,
    supermodelIoLogisticsExpressCreateShipmentResponse,
    supermodelIoLogisticsExpressDocumentImageResponse,
    supermodelIoLogisticsExpressEPODResponse,
    supermodelIoLogisticsExpressErrorResponse,
    supermodelIoLogisticsExpressImageUploadRequest,
    supermodelIoLogisticsExpressUploadInvoiceDataRequest,
} from '../api-interfaces/components.types';
import { IMessageHeaders, I3PVHeaders, IPathParameters } from '../api-interfaces/parameters.types';
import { IGetShipmentProofOfDeliveryQueryParamenters, IShipmentCreateQueryParameters, IShipmentGetImageParameters } from '../api-interfaces/shipment.types';
import queryString from 'query-string';
import { AbstractApiService } from './abstract-api-service';

export class ShipmentService extends AbstractApiService {
    /**
     * @summary Electronic Proof of Delivery
     *
     * @description The electronic proof of delivery service can be used to retrieve proof
     *   of delivery for certain delivered DHL Express shipments
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-shipment-exp-api-shipments-epod
     *
     * @operationid exp-api-shipments-epod
     * @request GET /shipments/{shipmentTrackingNumber}/proof-of-delivery
     * @tags shipment
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async getProofOfDelivery(
        shipmentTrackingNumber: IPathParameters['shipmentTrackingNumber'],
        params: IGetShipmentProofOfDeliveryQueryParamenters,
        headerParams?: IMessageHeaders & I3PVHeaders,
    ): Promise<supermodelIoLogisticsExpressEPODResponse> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/shipments/${shipmentTrackingNumber}/proof-of-delivery`,
            query: params,
        });

        return this.callAPI<supermodelIoLogisticsExpressEPODResponse, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }

    /**
     * @summary Upload Paperless Trade shipment (PLT) images of previously created shipment.
     *
     * @description The upload-image service can be used to upload PLT images to a previously created shipment.
     *   The PLT images for the shipment can be uploaded before the shipment has been physically
     *   collected by DHL courier. However, the original shipment must contain WY as the special service otherwise,
     *   an error will be returned when the customer wants to use the reupload function in this upload-image service.
     *   IMPORTANT: Please note that at least 10mins must be given between the initial createShipment request and then
     *   the upload-image request (including subsequent upload-image request).
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-shipment-exp-api-shipments-img-upload
     *
     * @operationid exp-api-shipments-img-upload
     * @request PATCH /shipments/{shipmentTrackingNumber}/upload-image
     * @tags shipment
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async uploadImage(
        shipmentTrackingNumber: IPathParameters['shipmentTrackingNumber'],
        body: supermodelIoLogisticsExpressImageUploadRequest,
        headerParams?: IMessageHeaders & I3PVHeaders,
    ): Promise<void> {
        return this.callAPI<void, supermodelIoLogisticsExpressErrorResponse>(`${this.baseUrl}/shipments/${shipmentTrackingNumber}/upload-image`, {
            method: 'PATCH',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }

    /**
     * @summary Create Shipment
     *
     * @description ## Create Shipment
     *   The ShipmentRequest Operation will allow you to generate an AWB number
     *   and piece IDs, generate a shipping label, transmit manifest shipment
     *   detail to DHL, and optionally book a courier for the pickup of a
     *   shipment. The key elements in the response of the Shipment Request will
     *   be a base64 encoded PDF label and the Shipment and Piece identification
     *   numbers, which you can use for tracking on the DHL web site.
     *   While the RateRequest and ShipmentRequest services can be used
     *   independently, DHL recommends the use of RateRequest to first validate
     *   the products available for the shipper/receiver. The global product
     *   codes which are output during the RateResponse can be used directly as
     *   input into the Shipment Request, as both perform similar validations in
     *   terms of service capability.
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-shipment-exp-api-shipments
     *
     * @operationid exp-api-shipments
     * @request POST /shipments?[strictValidation]&[bypassPLTError]&[validateDataOnly]
     * @tags shipment
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async create(
        params: IShipmentCreateQueryParameters,
        body: supermodelIoLogisticsExpressCreateShipmentRequest,
        headerParams?: IMessageHeaders & I3PVHeaders,
    ): Promise<supermodelIoLogisticsExpressCreateShipmentResponse> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/shipments`,
            query: params,
        });

        return this.callAPI<supermodelIoLogisticsExpressCreateShipmentResponse, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }

    /**
     * @summary Upload Commercial Invoice data for your DHL Express shipment
     *
     * @description ## Upload Invoice Data with Shipment ID
     *   The upload invoice data service can be used to upload Commerical Invoice
     *   data with Shipment Identification Number for your DHL Express
     *   shipment.Customer can provide Commercial Invoice data before Shipment
     *   Data via Create Shipment flow or vice versa.
     * ---
     *   Important Note: UploadInvoiceData service is not enabled by default and
     *   must be requested per customer. Use of this service is only enabled on
     *   exceptional basis and DHL Express recommends to submit shipment requests
     *   together with a commercial invoice data.To enable use of
     *   UploadInvoiceData service, please contact your DHL Express IT
     *   representative. To use UploadInvoiceData service, it is required that
     *   "PM" service code is provided in MyDHL API Create Shipment request. "PM"
     *   service code is not enabled by default for the customers, and needs to be enabled upon request.
     * ---
     *   When Shipment is created via MyDHL API Create Shipment service before
     *   uploading the Commercial Invoice (CIN) data,it is mandatory to provide
     *   the Shipment Identification Number as received in MyDHL API Create
     *   Shipment service Response.
     * ---
     *   When Commercial Invoice (CIN) data is uploaded prior to creating a
     *   shipment via MyDHL API Create Shipment service, it is mandatory to
     *   provide Invoice Reference Number with Invoice Reference Type value "CU"
     *   and Shipper Account Number.
     * ---
     *   These elements are mandatory to facilitate an effective data merge of
     *   the Commercial Invoice (CIN) data with Shipment Data. As an output
     *   customer will receive Notification element value '0' on successful
     *   upload of Commercial Invoice (CIN) data.
     * ---
     *   DHL backend application performs the subsequent data merging process of
     *   the Shipment Data and Commercial Invoice data.
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-shipment-exp-api-shipments-invoice-data-awb
     *
     * @operationid exp-api-shipments-invoice-data-awb
     * @request PATCH /shipments/{shipmentTrackingNumber}/upload-invoice-data
     * @tags shipment
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async uploadInvoiceData(
        shipmentTrackingNumber: IPathParameters['shipmentTrackingNumber'],
        body: supermodelIoLogisticsExpressUploadInvoiceDataRequest,
        headerParams?: IMessageHeaders & I3PVHeaders,
    ): Promise<void> {
        return this.callAPI<void, supermodelIoLogisticsExpressErrorResponse>(`${this.baseUrl}/shipments/${shipmentTrackingNumber}/upload-invoice-data`, {
            method: 'PATCH',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }

    /**
     * @summary Get Image
     *
     * @description The Get Image service can be used to retrieve customer's own uploaded or DHL generated Commercial Invoice,
     *    Waybill Document or other supporting documents that was uploaded during shipment creation.
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-shipment-exp-api-shipments-documentimage
     *
     * @operationid exp-api-shipments-documentimage
     * @request GET /shipments/{shipmentTrackingNumber}/get-image
     * @tags shipment
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async getImage(
        shipmentTrackingNumber: IPathParameters['shipmentTrackingNumber'],
        params: IShipmentGetImageParameters,
        headerParams?: IMessageHeaders & I3PVHeaders,
    ): Promise<supermodelIoLogisticsExpressDocumentImageResponse> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/shipments/${shipmentTrackingNumber}/get-image`,
            query: params,
        });

        return this.callAPI<supermodelIoLogisticsExpressDocumentImageResponse, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }
}
