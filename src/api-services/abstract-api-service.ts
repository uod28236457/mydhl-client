import type { RequestInit } from 'node-fetch';
import fetch from 'node-fetch';
import { Logger } from '../logger';

import type { ILoggerOptions } from '../logger';
import { MyDHLClientResponseError } from 'errors';

export type IClientOptions = ILoggerOptions & {
    baseUrl: string;
};

export abstract class AbstractApiService {
    protected readonly logger: Logger;
    protected readonly baseUrl: string;

    constructor(protected readonly options: IClientOptions) {
        this.logger = new Logger(options);
        this.baseUrl = options.baseUrl;
    }

    protected getBasicAuthorizationHeader() {
        const { user, password } = this.options.credentials;
        const bAuthStr = `${user}:${password}`;

        return {
            authorization: `Basic ${Buffer.from(bAuthStr).toString('base64')}`,
        };
    }

    protected logCallOptions(url, options) {
        try {
            const logOptions = {
                ...options,
                headers: { ...options?.headers },
            };
            // TODO: censor credentials

            this.logger.info({ url, options: logOptions }, `MyDHL API Client request options`);

            if (!options.body) {
                return;
            }

            this.logger.info(options.body, `MyDHL API Client request payload`);
        } catch (err) {
            this.logger.error(err, `MyDHL API Client request logging error`);
        }
    }

    protected async callAPI<R = any, E = any>(url: string, requestOptions: RequestInit): Promise<R> {
        try {
            const options: RequestInit = {
                ...requestOptions,
                headers: {
                    ...requestOptions.headers,
                    ...this.getBasicAuthorizationHeader(),
                },
            };
            this.logCallOptions(url, options);

            const response = await fetch(url, options);
            const result = await response.json();

            this.logger.info(result, 'MyDHL API Client response');

            if (!response.ok) {
                throw new MyDHLClientResponseError<E>('MyDHL API Client response error').withResponse(result as E);
            }

            return result as R;
        } catch (err) {
            this.logger.error(err, 'MyDHL API Client request error');

            throw err;
        }
    }
}
