import queryString from 'query-string';

import { I3PVHeaders, IMessageHeaders, IQueryParameters } from '../api-interfaces/parameters.types';
import { AbstractApiService } from './abstract-api-service';
import { supermodelIoLogisticsExpressErrorResponse, supermodelIoLogisticsExpressIdentifierResponse } from '../api-interfaces/components.types';

export class IdentifierService extends AbstractApiService {
    /**
     * @summary Service to allocate identifiers upfront for DHL Express Breakbulk or
     *   Loose Break Bulk shipments
     *
     * @description Service to allocate identifiers upfront for DHL Express Breakbulk or
     *   Loose Break Bulk shipments. Requires authorization to use this service
     *   from DHL Express.
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-identifier-exp-api-identifiers
     *
     * @operationid exp-api-identifiers
     * @request GET /identifiers
     * @tags identifier
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async get(params: Pick<IQueryParameters, 'accountNumber'>, headerParams?: IMessageHeaders & I3PVHeaders): Promise<supermodelIoLogisticsExpressIdentifierResponse> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/identifiers`,
            query: params,
        });

        return this.callAPI<supermodelIoLogisticsExpressIdentifierResponse, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }
}
