import queryString from 'query-string';

import { I3PVHeaders, IMessageHeaders, IPathParameters, IQueryParameters } from '../api-interfaces/parameters.types';
import {
    supermodelIoLogisticsExpressErrorResponse,
    supermodelIoLogisticsExpressPickupRequest,
    supermodelIoLogisticsExpressUpdatePickupRequest,
    supermodelIoLogisticsExpressUpdatePickupResponse,
} from '../api-interfaces/components.types';

import { AbstractApiService } from './abstract-api-service';

export class PickupService extends AbstractApiService {
    /**
     * @summary Cancel a DHL Express pickup booking request
     *
     * @description The Cancel Pickup service can be used to cancel a DHL Express pickup booking request
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-pickup-exp-api-pickups-cancel
     *
     * @operationid exp-api-pickups-cancel
     * @request DELETE /pickups/{dispatchConfirmationNumber}
     * @tags pickup
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async cancel(
        dispatchConfirmationNumber: IPathParameters['dispatchConfirmationNumber'],
        params: Pick<IQueryParameters, 'requestorName' | 'reason'>,
        headerParams?: IMessageHeaders & I3PVHeaders,
    ): Promise<void> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/pickups/${dispatchConfirmationNumber}`,
            query: params,
        });

        return this.callAPI<void, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }

    /**
     * @summary Update pickup information for an existing DHL Express pickup booking request
     *
     * @description The Update Pickup service can be used to update pickup information for an existing DHL Express pickup booking request
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-pickup-exp-api-pickups-update
     * @operationid exp-api-pickups-update
     * @request PATCH /pickups/{dispatchConfirmationNumber}
     * @tags pickup
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async update(
        dispatchConfirmationNumber: IPathParameters['dispatchConfirmationNumber'],
        body: supermodelIoLogisticsExpressUpdatePickupRequest,
        headerParams?: IMessageHeaders & I3PVHeaders,
    ): Promise<supermodelIoLogisticsExpressUpdatePickupResponse> {
        return this.callAPI<supermodelIoLogisticsExpressUpdatePickupResponse, supermodelIoLogisticsExpressErrorResponse>(`${this.baseUrl}/pickups/${dispatchConfirmationNumber}`, {
            method: 'PATCH',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }

    /**
     * @summary Create a DHL Express pickup booking request
     *
     * @description The Pickup service creates a DHL Express pickup booking request
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-pickup-exp-api-pickups
     *
     * @operationid exp-api-pickups
     * @request POST /pickups
     * @tags pickup
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async create(body: supermodelIoLogisticsExpressPickupRequest, headerParams?: IMessageHeaders & I3PVHeaders): Promise<supermodelIoLogisticsExpressUpdatePickupResponse> {
        return this.callAPI<supermodelIoLogisticsExpressUpdatePickupResponse, supermodelIoLogisticsExpressErrorResponse>(`${this.baseUrl}/pickups`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }
}
