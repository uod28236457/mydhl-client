import queryString from 'query-string';

import { I3PVHeaders, IMessageHeaders, IPathParameters, IStandartHeaders } from '../api-interfaces/parameters.types';
import { AbstractApiService } from './abstract-api-service';
import { ITrackMultipleShipmentsQueryParameters, ITrackSingleShipmentQueryParamenters } from '../api-interfaces/tracking.types';
import { supermodelIoLogisticsExpressErrorResponse, supermodelIoLogisticsExpressTrackingResponse } from '../api-interfaces/components.types';

export class TrackingService extends AbstractApiService {
    /**
     * @summary Track a single DHL Express Shipment
     *
     * @description The Tracking service retrieves tracking statuses for a single DHL
     *   Express Shipment
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-tracking-exp-api-shipments-tracking
     *
     * @operationid exp-api-shipments-tracking
     * @request GET /shipments/{shipmentTrackingNumber}/tracking
     * @tags tracking
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async trackShipment(
        shipmentTrackingNumber: IPathParameters['shipmentTrackingNumber'],
        params: ITrackSingleShipmentQueryParamenters,
        headerParams?: IMessageHeaders & I3PVHeaders & Pick<IStandartHeaders, 'Accept-Language'>,
    ): Promise<supermodelIoLogisticsExpressTrackingResponse> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/shipments/${shipmentTrackingNumber}/tracking`,
            query: params,
        });

        return this.callAPI<supermodelIoLogisticsExpressTrackingResponse, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }

    /**
     * @summary Track a single or multiple DHL Express Shipments
     *
     * @description The Tracking service retrieves tracking statuses for a single or
     *   multiple DHL Express Shipments
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-tracking-exp-api-shipments-tracking-multi
     *
     * @operationid exp-api-shipments-tracking-multi
     * @request GET /tracking
     * @tags tracking
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async trackMultipleShipments(
        params: ITrackMultipleShipmentsQueryParameters,
        headerParams?: IMessageHeaders & I3PVHeaders & Pick<IStandartHeaders, 'Accept-Language'>,
    ): Promise<supermodelIoLogisticsExpressTrackingResponse> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/tracking`,
            query: params,
        });

        return this.callAPI<supermodelIoLogisticsExpressTrackingResponse, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }
}
