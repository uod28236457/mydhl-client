import {
    supermodelIoLogisticsExpressUploadInvoiceDataRequestSID,
    supermodelIoLogisticsExpressUploadInvoiceDataResponse,
    supermodelIoLogisticsExpressErrorResponse,
} from '../api-interfaces/components.types';
import { IMessageHeaders, I3PVHeaders } from '../api-interfaces/parameters.types';
import { AbstractApiService } from './abstract-api-service';

export class InvoiceService extends AbstractApiService {
    /**
     * @summary Upload Commercial invoice data
     *
     * @description ## Upload invoice data
     *   The upload invoice data service can be used to upload Commerical Invoice
     *   data without Shipment Identification Number for your DHL Express
     *   shipment. Customer can provide Commercial Invoice data before Shipment
     *   Data via Create Shipment flow or vice versa.
     *   Important Note: UploadInvoiceData service is not enabled by default and
     *   must be requested per customer.Use of this service is only enabled on
     *   exceptional basis and DHL Express recommends to submit shipment requests
     *   together with a commercial invoice data.
     *   To enable use of UploadInvoiceData service, please contact your DHL
     *   Express IT representative. To use UploadInvoiceData service, it is
     *   required that "PM" service code is provided in MyDHL API Create Shipment
     *   request.
     *   "PM" service code is not enabled by default for the customers, and needs
     *   to be enabled upon request.
     *   When Shipment is created via MyDHL API Create Shipment service before
     *   uploading the Commercial Invoice (CIN) data,it is mandatory to provide
     *   the Shipment Identification Number as received in MyDHL API Create
     *   Shipment service Response. When Commercial Invoice (CIN) data is
     *   uploaded prior to creating a shipment via MyDHL API Create Shipment
     *   service, it is mandatory to provide Invoice Reference Number with Invoice Reference
     *   Type value "CU" and Shipper Account Number.
     *   These elements are mandatory to facilitate an effective data merge of
     *   the Commercial Invoice (CIN) data with Shipment Data. As an output
     *   customer will receive Notification element value '0' on successful
     *   upload of Commercial Invoice (CIN) data.
     *   DHL backend application performs the subsequent data merging process of
     *   the Shipment Data and Commercial Invoice data.
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-invoice-exp-api-shipments-invoice-data
     *
     * @operationid exp-api-shipments-invoice-data
     * @request POST /invoices/upload-invoice-data
     * @tags invoice
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async uploadInvoiceData(
        body: supermodelIoLogisticsExpressUploadInvoiceDataRequestSID,
        headerParams?: IMessageHeaders & I3PVHeaders,
    ): Promise<supermodelIoLogisticsExpressUploadInvoiceDataResponse> {
        return this.callAPI<supermodelIoLogisticsExpressUploadInvoiceDataResponse, supermodelIoLogisticsExpressErrorResponse>(`${this.baseUrl}/invoices/upload-invoice-data`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }
}
