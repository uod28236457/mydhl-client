import queryString from 'query-string';

import { AbstractApiService } from './abstract-api-service';
import { IAddressValidateQueryParams } from '../api-interfaces/address.types';
import { supermodelIoLogisticsExpressAddressValidateResponse, supermodelIoLogisticsExpressErrorResponse } from '../api-interfaces/components.types';
import { IMessageHeaders, I3PVHeaders } from '../api-interfaces/parameters.types';

export class AddressService extends AbstractApiService {
    /**
     * @summary Validate DHL Express pickup/delivery capabilities at origin/destination
     *
     * @description Validates if DHL Express has got pickup/delivery capabilities at
     *   origin/destination
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-address-exp-api-address-validate
     *
     * @operationid exp-api-address-validate
     * @request GET /address-validate
     * @tags address
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async validate(params: IAddressValidateQueryParams, headerParams?: IMessageHeaders & I3PVHeaders): Promise<supermodelIoLogisticsExpressAddressValidateResponse> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/address-validate`,
            query: params,
        });

        return this.callAPI<supermodelIoLogisticsExpressAddressValidateResponse, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }
}
