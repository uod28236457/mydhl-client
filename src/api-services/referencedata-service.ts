import queryString from 'query-string';

import { AbstractApiService } from './abstract-api-service';
import { supermodelIoLogisticsExpressReferenceDataResponse, supermodelIoLogisticsExpressErrorResponse } from '../api-interfaces/components.types';
import { IMessageHeaders, I3PVHeaders } from '../api-interfaces/parameters.types';
import { IGetReferenceDataQueryParameters } from '../api-interfaces/referencedata.types';

export class ReferenceDataService extends AbstractApiService {
    /**
     * @summary Provide reference data currently used for MyDHL API services usage.
     *
     * @description The reference data service retrieves the reference data used for
     *    MyDHL API shipment or upload invoice data service.
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-reference-data-exp-api-reference-data
     *
     * @operationid exp-api-reference-data
     * @request GET /reference-data
     * @tags reference-data
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async get(params: IGetReferenceDataQueryParameters, headerParams?: IMessageHeaders & I3PVHeaders): Promise<supermodelIoLogisticsExpressReferenceDataResponse> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/servicepoints`,
            query: params,
        });

        return this.callAPI<supermodelIoLogisticsExpressReferenceDataResponse, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }
}
