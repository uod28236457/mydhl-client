import queryString from 'query-string';

import type { IGetRatesManyQueryParameters, IGetRatesOnePieceQueryParams } from '../api-interfaces/rates.types';
import type {
    supermodelIoLogisticsExpressErrorResponse,
    ILogisticsExpressRateRequest,
    ILogisticsExpressRates,
    supermodelIoLogisticsExpressLandedCostRequest,
} from '../api-interfaces/components.types';
import type { I3PVHeaders, IMessageHeaders } from '../api-interfaces/parameters.types';

import { AbstractApiService } from './abstract-api-service';

export class RatingService extends AbstractApiService {
    /**
     * @summary Retrieve Rates for a one piece Shipment
     *
     * @description The Rate request will return DHL's product capabilities and prices (where applicable) based on the input data.
     * Using the shipper and receiver address as well
     * as the dimension and weights of the pieces belonging to a shipment,
     * this operation returns the available products including the shipping price (where applicable)
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-tag-rating
     *
     * @operationid exp-api-rates
     * @request GET /rates
     * @tags rating
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async getRatesOnePiece(params: IGetRatesOnePieceQueryParams, headerParams?: IMessageHeaders & I3PVHeaders): Promise<ILogisticsExpressRates> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/rates`,
            query: params,
        });

        return this.callAPI<ILogisticsExpressRates, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'GET',
            // body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }

    /**
     * @summary Retrieve Rates for Multi-piece Shipments
     *
     * @description The Rate request will return DHL's product capabilities and prices (where applicable) based on the input data.
     * Using the shipper and receiver address as well
     * as the dimension and weights of the pieces belonging to a shipment,
     * this operation returns the available products including the shipping price (where applicable)
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-rating-exp-api-rates-many
     *
     * @operationid exp-api-rates-many
     * @request POST /rates
     * @tags rating
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async getRatesMany(body: ILogisticsExpressRateRequest, params: IGetRatesManyQueryParameters, headerParams?: IMessageHeaders & I3PVHeaders): Promise<ILogisticsExpressRates> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/rates`,
            query: params,
        });

        return this.callAPI<ILogisticsExpressRates, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }

    /**
     * @summary  Landed Cost
     *
     * @description The Landed Cost section allows further information around products being sold to be provided.
     * In return the duty, tax and shipping charges are calculated in real time
     * and provides transparency about any extra costs the buyer may have to pay before they reach them.
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-rating-exp-api-landed-cost
     *
     * @operationid exp-api-landed-cost
     * @request POST /landed-cost
     * @tags rating
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async getLandedCost(body: supermodelIoLogisticsExpressLandedCostRequest, headerParams?: IMessageHeaders & I3PVHeaders): Promise<ILogisticsExpressRates> {
        return this.callAPI<ILogisticsExpressRates, supermodelIoLogisticsExpressErrorResponse>(`${this.baseUrl}/landed-cost`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }
}
