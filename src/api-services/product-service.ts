import queryString from 'query-string';

import { supermodelIoLogisticsExpressErrorResponse, supermodelIoLogisticsExpressProducts } from '../api-interfaces/components.types';
import { I3PVHeaders, IMessageHeaders } from '../api-interfaces/parameters.types';
import { AbstractApiService } from './abstract-api-service';
import { IGetProductsQueryParams } from '../api-interfaces/products.types';

export class ProductService extends AbstractApiService {
    /**
     * @summary Retrieve available DHL Express products for a one piece Shipment
     *
     * @description The GET Products API will return DHL's product capabilities for a
     *   certain set of input data.
     *   Using the shipper and receiver address as well as the dimension and
     *   weight of the piece belonging to a shipment, this operation returns the
     *   available products.
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-product-exp-api-products
     *
     * @operationid exp-api-products
     * @request GET /products
     * @tags product
     * @throws {supermodelIoLogisticsExpressErrorResponse | unknown}
     */
    async get(params: IGetProductsQueryParams, headerParams?: IMessageHeaders & I3PVHeaders): Promise<supermodelIoLogisticsExpressProducts> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/products`,
            query: params,
        });

        return this.callAPI<supermodelIoLogisticsExpressProducts, supermodelIoLogisticsExpressErrorResponse>(url, {
            method: 'GET',
            // body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }
}
