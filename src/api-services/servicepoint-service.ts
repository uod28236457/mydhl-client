import queryString from 'query-string';

import { IGetServicePointsQueryParameters } from '../api-interfaces/servicepoint.types';
import { AbstractApiService } from './abstract-api-service';
import { ExceptionResponse, ServicePointsRestResponseV3 } from '../api-interfaces/schemas.types';
import { IMessageHeaders, I3PVHeaders } from '../api-interfaces/parameters.types';

export class ServicepointService extends AbstractApiService {
    /**
     * @summary Returns list of service points based on the given postal location address, service point ID or geocode details
     *   for DHL Express Service points to pick-up and drop-off shipments
     *
     * @description Get service points based on the given input parameters
     *
     * @see https://developer.dhl.com/api-reference/dhl-express-mydhl-api?language_content_entity=en#operations-servicepoint-exp-api-servicepoints
     *
     * @operationid exp-api-servicepoints
     * @request GET /servicepoints
     * @tags servicepoint
     * @throws {ExceptionResponse | unknown}
     */
    async getList(params: IGetServicePointsQueryParameters, headerParams?: IMessageHeaders & I3PVHeaders): Promise<ServicePointsRestResponseV3> {
        const url = queryString.stringifyUrl({
            url: `${this.baseUrl}/servicepoints`,
            query: params,
        });

        return this.callAPI<ServicePointsRestResponseV3, ExceptionResponse>(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                ...headerParams,
            },
        });
    }
}
