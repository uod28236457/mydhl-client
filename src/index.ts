// import fetch from 'node-fetch';

import { MyDHLClientError } from './errors';

import { AbstractApiService, IClientOptions } from './api-services/abstract-api-service';
import { RatingService } from './api-services/rating-service';
import { ProductService } from './api-services/product-service';
import { ShipmentService } from './api-services/shipment-service';
import { TrackingService } from './api-services/tracking-service';
import { PickupService } from './api-services/pickup-service';
import { IdentifierService } from './api-services/identifier-service';
import { AddressService } from './api-services/address-service';
import { InvoiceService } from './api-services/invoice-service';
import { ServicepointService } from './api-services/servicepoint-service';
import { ReferenceDataService } from './api-services/referencedata-service';

export class MyDHLClient extends AbstractApiService {
    readonly rating: AbstractApiService;
    readonly product: AbstractApiService;
    readonly shipment: AbstractApiService;
    readonly tracking: AbstractApiService;
    readonly pickup: AbstractApiService;
    readonly identifier: AbstractApiService;
    readonly address: AbstractApiService;
    readonly invoice: AbstractApiService;
    readonly servicepoint: AbstractApiService;
    readonly referenceData: AbstractApiService;

    constructor(protected readonly options: IClientOptions) {
        super(options);
        this.logger.info('MyDHLClient initialized');
        const { user, password } = this.options.credentials;

        if (!(user || password)) {
            throw new MyDHLClientError('Empty auth user or password value, please provide correct basic auth credentials');
        }

        this.rating = new RatingService(options);
        this.product = new ProductService(options);
        this.shipment = new ShipmentService(options);
        this.tracking = new TrackingService(options);
        this.pickup = new PickupService(options);
        this.identifier = new IdentifierService(options);
        this.address = new AddressService(options);
        this.invoice = new InvoiceService(options);
        this.servicepoint = new ServicepointService(options);
        this.referenceData = new ReferenceDataService(options);
    }
}

export type { IClientOptions };
